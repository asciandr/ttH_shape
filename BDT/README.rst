Setup environment     
---------
I.e. ::

  source setup.sh

Run BDT training
---------
...with settings defined in the python script, e.g.:: 

  nohup python run.py R21_ttH/config_ttH_4L R21_ttH/input.varlist R21_ttH/selection.txt R21_ttH/weight.txt >& log &
  tail -f log

...with settings defined through the varlist file name, e.g.::

  nohup python run.py WWZ/config_DF_WWZ4Ltraining_skimmed WWZ/SF_NTrees1000_MinNodeSize5_Shrinkage010_NCuts30_MaxDepth5_BDT.varlist WWZ/selection.txt WWZ/weight.txt >& log &
  tail -f log

Run BDT evaluation
---------
Make sure that your xml inputs are mapped to MakeClass methods in ``app_header.h``. Then test locally, e.g.::

  nohup python run_application.py WWZ/app_config_skimmed WWZ/selection.txt WWZ/weight.txt WWZ/app_input_xml WWZ/app_outFolder >& log &
  tail -f log

and (you better) submit batch jobs (1 job per inpur root file), e.g. ::

  ./evaluation_on_batch.sh app_R21_ttH4L_skimmed_config_batch

Run optimisation of BDT parameters in batch
---------
Clean everything (make sure old ``LSFJOB_*`` folders are not there)::

  . clean.sh

Choose in ``parameters.txt`` ranges and steps for each BDT parameter and produce copies of a varlist with the defined combination of settings::

  nohup python produce_BDTconfigs.py parameters.txt WWZ/input.varlist >& log &
  tail -f log

List them in a txt file:: 

  ls -1 myvarlist_NTrees* >& test.txt

and submit all jobs to the batch::	

  ./submit_batch.sh test.txt

Once all bjobs are done find the best configuration::

  nohup python find_bestroc.py >& log &
  tail -f log
