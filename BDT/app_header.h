#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <string>
#include <cstring>

#include "nominal.C"

#ifndef __CINT__
// FLOAT
typedef float (nominal::*nominal_method_F);
typedef map<string, nominal_method_F> nominal_func_map_F;
// INT
typedef int (nominal::*nominal_method_I);
typedef map<string, nominal_method_I> nominal_func_map_I;
#endif

// Map nominal methods to names
// FLOAT
static nominal_func_map_F map_functions_F () {
  nominal_func_map_F m_F;
  m_F["lead_jetPt"]            		= &nominal::lead_jetPt;
  m_F["best_Z_Mll"]            		= &nominal::best_Z_Mll;
  m_F["best_Z_other_Mll"]      		= &nominal::best_Z_other_Mll;
  m_F["Mllll0123"]             		= &nominal::Mllll0123;
  m_F["MET_RefFinal_et"]       		= &nominal::MET_RefFinal_et;
  m_F["HT_lep"]                		= &nominal::HT_lep;
  m_F["HT"]                    		= &nominal::HT;
  m_F["fourLep_PME"]           		= &nominal::fourLep_PME;
  m_F["Ml2l3met"]              		= &nominal::Ml2l3met;
 
  return m_F;
}
// INT
static nominal_func_map_I map_functions_I () {
  nominal_func_map_I m_I;
  m_I["nJets_OR_T"]            		= &nominal::nJets_OR_T;

  return m_I;
}
