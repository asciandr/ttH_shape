#!/bin/bash

pwd
workdir=`pwd`
mydir=/afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/BDT/
input_folder=/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/newTightDefs_v5/
echo XSAMPTXT
cp $mydir/*.C $mydir/*.py $mydir/*.h $mydir/*.sh .
cp -r $mydir/R21_ttH/ .
ls -haltr 
. setup.sh
ls -haltr $input_folder/XFILE
ls -1 $input_folder/XFILE > XSAMPTXT
python run_application.py XSAMPTXT R21_ttH/app_selection R21_ttH/weight.txt R21_ttH/app_input_xml R21_ttH/app_outFolder
echo 'done.'
