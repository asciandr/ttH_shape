#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

#Playing with logfiles
outfile="rocs.txt"
print "Writing ROC integrals into '"+outfile+"'"
command="grep -R -i \"BEST ROC INTEGRAL: \" LSFJOB_*/STDOUT >& "+outfile
os.system(command)

#Find optimal configuration
maximum_integral=0
maximum_index=""
maximum_job=""
print "Opening .txt file '"+outfile+"'"
txt_file  = open(outfile,"r")
for i,line in enumerate(txt_file):
    indexA=line.find('BEST ROC INTEGRAL: ')+19
    indexB=line.find(' for the following combination')
    indexC=line.find('_')+1
    indexD=line.find('/')
    if float(line[indexA:indexB]) > maximum_integral:
        maximum_integral=float(line[indexA:indexB])
        maximum_index=i+1
        maximum_job=line[indexC:indexD]

print "I found the maximum integral: "+str(maximum_integral)+" obtained for combination on line "+str(maximum_index)+" and job n. "+maximum_job
best_joblog="LSFJOB_"+maximum_job+"/STDOUT"
print "Opening .txt file '"+best_joblog
job_file  = open(best_joblog,"r")
for line in job_file:
    if not line.find("BDT configuration:") != -1:
        continue
    else:
        print ""
        print "***********************************************************************************"
        print "**************************	OPTIMAL CONFIGURATION	**************************"
        print line
        print "**************************	OPTIMAL CONFIGURATION	**************************"
        print "***********************************************************************************"
        print ""
