#!/bin/bash

pwd
workdir=`pwd`
echo 'This is the configuration:'
echo CONFIG
mydir=/afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/BDT/
cp $mydir/*.C $mydir/*.py $mydir/*.h $mydir/*.sh .
cp $mydir/CONFIG .
cp -r $mydir/WWZ/ .
ls -haltr
. setup.sh
# eos not working...
#python run.py WWZ/config_DF_WWZ4Ltraining_skimmed WWZ/SF_NTrees1000_MinNodeSize5_Shrinkage010_NCuts30_MaxDepth5_BDT.varlist WWZ/selection.txt WWZ/weight.txt
python run.py WWZ/Test CONFIG WWZ/selection.txt WWZ/weight.txt
echo 'done.'
