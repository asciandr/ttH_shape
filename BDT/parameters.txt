##--- BDT Par		;step		;min		;max	---##
#SF_NTrees1000_MinNodeSize5_Shrinkage010_NCuts30_MaxDepth5_BDT.varlist
NTrees			;100		;500		;1000
MinNodeSize		;2		;1		;9
Shrinkage		;10		;10		;15
NCuts			;5		;10		;30
MaxDepth		;1		;2		;6
