#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys = str(os.environ['ROOTSYS'])

inputfiles 	= ""
selection 	= ""
weight 		= ""
inputXML 	= ""
outfolder 	= ""

try:
    inputfiles 		= str(sys.argv[1])
    selection 		= str(sys.argv[2])
    weight 		= str(sys.argv[3])
    inputXML 		= str(sys.argv[4])
    outfolder 		= str(sys.argv[5])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python run_application.py inputfiles selection weight inputXML outfolder"
    sys.exit(1)
    pass

if not os.path.exists("./"+inputfiles): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+selection):
    print "Input "+str(sys.argv[2])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+weight):
    print "Input "+str(sys.argv[3])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+inputXML):
    print "Input "+str(sys.argv[4])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+outfolder):
    print "Input "+str(sys.argv[5])+" not found, aborting..."
    sys.exit(1)
    pass

print "Compiling macros..."
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L auto_TMVAClassificationApplication.C+")
if cmpFailure:
    print "Compilation failure, aborting..."
    sys.exit(1)
    pass

myMethodList=""
#Set integrated luminosity
Lum=36074.6
analysis="4lep"
debug=true
onlyttZ=false

print ""
print"**********	SELECTED OPTIONS FOR THIS EVALUATION	**********"
print "inputfiles="+inputfiles+"	myMethodList="+myMethodList+"		Lum="+str(Lum)+"		selection="+selection+"	onlyttZ="+str(onlyttZ)+"		weight="+weight
print"**********	SELECTED OPTIONS FOR THIS EVALUATION	**********"
print ""

auto_TMVAClassificationApplication( myMethodList, Lum, inputfiles, inputXML, selection, weight, outfolder )

print ""
print "********************		DONE		********************"
print ""
