export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

setupATLAS --quiet

rootVersion=5.34.25-x86_64-slc6-gcc48-opt
#rootVersion=6.04.02-x86_64-slc6-gcc48-opt #for compatibility with TMVA-agile
localSetupROOT $rootVersion --quiet
echo "==> Setting up ROOT version: "$rootVersion
