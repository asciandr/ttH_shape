Run MC toys 
---------
Setup root environment::

  . setup.sh

Run as many toys as you want to::

  root -l -q runMyToys.cxx+ >& log &
  tail -f log
