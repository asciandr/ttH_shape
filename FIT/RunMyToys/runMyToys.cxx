//**********************************************************************************
//////////////////////////////////////////////////////
//// Author: A.Sciandra<andrea.sciandra@cern.ch> ////
////////////////////////////////////////////////////
//**********************************************************************************

#include "TFile.h"
#include "TH1.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "RooRandom.h"
#include "RooStats/ModelConfig.h"
#include "RooStats/ToyMCSampler.h"
#include "RooStats/MinNLLTestStat.h"

using namespace RooStats;

void runMyToys( int n_toys=10000, bool verbose=false, bool cond=true, bool doTTH=true, bool byHand=false ) {

  std::cout<<std::endl; std::cout<<"	*** ---->  Going to run "<< n_toys <<" toys"<<std::endl<<std::endl;
  // Set seed for a "TRandom-like" object (RooRandom as static interface to TRandom)
  RooRandom::randomGenerator()->SetSeed(0);
  // Define input Workspace path+name
  TString fileName = doTTH ? "/afs/cern.ch/user/a/asciandr/www/ttH/outputs/V6_02_fourLepton/RooStats/V6_02_fourLepton_combined_V6_02_fourLepton_model.root" : "/afs/cern.ch/user/a/asciandr/work/WWZ/TRExFitter/FitModel_A/RooStats/FitModel_A_combined_FitModel_A_model.root";
  //TString fileName = doTTH ? "/afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/fixed_ttZGen_fourLepton/RooStats/fixed_ttZGen_fourLepton_combined_fixed_ttZGen_fourLepton_model.root" : "/afs/cern.ch/user/a/asciandr/work/WWZ/TRExFitter/FitModel_A/RooStats/FitModel_A_combined_FitModel_A_model.root";
  //TString fileName = doTTH ? "/afs/cern.ch/user/a/asciandr/work/ttH/multiGFW2/fit/TRExFitter/FOURLEP/RooStats/ttHML_combined_ttHML_model.root" : "/afs/cern.ch/user/a/asciandr/work/WWZ/TRExFitter/FitModel_A/RooStats/FitModel_A_combined_FitModel_A_model.root";
  //TString fileName = doTTH ? "/afs/cern.ch/user/a/asciandr/work/ttH/multiGFW2/fit/TRExFitter/ZENR_FOURLEP/RooStats/ttHML_combined_ttHML_model.root" : "/afs/cern.ch/user/a/asciandr/work/WWZ/TRExFitter/FitModel_A/RooStats/FitModel_A_combined_FitModel_A_model.root";
  //TString fileName = doTTH ? "/afs/cern.ch/user/a/asciandr/work/ttH/multiGFW2/fit/TRExFitter/OneHundred_FOURLEP/RooStats/ttHML_combined_ttHML_model.root" : "/afs/cern.ch/user/a/asciandr/work/WWZ/TRExFitter/FitModel_A/RooStats/FitModel_A_combined_FitModel_A_model.root";
  TFile file(fileName, "read");
  // Check input file
  if (file.IsZombie()) {
    std::cerr<<"PLEASE CHECK your input workspace! Aborting..."<<std::endl;
    return;
  }
  std::cout<<std::endl; std::cout<<"	*** ---->  Getting 'combined' ws and model configuration from this input file: "<<std::endl<<std::endl<< file.GetName() <<std::endl<<std::endl;
  // The RooWorkspace is a persistable container for RooFit projects
  // Get RooStats workspace and its model configuration
  auto ws = (RooWorkspace*) file.Get("combined");
  auto mc = (ModelConfig*)  ws->obj("ModelConfig");
  //Printing all kind of info on ws and mc
  if(verbose) {
    // Workspace info
    std::cout<<std::endl; std::cout<<"    ********* VERBOSE MODE *********    "<<std::endl<<std::endl;
    std::cout<<std::endl; std::cout<<"    *** ----> Printing all information relative to the ws and the model config"<<std::endl<<std::endl;
    std::cout<<std::endl; std::cout<<"List of all dataset in the workspace: "<<std::endl<<std::endl;
    std::list<RooAbsData *> AllData = ws->allData();
    std::list<RooAbsData *>::const_iterator iterator;
    for (iterator = AllData.begin(); iterator != AllData.end(); ++iterator) (*iterator)->Print();
    std::cout<<std::endl; std::cout<<"Set with all function objects: "<<std::endl<<std::endl;
    ws->allFunctions().Print();
    std::cout<<std::endl; std::cout<<"List of all generic objects in the workspace: "<<std::endl<<std::endl;
    std::list<TObject *> AllGenericObjects = ws->allGenericObjects();
    std::list<TObject *>::const_iterator iterator_0;
    for (iterator_0 = AllGenericObjects.begin(); iterator_0 != AllGenericObjects.end(); ++iterator_0) (*iterator_0)->Print();
    std::cout<<std::endl; std::cout<<"List of all probability density function objects: "<<std::endl<<std::endl;
    ws->allPdfs().Print();
    std::cout<<std::endl; std::cout<<"List of all resolution model objects: "<<std::endl<<std::endl;
    ws->allResolutionModels().Print();
    std::cout<<std::endl; std::cout<<"List of all variable objects: "<<std::endl<<std::endl;
    ws->allVars().Print();
    std::cout<<std::endl; std::cout<<"Contents of the workspace: "<<std::endl<<std::endl;
    ws->Print();
    // Model config info 
    std::cout<<std::endl; std::cout<<"Multiline detailed information on model config pdf: "<<std::endl<<std::endl;
    if(mc->GetPdf() != NULL) 		(*mc->GetPdf())	.printMultiline(std::cout,kTRUE);
    std::cout<<std::endl; std::cout<<"Multi line detailed information on prior pdf: "<<std::endl<<std::endl;
    if(mc->GetPriorPdf() != NULL) 	(*mc->GetPriorPdf()).printMultiline(std::cout,kTRUE);
    else 				std::cout<<"NULL POINTER..."<<std::endl<<std::endl;
    std::cout<<std::endl; std::cout<<"Nuisance parameters of the model config: "<<std::endl<<std::endl;
    (*mc->GetNuisanceParameters()).Print();
  }
 
  std::cout<<std::endl; std::cout<<"	*** ---->  Get pdf from model config and define TestStatistic"<<std::endl<<std::endl;
  // MinNLLTestStat is an implementation of the TestStatistic interface that calculates the minimum value of the negative log likelihood function and returns it as a test statistic
  // Get model PDF from model config and feed it to the test statistic
  MinNLLTestStat *minNll = new MinNLLTestStat(*mc->GetPdf());
  // Choose Minuit2 minimizer (as for TRexFitter)
  minNll->SetMinimizer("Minuit2");
  // Strategy 0 as for TRexFitter and other few options
  minNll->SetStrategy(0);
  minNll->SetLOffset(true);
  if(verbose) minNll->SetPrintLevel(3);
  minNll->SetReuseNLL(true);
  minNll->EnableDetailedOutput(true);
  if(verbose) std::cout<<std::endl; std::cout<<"MinNLLTestStat var name: "<< minNll->GetVarName() <<std::endl<<std::endl;;

  std::cout<<std::endl; std::cout<<"	*** ---->  Set toy sampler"<<std::endl<<std::endl;
  // A ToyMCSampler generates Toy Monte Carlo for a given parameter point and evaluates a TestStatistic
  ToyMCSampler sampler;
  // Set pdf, observables and n. of toys (you want to generate) for new toy MC sampler
  sampler.SetPdf(*mc->GetPdf());
  sampler.SetObservables(*mc->GetObservables());
  sampler.SetNuisanceParameters(*mc->GetNuisanceParameters());
  sampler.SetNToys(n_toys);
  // Global observables?
  if (!cond) {
    sampler.SetGlobalObservables(*mc->GetGlobalObservables());
  }
  // Set POI
  sampler.SetParametersForTestStat(*mc->GetParametersOfInterest());
  sampler.AddTestStatistic(minNll);

  std::cout<<std::endl; std::cout<<"	*** ---->  Define set of POIs and generate the samplings"<<std::endl<<std::endl;
  // Define set of POIs and get the samplings
  RooArgSet* POIset = (RooArgSet*) ws->set("ModelConfig_POI")->Clone();
  // RooDataSet is a container class to hold unbinned data. Each data point in N-dimensional space is represented by a RooArgSet of RooRealVar, RooCategory or RooStringVar objects
  // Calling GetSamplingDistributions corresponds to the following loop:
  //////////////////////////////////////////////////////////////////
  // for(unsigned int i=0; i<n_toys; i++) {			  //
  //    RooAbsData *toyData = sampler.GenerateToyData(*POIset);   //
  //    sampler.EvaluateTestStatistic(*toyData,*POIset);          //
  // }   							  //
  //////////////////////////////////////////////////////////////////
  RooDataSet* sd = sampler.GetSamplingDistributions(*POIset);
  if(verbose) { std::cout<<std::endl; std::cout<<"RooDataSet from ToyMcSampler info: "<<std::endl<<std::endl; sd->printMultiline(std::cout,kTRUE); std::cout<<std::endl; }


  // Further code for "by-hand" sampling and for POI, LL scan and so on...   
  TH1* hnll = new TH1F("nll", "nll", 100, -5, 5);
  TH1* hpoi = new TH1F("poi", "poi", 100, -1, 5);
  const int nToy = 10, nScan = 30;
  double scanPOI[nScan], scanNLL[nScan], scanNLLsum[nScan];
  if(byHand) {
    std::cout<<std::endl; std::cout<<"    *** ---->  Going to run toys 'by-hand' !!!"<<std::endl<<std::endl; 
    RooArgSet *paramPoint = (RooArgSet*) POIset->snapshot();
    std::cout<<std::endl; std::cout<<"paramPoint multi line info: "<<std::endl<<std::endl;
    paramPoint->printMultiline(std::cout,kTRUE);
    for (long int iToy = 0; iToy < nToy; iToy++) {
      if (iToy % (nToy/101+1) == 0) {
        std::cout<<std::endl; std::cout<<"Toy n. "<< iToy+1 <<" of "<< nToy <<std::endl<<std::endl;
      }
      TString POIname = doTTH ? "mu_XS_ttH" : "mu_XS_WWZ";
      RooRealVar* POIvar = (RooRealVar*) paramPoint->find("mu_XS_ttH");
      if(POIvar == NULL) {  std::cout<<std::endl; std::cout<<"Poivar is NULL POINTER..."<<std::endl<<std::endl; continue; }
      //POIvar -> setVal(1);
      //POIvar -> setConstant(false);
      RooArgSet *allVars = new RooArgSet(*(RooArgSet*) mc->GetObservables()->snapshot());
      std::cout<<std::endl; std::cout<<"allVars multi line info: "<<std::endl<<std::endl;
      allVars -> add(*POIvar);
      allVars -> dump();
      allVars->printMultiline(std::cout,kTRUE); std::cout<<std::endl;
      Double_t weight = 1.0;
      RooAbsData* toydata = sampler.GenerateToyData(*paramPoint, weight);
      std::cout<<std::endl; std::cout<<"toydata multi line info: "<<std::endl<<std::endl;
      toydata->printMultiline(std::cout,kTRUE); std::cout<<std::endl;
      
      std::cout<<std::endl; std::cout<<"    *** ---->  Evaluate test statistic"<<std::endl<<std::endl;
      double ts = sampler.EvaluateTestStatistic(*toydata, *allVars);
      std::cout << "TS:   " << ts << std::endl;
      std::cout << "POI: 	" << POIvar -> getVal() << std::endl;

      int extrema = 0;
      bool poiIncreasing = false;
      float lSum = 0;
      for (int iScan = 0; iScan < nScan; iScan++) {
        float lower = -2;
        float upper = 8;
        float step = (upper - lower) / nScan;
        float poiVal = lower + (iScan+1) * step;
        std::cout<<"Scan: "<< iScan <<"	poiVal: "<< poiVal <<std::endl;
        POIvar -> setVal(poiVal);
        float nllVal = -sampler.EvaluateTestStatistic(*toydata, *paramPoint);
        if (iToy == 0) {
          scanPOI[iScan] = poiVal;
          scanNLLsum[iScan] = 0;
        }
        lSum += exp(-nllVal);
        scanNLL[iScan] = nllVal;
        if (iScan > 0) {
          if (scanNLL[iScan] > scanNLL[iScan-1]) {
            if (!poiIncreasing) extrema++;
            poiIncreasing = true;
          } else {
            if (poiIncreasing) extrema++;
            poiIncreasing = false;
          }
        }
        //std::cout << "poi: " << poiVal << " NLL: " << nllVal << std::endl;
      }
      //std::cout << std::endl;
      //toydata->Print();
      //paramPoint -> dump();
      //allVars->dump();
      //return;
      //std::cout << "L sum: " << lSum << std::endl;
      if (!(extrema > 1)) {
        float poiMean = 0;
        for (int iScan = 0; iScan < nScan; iScan++) {
          //scanNLLsum[iScan] += scanNLL[iScan] + log(lSum);
          scanNLLsum[iScan] += scanNLL[iScan];
          poiMean += scanPOI[iScan] * exp(-scanNLL[iScan]);
        }
        hpoi -> Fill(poiMean / lSum);
      } else {
        std::cout << "Skip!" << std::endl;
      }

      //RooPlot* frame = POIvar -> frame();
      //RooAbsReal* nll = mc->GetPdf()->createNLL(*toydata);
      //nll->plotOn(frame);
      //frame->Draw();
      
      hnll->Fill(ts);
//      poi->Fill(POIvar->getVal());
      
      //TCanvas* c = new TCanvas();
      //paramPoint -> first() -> plotOn(c);
      //minNll->plotOn(c);

      delete paramPoint;
      delete allVars;
      delete toydata;
      //delete nll;

    }

  }

  TGraph* scan = new TGraph(nScan, scanPOI, scanNLLsum);
  scan -> SetMarkerStyle(7);
  if(byHand) scan -> Draw("APL");

  if(byHand) { 
    TCanvas* c = new TCanvas();
    hnll -> DrawCopy();
    c = new TCanvas();
    hpoi -> DrawCopy();
  }

  std::cout<<std::endl; std::cout<<"	 *** ----> Writing into output file"<<std::endl<<std::endl;
  
  TString outName = "output/toy";
  if (cond) outName += "_cond";
  else      outName += "_NOcond";
  outName += ".root";
  TFile fileOut(outName, "recreate");
  TTree *toyTree = RooStats::GetAsTTree("toyTree", "toyTree", *sd);
  toyTree->Write();

  if(byHand) {
    scan -> Write();
    hnll -> Write();
    hpoi -> Write();
  }

  fileOut.Close();
}
