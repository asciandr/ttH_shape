#!/bin/bash

pwd
workdir=`pwd`
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#localSetupROOT --skipConfirm
ls -haltr /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/superSuperSkim_fit_l4_enrZ_l4_depZ.config.txt
#cat /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/superSuperSkim_fit_l4_enrZ_l4_depZ.config.txt
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/*.C 						.
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/*.h 						.
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/*.sh 						.
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/*.py 						.
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/TtHFitter/ 					.
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/Root/						.
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/util/ 						.
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/Makefile 					.
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/superSuperSkim_fit_l4_enrZ_l4_depZ.config.txt 	.
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/replacement.txt 				.
#mkdir ttHML_inputs/
ls -haltr
. setup.sh
make
./myFit.exe n superSuperSkim_fit_l4_enrZ_l4_depZ.config.txt
cp -rf SUPERSUPERSKIM_fit_l4_enrZ_l4_depZ/ /afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/batch_result 
echo 'done.'
