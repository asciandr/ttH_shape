# --------------- %
# ---  JOB    --- %
# --------------- %

Job: "STUDY_looseRegion_fakeNtuples"
  CmeLabel: "13 TeV"
  POI: "ttZ_norm"
%  POI: "HEAVY_mu_norm"
%  POI: "HEAVY_el_norm"
  ReadFrom: NTUP
  NtuplePaths: "/afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/Fakes/looseRegion_fakeNtuples/"
%  Label: "     "
  LumiLabel: "36.1 fb^{-1}"
%  MCweight: "mcWeightOrg*xsec*(1./totweight)*pileupEventWeight_090*lepSFTrigTight*lepSFObjTight*tauSFTight*JVT_EventWeight*SherpaNJetWeight"
%  v27 luminosity
  Lumi: 36074.6 
%   LumiScale: 10    % use this to scale on the fly (i.e. after option "n" or "h") MC samples - e.g. for fast projections
  Selection: "1"
  PlotOptions: "YIELDS"
%  PlotOptions: "NOSIG,OVERSIG"
  NtupleName: "nominal"
  DebugLevel: 1
  ReplacementFile: Common_XS_unc.txt 
%  MCstatThreshold: 0.01 %Increase to 0.5-0.9 for stat only
  MCstatThreshold: 0.5  %Increase to 0.5-0.9 for stat only
%  MCstatThreshold: 0.9   %Increase to 0.5-0.9 for stat only
%  SystControlPlots: TRUE
%  SystPruningShape: 0.0005
%  SystPruningNorm: 0.001
  CorrelationThreshold: 0.10
  RankingMaxNP: 15
  HistoChecks: NOCRASH
  SplitHistoFiles: TRUE   % set this in order to have separated .root files in Histograms/ for different regions
  ImageFormat: "png"
%  ImageFormat: "png,eps,pdf"
%  StatOnly: TRUE
%  BlindingThreshold: 0.05
%  SystLarge: 0.90


% --------------- %
% ---  FIT    --- %
% --------------- %

Fit: "fit"
  UseMinos: "all"
%  FitType: SPLUSB
  FitType: BONLY
%  FitRegion: CRSR
  FitRegion: CRONLY
  POIAsimov: 1
%  POIAsimov: 0
%  FitBlind: TRUE
%  doLHscan: "mu_XS_ttZ"

% --------------- %
% ---  LIMIT  --- %
% --------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: TRUE

% --------------- %
% --- REGIONS --- %
% --------------- %

%%%% VALIDATION REGIONS %%%%
Region: "oneBin"; "MET"; "MT"; "MET_MT"; "leadJetPt"; "best_Z_Mll"; "OSlepPt"
  Type: VALIDATION
  Variable: "1.",1,0.5,1.5; "MET_RefFinal_et/1000.",20,0.,200.; "MT",15,0.,150.; "MET_RefFinal_et/1000.+MT",12,0.,300.; "lead_jetPt/1000.",10,0.,100.; "best_Z_Mll/1000.",10,80.,100.; "lep_Pt_0/1000.",10,20.,200. 
  VariableTitle: "event count"; "MET [GeV]"; "m_{T} [GeV]"; "MET + m_{T}  [GeV]"; "lead jet p_{T} [GeV]"; "best Z M_{ll} [GeV]"; "OS lepton pT [GeV]"
  Label: "inclusive CR"
  ShortLabel: "inclusive CR"  
  %%Z+j && ttbar
  %Selection: "(@m_jet_pt.size()==1||@m_jet_pt.size()==2)"
  %%optimising Z+j && ttbar selection...
  %Selection: "(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&((((lepton_ID[0]+lepton_ID[1]==0&&abs(Mll01/1000.-91.2)<10.)||(lepton_ID[0]+lepton_ID[2]==0&&abs(Mll02/1000.-91.2)<10.))&&(MET_RefFinal_et/1000.+MT<50.)&&(lead_jetPt/1000.<50.))||(!((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0)))&&(lead_jetPt/1000.>30.))"
  %Selection: "(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&((((lepton_ID[0]+lepton_ID[1]==0&&abs(Mll01/1000.-91.2)<10.)||(lepton_ID[0]+lepton_ID[2]==0&&abs(Mll02/1000.-91.2)<10.))&&(MET_RefFinal_et/1000.<50.&&MT<50.))||(!((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0))))"
  %Selection: "(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&((((lepton_ID[0]+lepton_ID[1]==0&&abs(Mll01/1000.-91.2)<10.)||(lepton_ID[0]+lepton_ID[2]==0&&abs(Mll02/1000.-91.2)<10.)))||(!((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0))))"
  %%Z+j
  %Selection: "(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&((lepton_ID[0]+lepton_ID[1]==0&&abs(Mll01/1000.-91.2)<10.)||(lepton_ID[0]+lepton_ID[2]==0&&abs(Mll02/1000.-91.2)<10.))&&(MET_RefFinal_et/1000.+MT<50.)"
  %Selection: "(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&((lepton_ID[0]+lepton_ID[1]==0&&abs(Mll01/1000.-91.2)<10.)||(lepton_ID[0]+lepton_ID[2]==0&&abs(Mll02/1000.-91.2)<10.))&&(MET_RefFinal_et/1000.<50.&&MT<50.)"
  Selection: "(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&((lepton_ID[0]+lepton_ID[1]==0&&abs(Mll01/1000.-91.2)<10.)||(lepton_ID[0]+lepton_ID[2]==0&&abs(Mll02/1000.-91.2)<10.))&&MT<30."
  %%ttbar
  %Selection: "(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&!((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0))&&(lead_jetPt/1000.>30.)"
  %Selection: "(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&!((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0))"
  DataType:"DATA"

%%%Region: "eee_CR-njets"; "eee_CR-lep0_pt"; "eee_CR-lep0_eta"; "eee_CR-jet0_pt"; "eee_CR-MET"; "eee_CR-bjets" 
%%%  Type: VALIDATION
%%%  Variable: "@m_jet_pt.size()",10,-0.5,9.5; "lep_Pt_0/1000.",10,0.,200.; "lep_Eta_0",16,-4.,4.; "lead_jetPt/1000.",2,0.,100.; "MET_RefFinal_et/1000.",5,0.,200.; "Sum$(m_jet_flavor_weight_MV2c10>0.8244)",10,-0.5,9.5
%%%  VariableTitle: "n jets"; "lead l p_{T} [GeV]"; "lead l eta"; "lead jet p_{T} [GeV]"; "MET [GeV]"; "b jets" 
%%%  Label: "eee CR"
%%%  ShortLabel: "eee CR"
%%%  Selection: "(Region==0)&&(@m_jet_pt.size()==1||@m_jet_pt.size()==2)"
%%%%  Selection: "Region==0"
%%%  DataType:"DATA"

% --------------- %
% --- SAMPLES --- %
% --------------- %

Sample: "ttH"
  Type: SIGNAL
  Title: "t#bar{t}H"
  NtupleFiles: "FAKE_ttH_aMcNloP8"
  FillColor: 2
  LineColor: 1
  MCweight: "mcWeightOrg*xsec*(1./totweight)*pileupEventWeight_090*lepSFTrigLoose*lepSFObjLoose*JVT_EventWeight*SherpaNJetWeight"

Sample: "ttZ"
  Type: BACKGROUND
  Title: "t#bar{t}Z"
  NtupleFiles:"FAKE_ttZ_aMcNloP8*"
  FillColor: 4
  LineColor: 1
  MCweight: "mcWeightOrg*xsec*(1./totweight)*pileupEventWeight_090*lepSFTrigLoose*lepSFObjLoose*JVT_EventWeight*SherpaNJetWeight"

Sample: "ttW"
  Type: BACKGROUND
  Title: "t#bar{t}W"
  NtupleFiles:"FAKE_ttW_aMcNloP8*"
  FillColor: 797
  LineColor: 1
  MCweight: "mcWeightOrg*xsec*(1./totweight)*pileupEventWeight_090*lepSFTrigLoose*lepSFObjLoose*JVT_EventWeight*SherpaNJetWeight"

Sample: "VV"
  Type: BACKGROUND
  Title: "VV"
  NtupleFiles:"FAKE_diboson"
%  NtupleFiles:"FAKE_diboson_bfilter"
  FillColor: 428
  LineColor: 1
  MCweight: "mcWeightOrg*xsec*(1./totweight)*pileupEventWeight_090*lepSFTrigLoose*lepSFObjLoose*JVT_EventWeight*SherpaNJetWeight"

Sample: "single_t"
  Type: BACKGROUND
  Title: "single top"
  NtupleFiles:"FAKE_singletop"
  FillColor: 4
  LineColor: 1
  MCweight: "mcWeightOrg*xsec*(1./totweight)*pileupEventWeight_090*lepSFTrigLoose*lepSFObjLoose*JVT_EventWeight*SherpaNJetWeight"

Sample: "Other"
  Type: BACKGROUND
  Title: "other"
  NtupleFiles:"FAKE_rare"
  FillColor: 430
  LineColor: 1
  MCweight: "mcWeightOrg*xsec*(1./totweight)*pileupEventWeight_090*lepSFTrigLoose*lepSFObjLoose*JVT_EventWeight*SherpaNJetWeight"

Sample: "Wjets"
  Type: BACKGROUND
  Title: "W+jets"
  NtupleFiles:"FAKE_Wjets_NNPDF"
  FillColor: 4
  LineColor: 1
  MCweight: "mcWeightOrg*xsec*(1./totweight)*pileupEventWeight_090*lepSFTrigLoose*lepSFObjLoose*JVT_EventWeight*SherpaNJetWeight"

Sample: "ttbar"
  Type: BACKGROUND
  Title: "t#bar{t}"
%  NtupleFiles:"FAKE_ttbar_nonallhad"
%  NtupleFiles:"FAKE_ttbar*"
  NtupleFiles:"FAKE_ttbar_2lep_PP8"
  FillColor: 3
  LineColor: 1
  MCweight: "mcWeightOrg*xsec*(1./totweight)*pileupEventWeight_090*lepSFTrigLoose*lepSFObjLoose*JVT_EventWeight*SherpaNJetWeight"

Sample: "Zjets"
  Type: BACKGROUND
  Title: "Z+jets"
  NtupleFiles:"FAKE_Zjets_NNPDF"
  FillColor: 3
  LineColor: 1
  MCweight: "mcWeightOrg*xsec*(1./totweight)*pileupEventWeight_090*lepSFTrigLoose*lepSFObjLoose*JVT_EventWeight*SherpaNJetWeight"

Sample: "Data"
  Title: "Data"
  Type: DATA
  NtupleFile: "FAKE_data"

% ------------------- %
% --- SYSTEMATICS --- %
% ------------------- %

NormFactor: "ttZ_norm"
  Title: "k(t#bar{t}Z)"
  Nominal: 1 
  Min: 1 
  Max: 1
  Samples: ttZ 
  Constant: TRUE
%  Exclude: "mmm_CR","eem_CR" 

%%%NormFactor: "HEAVY_el_norm"
%%%  Title: "el_norm"
%%%%  Title: "el_H_el"
%%%  Nominal: 1
%%%  Min: 0
%%%  Max: 5
%%%%  Samples: H_el 
%%%  Samples: H_el,O_el 
%%%%  Constant: TRUE
%%%  Exclude: "mmm_CR"
%%%
%%%%%%NormFactor: "OTHER_el_norm"
%%%%%%  Title: "el_O_el"
%%%%%%  Nominal: 1
%%%%%%  Min: 0
%%%%%%  Max: 5
%%%%%%  Samples: O_el
%%%%%%%  Constant: TRUE
%%%%%%  Exclude: "mmm_CR"
%%%%%%%  Exclude: "mmm_CR","eem_CR"
%%%
%%%%NormFactor: "HEAVY_mu_norm"
%%%NormFactor: "mu_norm"
%%%%  Title: "mu_H_mu"
%%%  Title: "mu_norm"
%%%  Nominal: 1 
%%%  Min: 0
%%%  Max: 5
%%%%  Samples: H_mu
%%%  Samples: H_mu,O_mu
%%%%%  Constant: TRUE
%%%  Exclude: "eee_CR"
%%%
%%%%%%NormFactor: "OTHER_mu_norm"
%%%%%%  Title: "mu_O_mu"
%%%%%%  Nominal: 1
%%%%%%  Min: 0
%%%%%%  Max: 5
%%%%%%  Samples: O_mu
%%%%%%%  Constant: TRUE
%%%%%%  Exclude: "mu_ttbar_CR"
%%%
