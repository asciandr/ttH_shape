Run several TRexFitter fits in batch
---------
This example shows how to run in batch several fits with a different BDT cut, but in principle any other parameter can be varied. Make sure the directory is clean (especially old ``LSFJOB_*`` folders)::

  . clean.sh

Choose in ``bdt_range.txt`` the range (in this case) for the BDT cuts and the steps; then produce all cuts::

  python produce_BDTcuts.py bdt_range.txt

and submit all jobs to the batch::	

  ./submit_fits.sh allBDTcuts.txt

Once all bjobs are done find the configuration maximising the expected median significance:: 

  python find_bestS.py

If you want to plot the expected median significance as a function of the BDT cut turn on the python flag ``plotit`` and ``stupidPlot.C`` will do it for you.
