#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

#Name of the BDT variable
#SF BDT
#bdt_name="BDT0"
#DF BDT
#bdt_name="BDT1"
#ttH->4l
bdt_name="MVA4lBonn_weight_ttZ"

#Python flag -> do you want to plot median significance asaf of BDT cut?
#plotit=False
plotit=True
if plotit:
    rootsys = str(os.environ['ROOTSYS'])

#Playing with logfiles
raw_outfile="raw_sig.txt"
outfile="sig.txt"
print "Writing exp median significances into '"+raw_outfile+"'"
command="grep -R -i \"Median significance: \" LSFJOB_*/STDOUT >& "+raw_outfile
os.system(command)
command2="cat "+raw_outfile+" | perl -pe 's/\e([^\[\]]|\[.*?[a-zA-Z]|\].*?\a)//g' | col -b > "+outfile
os.system(command2)

#Find optimal configuration
maximum_S=0
maximum_index=""
maximum_job=""
jobs=[]
sigmas=[]
print "Opening .txt file '"+outfile+"'"
txt_file  = open(outfile,"r")
for i,line in enumerate(txt_file):
    indexA=line.find('Median significance:\t')+len('Median significance:\t')
    indexC=line.find('_')+1
    indexD=line.find('/')
    jobs.append(line[indexC:indexD])
    sigmas.append(line[indexA:len(line)])
    if float(line[indexA:len(line)]) > maximum_S:
        maximum_S=float(line[indexA:len(line)])
        maximum_index=i+1
        maximum_job=line[indexC:indexD]

print "I found the maximum significance: "+str(maximum_S)+" obtained for combination on line "+str(maximum_index)+" and job n. "+maximum_job

best_joblog="LSFJOB_"+maximum_job+"/STDOUT"
print "Opening .txt file '"+best_joblog+"'"
job_file=open(best_joblog,"r")
count=0
for line in job_file:
    if count!=0:
        break
    if not line.find(bdt_name+" > ") != -1:
        continue
    else:
        count=count+1
        print ""
        print "***********************************************************************************"
        print "**************************	OPTIMAL CONFIGURATION	**************************"
        #print line[line.find(bdt_name+" > "):line.find(bdt_name+" > ")+15]
        print line[line.find(bdt_name+" > "):line.find(bdt_name+" > ")+26]
        print "**************************	OPTIMAL CONFIGURATION	**************************"
        print "***********************************************************************************"
        print ""

if not plotit:
    print "You decided not to plot sigmas..."
    sys.exit(1)
    pass
    
#Plot sigmas vs BDT cut
cuts_sigmas_txt="sigmasVScuts.txt"
#outFile_name="graph_sigmasVScuts.eps"
outFile_name="graph_sigmasVScuts.C"
outFile_name_pdf="graph_sigmasVScuts.pdf"
cuts=[]

rootInput=open(cuts_sigmas_txt,"w")

for j,job in enumerate(jobs):
    joblog_jg="LSFJOB_"+job+"/STDOUT"
    job_file_j=open(joblog_jg,"r")
    for l,line in enumerate(job_file_j):
        #if line.find("Creating the output file: MYFIT_")==-1:
        #if line.find("Creating the output file: ttH4L_")==-1:
        if line.find("Creating the output file: fourLepton_")==-1:
            continue
        else:
            #indexA=line.find("Creating the output file: MYFIT_")+len("Creating the output file: MYFIT_")
            #indexA=line.find("Creating the output file: ttH4L_")+len("Creating the output file: ttH4L_")
            indexA=line.find("Creating the output file: fourLepton_")+len("Creating the output file: fourLepton_")
            line2=line[indexA:len(line)]
            indexB=line2.find("/")
            cuts.append(line2[0:indexB])
            rootInput.write(cuts[j]+"\t;"+sigmas[j])
#            print "BDT cut: "+str(cuts[j])+" and Sigma: "+str(sigmas[j])+" for job n. "+jobs[j]
rootInput.close()

print "Compiling macros..."
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L stupidPlot.C+")
if cmpFailure:
    print "Compilation failure, aborting..."
    sys.exit(1)
    pass

stupidPlot( cuts_sigmas_txt, outFile_name )
stupidPlot( cuts_sigmas_txt, outFile_name_pdf )

print ""
print "**********************************************************************"
print "**************************       I'M DONE   **************************"
print "**********************************************************************"
print ""
