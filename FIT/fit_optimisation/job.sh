#!/bin/bash

PACKAGE_folder=/afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/FIT/fit_optimisation/
TREX_folder=/afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/

ls -haltr ${PACKAGE_folder}/BDT_CUT_PLACE_HERE_THE_CUT.config

cp -rf ${TREX_folder}/*.C 						.
cp -rf ${TREX_folder}/*.h 						.
cp -rf ${TREX_folder}/*.sh 						.
cp -rf ${TREX_folder}/*.py 						.
cp -rf ${TREX_folder}/TtHFitter/ 					.
cp -rf ${TREX_folder}/Root/						.
cp -rf ${TREX_folder}/util/ 						.
cp -rf ${TREX_folder}/Makefile 						.

cp -rf ${PACKAGE_folder}/BDT_CUT_PLACE_HERE_THE_CUT.config          	.
cp -rf ${TREX_folder}/replacement_Maria.txt				.

ls -haltr
. setup.sh
make
./myFit.exe nwfs BDT_CUT_PLACE_HERE_THE_CUT.config
echo 'done.'
