#!/bin/usr/python 

from array import array
import sys
import os
import os.path
import time
from datetime import datetime

configFile 	= ""

try:
    configFile 	= str(sys.argv[1])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python produce_BDTcuts.py bdt_range.txt"
    sys.exit(1)
    pass

if not os.path.exists("./"+configFile): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass

#Retrieve input values
steps=""
mins=""
maxs=""

print "Opening .txt file '"+configFile+"'"
txt_file  = open(configFile,"r")
for line in txt_file:
    if(line.startswith("#")): 
        continue
    else:
        indexA=line.find('\t')
        steps=line[0:indexA]
        indexB=line.find(';')+1
        line2=line[indexB:len(line)]
        indexC=line2.find('\t')
        mins=line2[0:indexC]
        indexD=line2.find(';')+1
        line3=line2[indexD:len(line2)]
        indexE=line3.find(';')+1
        maxs=line3[indexE:line3.find('    ')]
        
print ""
print "List of steps, mins and maxs:"
print ""
print steps
print mins
print maxs
print ""

#Save all values btw mins and maxs
allvalues=[]

STEP	=float(steps)
MIN	=float(mins)
MAX	=float(maxs)
while (MIN<=MAX):
    allvalues.append(MIN)
    MIN=MIN+STEP
       
print allvalues

##Write all values into a txt files
outFolder=""
outFilename="allBDTcuts.txt"

myfile=open(outFilename,"w")
for a in allvalues:
    myfile.write(str(a)+'\n')  

myfile.close()
     
print ""
print "********************		ALL BDT CUTS ARE READY TO RUN TREXFITTER		********************"
print ""
