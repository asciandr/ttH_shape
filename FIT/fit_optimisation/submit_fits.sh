#!/bin/bash

listtxt=$*
COUNTER=0

while read config
do
    echo $config
### SF fit --- cut on SF BDT
#    cat BATCH_MODEL.config | sed "s/PLACE_HERE_THE_CUT/$config/g" > BDT_CUT_$COUNTER.config 
### SF+DF combination --- cut on SF BDT
#    cat BATCH_MODEL_comb.config | sed "s/PLACE_HERE_THE_CUT/$config/g" > BDT_CUT_$COUNTER.config 
### SF+DF combination --- cut on DF BDT
#    cat BATCH_MODEL_comb_DFbdtCut.config | sed "s/PLACE_HERE_THE_CUT/$config/g" > BDT_CUT_$COUNTER.config 
### ttH->4L paper
#    cat ttH4L_paper_BATCH_MODEL.config | sed "s/PLACE_HERE_THE_CUT/$config/g" > BDT_CUT_$COUNTER.config 
### R21 ttH->4L
    cat R21_ttH4L_BATCH_MODEL.config | sed "s/PLACE_HERE_THE_CUT/$config/g" > BDT_CUT_$COUNTER.config 
    cat job.sh | sed "s/PLACE_HERE_THE_CUT/$COUNTER/g" > job_$COUNTER.sh
    chmod 755 BDT_CUT_$COUNTER.config
    chmod 755 job_$COUNTER.sh
#    queue="1nh"
    queue="8nh"
    bsub -q $queue job_$COUNTER.sh
    COUNTER=$[$COUNTER+1]
done < $*
