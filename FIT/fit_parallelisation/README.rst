Run batch parallelisation of histogram creation (from ntuples) with TRexFitter
---------
This example shows how to speed up the production of input histogram (from ntuples), necessary to create the RooStats xmls and workspaces (which is much faster). Just check ``job.sh``, make sure that the ``PACKAGE_folder`` and ``TREX_folder`` are matching the folders where you have this package and the TRexFitter package, respectively. Also check that all needed files to run your config are being copied to the pool where the job is going to be run. At this point just run the bash script::

  ./split_fits.sh CONFIG.config

giving your TRex config as input, where the samples are defined in the format ``Sample: MY_SAMPLE``. After running, hadd the relevant histogram and smoothly run the options ``wf``. Sit back, relax and wait for your histograms to be there, ready to produce the workspace and run!

To split jobs in regions, very useful when ghost samples are defined and making the aforementioned splitting difficult, check ``split_regions_fits.sh`` out.
