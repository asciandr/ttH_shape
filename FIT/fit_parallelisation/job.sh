#!/bin/bash

PACKAGE_folder=/afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/FIT/fit_parallelisation/
TREX_folder=/afs/cern.ch/user/a/asciandr/work/ttH/hacked_TRExFitter/
#TREX_folder=/afs/cern.ch/user/a/asciandr/work/ttH/TRExFitter/

ls -haltr ${PACKAGE_folder}/CONFIG

cp -rf ${TREX_folder}/*.C 					.
cp -rf ${TREX_folder}/*.h 					.
cp -rf ${TREX_folder}/*.sh 					.
cp -rf ${TREX_folder}/*.py 					.
cp -rf ${TREX_folder}/TtHFitter/ 				.
cp -rf ${TREX_folder}/Root/					.
cp -rf ${TREX_folder}/util/ 					.
cp -rf ${TREX_folder}/Makefile 					.

cp -rf ${PACKAGE_folder}/CONFIG          			.
cp -rf ${TREX_folder}/replacement.txt				.
#cp -rf ${TREX_folder}/replacement_Maria.txt			.

ls -haltr
. setup.sh
make
./myFit.exe n CONFIG 'Samples=SAMPLE_NAME'
echo 'Histo creation done. Now copying...'
mkdir -p ${TREX_folder}/JOB_NAME/SAMPLE_NAME_Histograms/
mkdir -p ${TREX_folder}/JOB_NAME/SAMPLE_NAME_Systematics/
cp JOB_NAME/Histograms/* ${TREX_folder}/JOB_NAME/SAMPLE_NAME_Histograms/
cp -r JOB_NAME/Systematics/* ${TREX_folder}/JOB_NAME/SAMPLE_NAME_Systematics/
echo 'I AM DONE.'
