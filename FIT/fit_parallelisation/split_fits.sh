#!/bin/bash

listtxt=$*

JOBLINE=`cat $listtxt | grep Job: `
JOBNAME=`echo "$JOBLINE" | cut -d' ' -f 2`

echo "JOBLINE: $JOBLINE"
echo "JOBNAME: $JOBNAME"

while read config
do
    if [[ $config = Sample:* ]]; then
        echo ""
        echo "I have found a 'Sample'"
        echo "$config"
        SUBSTRING=`echo "$config" | cut -d' ' -f 2`
        echo "The sample is called $SUBSTRING"
        echo "Going to run $listtxt to produce histograms related to this sample"
        cat job.sh | sed "s/JOB_NAME/$JOBNAME/g" | sed "s/SAMPLE_NAME/$SUBSTRING/g" | sed "s/CONFIG/$listtxt/g" > job_$SUBSTRING.sh
        chmod 755 job_$SUBSTRING.sh
        queue="1nd"
        #queue="8nh"
        bsub -q $queue job_$SUBSTRING.sh
    fi
done < $*
