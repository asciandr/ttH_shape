#!/bin/bash

listtxt=$*

JOBLINE=`cat $listtxt | grep Job: `
JOBNAME=`echo "$JOBLINE" | cut -d' ' -f 2`

echo "JOBLINE: $JOBLINE"
echo "JOBNAME: $JOBNAME"

while read config
do
    if [[ $config = Region:* ]]; then
        echo ""
        echo "I have found a 'Region'"
        echo "$config"
        SUBSTRING=`echo "$config" | cut -d' ' -f 2`
        echo "The region is called $SUBSTRING"
        echo "Going to run $listtxt to produce histograms related to this region"
        cat regions_job.sh | sed "s/JOB_NAME/$JOBNAME/g" | sed "s/REGION_NAME/$SUBSTRING/g" | sed "s/CONFIG/$listtxt/g" > regions_job_$SUBSTRING.sh
        chmod 755 regions_job_$SUBSTRING.sh
        #queue="8nh"
        #queue="1nd"
        queue="2nd"
        bsub -q $queue regions_job_$SUBSTRING.sh
    fi
done < $*
