///////////////////////// -*- C++ -*- /////////////////////////////
// NEWfakeComputation.C
// Author: A.Sciandra <andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include <string.h>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <string>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>

#include "optimisation_FSF.h"

using namespace std      ;

//23-01-2017: redefine method w/o any fake candidate choice, look at the two merged CRs and separate them in 4 regions (eee,eem,emm,mmm), then assign to different classes according to the flavour of the fake (in truth MC record) and whther it is from light/heavy sources

int main() {
  //Flag to look at 3 loose leps
  bool loose(false);
  bool debug(false);//(true);

  clock_t begin       = clock();
  TString rel4l = "@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)";
  TString ICHEPSR4l = "@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(lepton_isolationGradient[0]==1&&lepton_isolationGradient[1]==1&&lepton_isolationGradient[2]==1&&lepton_isolationGradient[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(Mllll0123/1000>100.&&Mllll0123/1000<350.)&&(RunYear==2015||RunYear==2016)";
  TString SumQ2_4l = "@lepton_pt.size()==4&&(abs((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3]))==2)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(lepton_isolationGradient[0]==1&&lepton_isolationGradient[1]==1&&lepton_isolationGradient[2]==1&&lepton_isolationGradient[3]==1)&&(RunYear==2015||RunYear==2016)";

  //Define input folder
  TString inpFold = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v27/01/";
  //TString inpFold = "/afs/cern.ch/user/a/asciandr/work/ttH/loose_ntuples_v23/samples/";
  //Root files for looping
  // NEW ttbar PYTHIA 8!
  const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root","ttW_aMcNloP8.root","diboson_bfilter.root","rare.root","singletop.root","Wjets_NNPDF.root","ttbar_2lep_PP8.root","ttbar_3lep.root","Zjets_NNPDF.root"};//,"data.root"};
  //const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root","ttW_aMcNloP8.root","diboson_bfilter.root","rare.root","singletop.root","Wjets_NNPDF.root","ttbar_3lep.root","Zjets_NNPDF.root"};//,"data.root"};
  //const char*      files[] = {"ttbar_3lep.root"};
 
  /* Define leptons from heavy and light sources (prompt is what remains) */
  int heavy[10] = {6,8,9,25,26,27,28,29,32,33};
  int light[16] = {0,3,5,7,23,24,30,31,34,35,36,37,38,39,40,42};
  /* Define leptons from heavy and light sources (prompt is what remains) */
  
  cout<<endl;
  cout<<"   ///////////////////////// -*- C++ -*- /////////////////////////////"<<endl;
  cout<<"  // NEWfakeComputation.C                                          //"<<endl;
  cout<<" // Author: A.Sciandra <andrea.sciandra@cern.ch>                  //"<<endl;
  cout<<"///////////////////////////////////////////////////////////////////"<<endl;

  cout<<endl;
  
  const int                   NMAX = sizeof(files) / sizeof(files[0])+4;
  const int                   N = (sizeof(files) / sizeof(files[0]));
 
  TTree *skimTrees[N];
  vector<TFile*>              inputFile;
  vector<TTree*>              v_fChain;
  inputFile                   .reserve( NMAX );
  v_fChain                    .reserve( NMAX );

  cout<<endl;
  cout<<"** Looping on the following samples:                                    **"<<endl;
  cout<<endl;
  
  /* OUTPUT FILES */
  
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TFile *f = new TFile("f.root","RECREATE");
    //TString     string_i  = string()+files[i]   ;
    TString     string_i  = string()+inpFold+files[i]   ;
    TTree       *chain;
    inputFile   .push_back(new TFile(string_i));
    inputFile   .at(i)->GetObject("nominal",chain); 
    v_fChain    .push_back( chain );
    
    cout<< inputFile.at(i)->GetName() <<endl;
  }

  //TString PreSelection = string()+SumQ2_4l;
  TString PreSelection = string()+rel4l;
  //TString PreSelection = string()+ICHEPSR4l;

  cout<<endl;
  cout<<"** Applying the following preselection: **"<<endl;
  cout<<endl;
  cout<<"** '' "<< PreSelection <<" '' **"<<endl;
  cout<<endl;

  // Skimmed TTreeS
  vector<TTree*>     v_fChainSkim;
  vector<nominal*>   nTree       ;  
  v_fChainSkim       .reserve( NMAX );
  nTree              .reserve( NMAX );
    
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TFile *f = new TFile("f.root","RECREATE");
    v_fChainSkim .push_back( (TTree*)v_fChain.at(i)->CopyTree( PreSelection ) );
    skimTrees[i] = v_fChainSkim[i];
    nTree        .push_back( new nominal(v_fChainSkim.at(i))               );
  } 
  	
  /* Loop over the different samples */
  for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {
    cout<<endl;
    cout<<"****                    Processing "<< inputFile.at(t)->GetName() <<"                    ****"<<endl;
    cout<<endl;

    // Save skimmed tree + new branches
    string str; ostringstream temp; temp<<t; str=temp.str();
    TString fName = string()+"FAKE_"+files[t];
    cout<<"Output file name= "<< fName <<endl;
    TFile *Target = new TFile(fName,"RECREATE");
    TTree *newtree = (TTree*)v_fChainSkim.at(t)->CloneTree(0);
    Float_t isPrompt(-1);
    Int_t   FakeIndex(-1), Class(-1);
    newtree->Branch("FakeIndex", &FakeIndex, "FakeIndex/I");
    newtree->Branch("Class", &Class, "Class/I");

    Long64_t nentries = v_fChainSkim.at(t)->GetEntriesFast();

    /* Looping over the events */
    for (Long64_t ievt=0; ievt<nentries;ievt++) {
      //for (Long64_t ievt=49; ievt<50;ievt++) {
      bool goodOne(true);//(false);
      int  trash=nTree.at(t)->GetEntry(ievt);
      int  CLASS(-1.), FAKE_IND(-1);

      if(goodOne) {
	// Data event
	if( (((string)inputFile.at(t)->GetName()).find("data"))!=string::npos ) { CLASS=-99; FAKE_IND=-99; }
        
	bool HO(false), LO(false);
	for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++){
	  int ID = nTree.at(t)->lepton_ID->at(l);
	  int TO = nTree.at(t)->lepton_truthOrigin->at(l);
	  //Are there (truth) fake leps? Origin?
	  //Class: prompt (0), heavy/light el (1/2), heavy/light mu (3/4)
	  for(unsigned int i=0; i<sizeof(heavy)/sizeof(*heavy); i++) { 
	    if(TO==heavy[i]) { 
	      if(debug) cout<<"heavy: "<< TO <<endl; 
	      HO=true; FAKE_IND=l;
	      if(abs(ID)==11)      CLASS = 1; 
	      else if(abs(ID)==13) CLASS = 3;
              else cout<<"CHECK ME: lepton which is not an el nor a mu?!"<<endl;
	    } 
	  }
	  for(unsigned int i=0; i<sizeof(light)/sizeof(*light); i++) { 
	    if(TO==light[i]) { 
	      if(debug) cout<<"light: "<< TO <<endl; 
	      LO=true; FAKE_IND=l; 
	      if(abs(ID)==11)      CLASS = 2;
              else if(abs(ID)==13) CLASS = 4;
              else cout<<"CHECK ME: lepton which is not an el nor a mu?!"<<endl;
	    } 
	  }
	}//for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++)
	//Prompt events
	if((!HO)&&(!LO)) CLASS = 0;
	if((!HO)&&(!LO)&&(CLASS!=0)) { cout<<"**WARNING** Here I am and my TOs are:"<<endl; for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++) { cout<< nTree.at(t)->lepton_truthOrigin->at(l) <<endl; cout<<endl; } cout<<"next bad event"<<endl; }

	FakeIndex 	= FAKE_IND;
	Class		= CLASS;

	Target->cd();
	newtree->Fill();
      }//if(goodOne)
    }//for (Long64_t ievt=0; ievt<nentries;ievt++)
    Target->Write();

  }//for(files)
  //f->Write();

  cout<<endl; cout<<"******************** FINISHED ********************"<<endl; cout<<endl;

  return 0;
}//int main() 
