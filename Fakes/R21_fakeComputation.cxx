///////////////////////// -*- C++ -*- /////////////////////////////
// fakeComputation.C
// Author: A.Sciandra <andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include <string.h>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <string>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>

#include "R21_optimisation_FSF.h"

using namespace std      ;

//23-01-2017: redefine method w/o any fake candidate choice, look at the two merged CRs and separate them in 4 regions (eee,eem,emm,mmm), then assign to different classes according to the flavour of the fake (in truth MC record) and whether it is from light/heavy sources

int main() {
  //Flag to look at 3 loose leps
  bool loose(true);
  bool LooseRegion(true);
  bool debug(false);//true);

  clock_t begin       = clock();
  // new CRs with low jet multiplicity (1 or 2) //
  const char*      CRs[2];
  // R21
  CRs[0] = "( (trilep_type && (lep_isTrigMatch_0 || lep_isTrigMatch_1 || lep_isTrigMatch_2) && (abs(total_charge)==1) && (lep_isolationFixedCutLoose_0 && lep_isolationFixedCutLoose_1 && lep_isolationFixedCutLoose_2)) && (nJets_OR_T>=1) && ((((lep_ID_0+lep_ID_1==0 && abs(Mll01/1000.-91.2)<10.) || (lep_ID_0+lep_ID_2==0 && abs(Mll02/1000.-91.2)<10.)))) )";
  CRs[1] = "( (trilep_type && (lep_isTrigMatch_0 || lep_isTrigMatch_1 || lep_isTrigMatch_2) && (abs(total_charge)==1) && (lep_isolationFixedCutLoose_0 && lep_isolationFixedCutLoose_1 && lep_isolationFixedCutLoose_2)) && (nJets_OR_T>=1) && ( !((lep_ID_0+lep_ID_1==0) || (lep_ID_0+lep_ID_2==0)) ) )";
  //CRs[0] = "( (trilep_type && (lep_isTrigMatch_0 || lep_isTrigMatch_1 || lep_isTrigMatch_2) && (abs(total_charge)==1) && (lep_isolationFixedCutLoose_0 && lep_isolationFixedCutLoose_1 && lep_isolationFixedCutLoose_2)) && (nJets_OR_T==1 || nJets_OR_T==2) && ((((lep_ID_0+lep_ID_1==0 && abs(Mll01/1000.-91.2)<10.) || (lep_ID_0+lep_ID_2==0 && abs(Mll02/1000.-91.2)<10.)))) )";
  //CRs[1] = "( (trilep_type && (lep_isTrigMatch_0 || lep_isTrigMatch_1 || lep_isTrigMatch_2) && (abs(total_charge)==1) && (lep_isolationFixedCutLoose_0 && lep_isolationFixedCutLoose_1 && lep_isolationFixedCutLoose_2)) && (nJets_OR_T==1 || nJets_OR_T==2) && ( !((lep_ID_0+lep_ID_1==0) || (lep_ID_0+lep_ID_2==0)) ) )";
//  CRs[0] = "( (trilep_type && (lep_isTrigMatch_0 || lep_isTrigMatch_1 || lep_isTrigMatch_2) && (abs(total_charge)==1) && (lep_isolationFixedCutLoose_0 && lep_isolationFixedCutLoose_1 && lep_isolationFixedCutLoose_2)) && ((lep_ID_0+lep_ID_1==0 && abs(Mll01/1000.-91.2)<10.)||(lep_ID_0+lep_ID_2==0 && abs(Mll02/1000.-91.2)<10.)) && (nJets_OR_T>=1) && MET_RefFinal_et/1000.<50. )";
//  CRs[1] = "( (trilep_type && (lep_isTrigMatch_0 || lep_isTrigMatch_1 || lep_isTrigMatch_2) && (abs(total_charge)==1) && (lep_isolationFixedCutLoose_0 && lep_isolationFixedCutLoose_1 && lep_isolationFixedCutLoose_2)) && !((lep_ID_0+lep_ID_1==0) || (lep_ID_0+lep_ID_2==0)) && (abs((abs(lep_ID_0)/lep_ID_0)+(abs(lep_ID_1)/lep_ID_1)+(abs(lep_ID_2)/lep_ID_2))==1) && (nJets_OR_T>=1) && (Sum$(m_jet_pt/1000.>30.)>=1) )";
  // R20.7
  // gradient-isolated leptons
//  if (!loose && !LooseRegion) {
//    CRs[0] = "((trilep_type)&&(abs(total_charge)==1)&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatchDLT_0||lep_isTrigMatchDLT_1||lep_isTrigMatchDLT_2)&&((lep_ID_0+lep_ID_1==0&&abs(Mll01/1000.-91.2)<10.)||(lep_ID_0+lep_ID_2==0&&abs(Mll02/1000.-91.2)<10.))&&(nJets_OR_T>=1)&&(lep_promptLeptonIso_TagWeight_0<-0.5&&lep_promptLeptonIso_TagWeight_1<-0.5&&lep_promptLeptonIso_TagWeight_2<-0.5)&&(RunYear==2015||RunYear==2016)&&MET_RefFinal_et/1000.<50.)";
//    CRs[1] = "((trilep_type)&&(abs(total_charge)==1)&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatchDLT_0||lep_isTrigMatchDLT_1||lep_isTrigMatchDLT_2)&&!((lep_ID_0+lep_ID_1==0)||(lep_ID_0+lep_ID_2==0))&&(abs((abs(lep_ID_0)/lep_ID_0)+(abs(lep_ID_1)/lep_ID_1)+(abs(lep_ID_2)/lep_ID_2))==1)&&(nJets_OR_T>=1)&&(lep_promptLeptonIso_TagWeight_0<-0.5&&lep_promptLeptonIso_TagWeight_1<-0.5&&lep_promptLeptonIso_TagWeight_2<-0.5)&&(Sum$(m_jet_pt/1000.>30.)>=1)&&(RunYear==2015||RunYear==2016))";
//  }
//  else if(!loose && LooseRegion) {
//    CRs[0] = "((trilep_type)&&(abs(total_charge)==1)&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatchDLT_0||lep_isTrigMatchDLT_1||lep_isTrigMatchDLT_2)&&((lep_ID_0+lep_ID_1==0&&abs(Mll01/1000.-91.2)<10.)||(lep_ID_0+lep_ID_2==0&&abs(Mll02/1000.-91.2)<10.))&&(nJets_OR_T>=1)&&(lep_promptLeptonIso_TagWeight_1<-0.5&&lep_promptLeptonIso_TagWeight_2<-0.5)&&(RunYear==2015||RunYear==2016))";
//    CRs[1] = "((trilep_type)&&(abs(total_charge)==1)&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatchDLT_0||lep_isTrigMatchDLT_1||lep_isTrigMatchDLT_2)&&!((lep_ID_0+lep_ID_1==0)||(lep_ID_0+lep_ID_2==0))&&(abs((abs(lep_ID_0)/lep_ID_0)+(abs(lep_ID_1)/lep_ID_1)+(abs(lep_ID_2)/lep_ID_2))==1)&&(nJets_OR_T>=1)&&(lep_promptLeptonIso_TagWeight_1<-0.5&&lep_promptLeptonIso_TagWeight_2<-0.5)&&(RunYear==2015||RunYear==2016))";
//  }
//  // loose leptons and loose region
//  else {
//    CRs[0] = "((trilep_type)&&(abs(total_charge)==1)&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatchDLT_0||lep_isTrigMatchDLT_1||lep_isTrigMatchDLT_2)&&((lep_ID_0+lep_ID_1==0&&abs(Mll01/1000.-91.2)<10.)||(lep_ID_0+lep_ID_2==0&&abs(Mll02/1000.-91.2)<10.))&&(nJets_OR_T>=1)&&(RunYear==2015||RunYear==2016))";
//    CRs[1] = "((trilep_type)&&(abs(total_charge)==1)&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatchDLT_0||lep_isTrigMatchDLT_1||lep_isTrigMatchDLT_2)&&!((lep_ID_0+lep_ID_1==0)||(lep_ID_0+lep_ID_2==0))&&(abs((abs(lep_ID_0)/lep_ID_0)+(abs(lep_ID_1)/lep_ID_1)+(abs(lep_ID_2)/lep_ID_2))==1)&&(nJets_OR_T>=1)&&(RunYear==2015||RunYear==2016))";
//  }

  //Define input folder
  // R21
  TString inpFold = "/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN1/3L4L_skim_Peters_v5/";
  //TString inpFold = "/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN1/Fakes_looser_skim_v5/";
  //TString inpFold = "/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN1/Peters_skim_v5/MC16c/";
  // R20.7
//  TString inpFold = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v27/01/";
  //Root files for looping
  // R21
  const char*      files[] = {"data.root","ttbar_2lep_PP8.root","ttH.root","ttW.root","ttZ.root","VH.root","WW.root","WWZ.root","WZ.root","WZZ.root","Zgamma.root","Zjets.root","ZZ.root"};
  //const char*      files[] = {"data.root"};
  //const char*      files[] = {"singletop.root","ttbar_2lep_PP8.root","ttH.root","ttW.root","ttZ.root","WZ.root","Zjets.root","ZZ.root"};
  // R20.7
//  const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root","ttW_aMcNloP8.root","diboson.root","rare.root","singletop.root","Wjets_NNPDF.root","ttbar_2lep_PP8.root","Zjets_NNPDF.root","data.root"};
 
  /* Define leptons from heavy and light sources (prompt is what remains) */
  int heavy[10] = {6,8,9,25,26,27,28,29,32,33};
  int light[16] = {0,3,5,7,23,24,30,31,34,35,36,37,38,39,40,42};
  /* Define leptons from heavy and light sources (prompt is what remains) */
  
  cout<<endl;
  cout<<"   ///////////////////////// -*- C++ -*- /////////////////////////////"<<endl;
  cout<<"  // fakeComputation.C                                             //"<<endl;
  cout<<" // Author: A.Sciandra <andrea.sciandra@cern.ch>                  //"<<endl;
  cout<<"///////////////////////////////////////////////////////////////////"<<endl;

  cout<<endl;
  
  const int                   NMAX = sizeof(files) / sizeof(files[0])+4;
  const int                   N = (sizeof(files) / sizeof(files[0]));
 
  TTree *skimTrees[N];
  vector<TFile*>              inputFile;
  vector<TTree*>              v_fChain;
  inputFile                   .reserve( NMAX );
  v_fChain                    .reserve( NMAX );

  cout<<endl;
  cout<<"** Looping on the following samples:                                    **"<<endl;
  cout<<endl;
  
  /* OUTPUT FILES */
  
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TFile *f = new TFile("f.root","RECREATE");
    TString     string_i  = string()+inpFold+files[i]   ;
    TTree       *chain;
    inputFile   .push_back(new TFile(string_i));
    inputFile   .at(i)->GetObject("nominal",chain); 
    v_fChain    .push_back( chain );
    
    cout<< inputFile.at(i)->GetName() <<endl;
  }

  //Preselection: pass either Z+j or ttbar CR selection
  TString PreSelection = string()+CRs[0]+" || "+CRs[1];

  cout<<endl;
  cout<<"** Applying the following preselection: **"<<endl;
  cout<<endl;
  cout<<"** '' "<< PreSelection <<" '' **"<<endl;
  cout<<endl;

  // Skimmed TTreeS
  vector<TTree*>     v_fChainSkim;
  vector<nominal*>   nTree       ;  
  v_fChainSkim       .reserve( NMAX );
  nTree              .reserve( NMAX );
    
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TFile *f = new TFile("f.root","RECREATE");
    v_fChainSkim .push_back( (TTree*)v_fChain.at(i)->CopyTree( PreSelection ) );
    skimTrees[i] = v_fChainSkim[i];
    nTree        .push_back( new nominal(v_fChainSkim.at(i))               );
  } 
  	
  /* Loop over the different samples */
  for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {
    cout<<endl;
    cout<<"****                    Processing "<< inputFile.at(t)->GetName() <<"                    ****"<<endl;
    cout<<endl;

    // Save skimmed tree + new branches
    string str; ostringstream temp; temp<<t; str=temp.str();
    TString fName = string()+"FAKE_"+files[t];
    cout<<"Output file name= "<< fName <<endl;
    TFile *Target = new TFile(fName,"RECREATE");
    TTree *newtree = (TTree*)v_fChainSkim.at(t)->CloneTree(0);
    Float_t isPrompt(-1);
    Float_t MT(-99), FakeFlavour(-99.);
    Int_t   FakeClass(-1), FakeIndex(-1), Region(-1), nEl(0.), nMu(0.), Class(-1);
    Int_t fakeIndex(-1);
    newtree->Branch("isPrompt", &isPrompt, "isPrompt/F");
    newtree->Branch("MT", &MT, "MT/F");
    newtree->Branch("FakeIndex", &FakeIndex, "FakeIndex/I");
    newtree->Branch("FakeClass", &FakeClass, "FakeClass/I");
    newtree->Branch("FakeFlavour", &FakeFlavour, "FakeFlavour/F");
    //23-01-2017
    newtree->Branch("Region", &Region, "Region/I");
    newtree->Branch("nEl", &nEl, "nEl/I");
    newtree->Branch("nMu", &nMu, "nMu/I");
    newtree->Branch("Class", &Class, "Class/I");
    //05-05-2017
    newtree->Branch("fakeIndex", &fakeIndex, "fakeIndex/I");

    Long64_t nentries = v_fChainSkim.at(t)->GetEntriesFast();

    if(debug) cout<<"Starting loop..."<<endl;
    /* Looping over the events */
    for (Long64_t ievt=0; ievt<nentries;ievt++) {
      //for (Long64_t ievt=49; ievt<50;ievt++) {
      if(debug) cout<<"Event n. "<< ievt <<endl;
      int  n_CR(0.);
      bool goodOne(false);
      bool cr1(false), cr2(false);
      int  trash=nTree.at(t)->GetEntry(ievt);
      int  NEL(0.), NMU(0.), REGION(-1.), CLASS(-1.);
      int FAKEINDEX(-1);
    
      // Z+j or ttbar CR?

      // R21
      if(  (nTree.at(t)->trilep_type) && (abs(nTree.at(t)->total_charge)==1) && (nTree.at(t)->lep_isTrigMatch_0 || nTree.at(t)->lep_isTrigMatch_1 || nTree.at(t)->lep_isTrigMatch_2) && (nTree.at(t)->lep_isolationFixedCutLoose_0 && nTree.at(t)->lep_isolationFixedCutLoose_1 && nTree.at(t)->lep_isolationFixedCutLoose_2) && ((nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_1==0 && abs(nTree.at(t)->Mll01/1000.-91.2)<10.)||(nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_2==0 && abs(nTree.at(t)->Mll02/1000.-91.2)<10.)) && (nTree.at(t)->nJets_OR_T>=1)  ) cr1 = true;
      if(  (nTree.at(t)->trilep_type) && (abs(nTree.at(t)->total_charge)==1) && (nTree.at(t)->lep_isTrigMatch_0 || nTree.at(t)->lep_isTrigMatch_1 || nTree.at(t)->lep_isTrigMatch_2) && (nTree.at(t)->lep_isolationFixedCutLoose_0 && nTree.at(t)->lep_isolationFixedCutLoose_1 && nTree.at(t)->lep_isolationFixedCutLoose_2) && !((nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_1==0) || (nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_2==0)) && (nTree.at(t)->nJets_OR_T>=1)  ) cr2 = true;
      //if(  (nTree.at(t)->trilep_type) && (abs(nTree.at(t)->total_charge)==1) && (nTree.at(t)->lep_isTrigMatch_0 || nTree.at(t)->lep_isTrigMatch_1 || nTree.at(t)->lep_isTrigMatch_2) && (nTree.at(t)->lep_isolationFixedCutLoose_0 && nTree.at(t)->lep_isolationFixedCutLoose_1 && nTree.at(t)->lep_isolationFixedCutLoose_2) && ((nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_1==0 && abs(nTree.at(t)->Mll01/1000.-91.2)<10.)||(nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_2==0 && abs(nTree.at(t)->Mll02/1000.-91.2)<10.)) && (nTree.at(t)->nJets_OR_T>=1)  ) cr1 = true;
      //if(  (nTree.at(t)->trilep_type) && (abs(nTree.at(t)->total_charge)==1) && (nTree.at(t)->lep_isTrigMatch_0 || nTree.at(t)->lep_isTrigMatch_1 || nTree.at(t)->lep_isTrigMatch_2) && (nTree.at(t)->lep_isolationFixedCutLoose_0 && nTree.at(t)->lep_isolationFixedCutLoose_1 && nTree.at(t)->lep_isolationFixedCutLoose_2) && !((nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_1==0) || (nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_2==0)) && (nTree.at(t)->nJets_OR_T>=1)  ) cr2 = true;
//
//      // ( (trilep_type && (lep_isTrigMatch_0 || lep_isTrigMatch_1 || lep_isTrigMatch_2) && (abs(total_charge)==1) && (lep_isolationFixedCutLoose_0 && lep_isolationFixedCutLoose_1 && lep_isolationFixedCutLoose_2)) && ((lep_ID_0+lep_ID_1==0 && abs(Mll01/1000.-91.2)<10.)||(lep_ID_0+lep_ID_2==0 && abs(Mll02/1000.-91.2)<10.)) && (nJets_OR_T>=1) && MET_RefFinal_et/1000.<50. )
//      if(  (nTree.at(t)->trilep_type) && (abs(nTree.at(t)->total_charge)==1) && (nTree.at(t)->lep_isTrigMatch_0 || nTree.at(t)->lep_isTrigMatch_1 || nTree.at(t)->lep_isTrigMatch_2) && (nTree.at(t)->lep_isolationFixedCutLoose_0 && nTree.at(t)->lep_isolationFixedCutLoose_1 && nTree.at(t)->lep_isolationFixedCutLoose_2) && ((nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_1==0 && abs(nTree.at(t)->Mll01/1000.-91.2)<10.)||(nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_2==0 && abs(nTree.at(t)->Mll02/1000.-91.2)<10.)) && (nTree.at(t)->nJets_OR_T>=1) && nTree.at(t)->MET_RefFinal_et/1000.<50.  ) cr1 = true;
//      // ( (trilep_type && (lep_isTrigMatch_0 || lep_isTrigMatch_1 || lep_isTrigMatch_2) && (abs(total_charge)==1) && (lep_isolationFixedCutLoose_0 && lep_isolationFixedCutLoose_1 && lep_isolationFixedCutLoose_2)) && !((lep_ID_0+lep_ID_1==0) || (lep_ID_0+lep_ID_2==0)) && (abs((abs(lep_ID_0)/lep_ID_0)+(abs(lep_ID_1)/lep_ID_1)+(abs(lep_ID_2)/lep_ID_2))==1) && (nJets_OR_T>=1) && (Sum$(m_jet_pt/1000.>30.)>=1) )
//      if(  (nTree.at(t)->trilep_type) && (abs(nTree.at(t)->total_charge)==1) && (nTree.at(t)->lep_isTrigMatch_0 || nTree.at(t)->lep_isTrigMatch_1 || nTree.at(t)->lep_isTrigMatch_2) && (nTree.at(t)->lep_isolationFixedCutLoose_0 && nTree.at(t)->lep_isolationFixedCutLoose_1 && nTree.at(t)->lep_isolationFixedCutLoose_2) && !((nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_1==0) || (nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_2==0)) && (abs((abs(nTree.at(t)->lep_ID_0)/nTree.at(t)->lep_ID_0)+(abs(nTree.at(t)->lep_ID_1)/nTree.at(t)->lep_ID_1)+(abs(nTree.at(t)->lep_ID_2)/nTree.at(t)->lep_ID_2))==1) && (nTree.at(t)->nJets_OR_T>=1)  ) cr2 = true;
      // R20.7
      // gradient-isolated leptons      
//      if( (!loose) && (!LooseRegion) && ((nTree.at(t)->trilep_type)&&(abs(nTree.at(t)->total_charge)==1)&&(nTree.at(t)->lep_isTrigMatch_0||nTree.at(t)->lep_isTrigMatch_1||nTree.at(t)->lep_isTrigMatch_2||nTree.at(t)->lep_isTrigMatchDLT_0||nTree.at(t)->lep_isTrigMatchDLT_1||nTree.at(t)->lep_isTrigMatchDLT_2)&&((nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_1==0&&abs(nTree.at(t)->Mll01/1000.-91.2)<10.)||(nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_2==0&&abs(nTree.at(t)->Mll02/1000.-91.2)<10.))&&(nTree.at(t)->nJets_OR_T>=1)&&(nTree.at(t)->lep_promptLeptonIso_TagWeight_0<-0.5&&nTree.at(t)->lep_promptLeptonIso_TagWeight_1<-0.5&&nTree.at(t)->lep_promptLeptonIso_TagWeight_2<-0.5)&&(nTree.at(t)->RunYear==2015||nTree.at(t)->RunYear==2016)&&nTree.at(t)->MET_RefFinal_et/1000.<50.) ) cr1=true;
//      if( (!loose) && (!LooseRegion) && ((nTree.at(t)->trilep_type)&&(abs(nTree.at(t)->total_charge)==1)&&(nTree.at(t)->lep_isTrigMatch_0||nTree.at(t)->lep_isTrigMatch_1||nTree.at(t)->lep_isTrigMatch_2||nTree.at(t)->lep_isTrigMatchDLT_0||nTree.at(t)->lep_isTrigMatchDLT_1||nTree.at(t)->lep_isTrigMatchDLT_2)&&!((nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_1==0)||(nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_2==0))&&(abs((abs(nTree.at(t)->lep_ID_0)/nTree.at(t)->lep_ID_0)+(abs(nTree.at(t)->lep_ID_1)/nTree.at(t)->lep_ID_1)+(abs(nTree.at(t)->lep_ID_2)/nTree.at(t)->lep_ID_2))==1)&&(nTree.at(t)->nJets_OR_T>=1)&&(nTree.at(t)->lep_promptLeptonIso_TagWeight_0<-0.5&&nTree.at(t)->lep_promptLeptonIso_TagWeight_1<-0.5&&nTree.at(t)->lep_promptLeptonIso_TagWeight_2<-0.5)&&(nTree.at(t)->RunYear==2015||nTree.at(t)->RunYear==2016)) ) cr2=true;
//      // loose leptons      
//      if( (loose) && ((nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_1==0&&abs(nTree.at(t)->Mll01/1000.-91.2)<10.)||(nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_2==0&&abs(nTree.at(t)->Mll02/1000.-91.2)<10.)) ) cr1=true;
//      if( (loose) && !((nTree.at(t)->lepton_ID->at(0)+nTree.at(t)->lepton_ID->at(1)==0)||(nTree.at(t)->lepton_ID->at(0)+nTree.at(t)->lepton_ID->at(2)==0)) ) cr2=true;
//      //Loose CR
//      if( LooseRegion && ((nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_1==0&&abs(nTree.at(t)->Mll01/1000.-91.2)<10.)||(nTree.at(t)->lep_ID_0+nTree.at(t)->lep_ID_2==0&&abs(nTree.at(t)->Mll02/1000.-91.2)<10.)) ) cr1=true;
//      if( LooseRegion && !((nTree.at(t)->lepton_ID->at(0)+nTree.at(t)->lepton_ID->at(1)==0)||(nTree.at(t)->lepton_ID->at(0)+nTree.at(t)->lepton_ID->at(2)==0)) ) cr2=true;

      if(cr1&&cr2) cout<<"CHECK ME, something is wrong in CRs definition: they are overlapping"<<endl;
      if((!cr1)&&(!cr2)) cout<<"CHECK ME, what about preselection?!"<<endl; 
      
      if(cr1) n_CR=1;
      else if(cr2) n_CR=2;
      else cout<<"HEY HEY HEY, what about preselection?!"<<endl; 
      
      if(debug) cout<<"cr1: "<< cr1 <<" cr2: "<< cr2 <<endl; 

      //Selection of events ( ON TOP OF SKIMMING!!! )
      //Compute transverse mass with Et_miss and fake lepton candidate
      int njets(0.);
      for(int kj=0; kj<nTree.at(t)->m_jet_pt->size(); kj++) { if(nTree.at(t)->m_jet_pt->at(kj)/1000./30.) njets++;}
      
      int fake_lep(-999);
      if(n_CR==1) fake_lep = fakeInd(nTree.at(t));
      else if( n_CR==2 && ((!LooseRegion && njets>=1) || (LooseRegion)) ) {
	if (nTree.at(t)->lep_ID_1>nTree.at(t)->lep_ID_2)     fake_lep = 2;
	else         					     fake_lep = 1;	
      }
      //else continue;
      
      if(debug) cout<<"I'm right before m_t computation, the fake_lep is "<< fake_lep <<endl;
      float m_t(-999);
      float fake_lep_pt(-999), fake_lep_phi(-999);
      if (fake_lep==0) 		{ fake_lep_pt = nTree.at(t)->lep_Pt_0; fake_lep_phi = nTree.at(t)->lep_Phi_0; }
      else if (fake_lep==1) 	{ fake_lep_pt = nTree.at(t)->lep_Pt_1; fake_lep_phi = nTree.at(t)->lep_Phi_1; }
      else if (fake_lep==2) 	{ fake_lep_pt = nTree.at(t)->lep_Pt_2; fake_lep_phi = nTree.at(t)->lep_Phi_2; }
      // R21
      m_t = newTransMass(nTree.at(t), fake_lep_pt, fake_lep_phi)/1000.;
      if(debug) cout << "Transverse mass:	" << m_t << endl;
      // R20.7
      //m_t = newTransMass(nTree.at(t),fake_lep)/1000.;

      if(cr1 && m_t<50. && !LooseRegion) goodOne = true; //CR1
      if(cr2 && !LooseRegion) goodOne = true; //CR2
      if(LooseRegion) goodOne = true;

      //if(debug&&(cr1&&(!goodOne))) cout<<"My transverse mass is: "<< m_t <<endl;

      //23-01-2017: NEW, fill only if the event is in one of the 2 CRs
      if(goodOne) {
	// Data event
	if( (((string)inputFile.at(t)->GetName()).find("data"))!=string::npos ) isPrompt = 99;
	// Prompt event
	else if( (((string)inputFile.at(t)->GetName()).find("Zjets"))==string::npos && (((string)inputFile.at(t)->GetName()).find("ttbar"))==string::npos && (((string)inputFile.at(t)->GetName()).find("Wjets"))==string::npos && (((string)inputFile.at(t)->GetName()).find("singletop"))==string::npos ) isPrompt = 1;
	// Non-prompt event
	else isPrompt = 0;

	FakeIndex = fake_lep;
	MT = m_t;
        if (fake_lep==0)          { FakeFlavour = nTree.at(t)->lep_ID_0; }
        else if (fake_lep==1)     { FakeFlavour = nTree.at(t)->lep_ID_1; }
        else if (fake_lep==2)     { FakeFlavour = nTree.at(t)->lep_ID_2; }
        // R20.7
//	FakeFlavour = nTree.at(t)->lepton_ID->at(fake_lep);

        // 23-04-2018
        vector<float> v_lepton_pt; v_lepton_pt.reserve(4);
        v_lepton_pt.push_back(nTree.at(t)->lep_Pt_0);
        v_lepton_pt.push_back(nTree.at(t)->lep_Pt_1);
        v_lepton_pt.push_back(nTree.at(t)->lep_Pt_2);
        vector<float> v_lepton_ID; v_lepton_ID.reserve(4);
        v_lepton_ID.push_back(nTree.at(t)->lep_ID_0);
        v_lepton_ID.push_back(nTree.at(t)->lep_ID_1);
        v_lepton_ID.push_back(nTree.at(t)->lep_ID_2);
        vector<float> v_lepton_truthOrigin; v_lepton_truthOrigin.reserve(4);
        v_lepton_truthOrigin.push_back(nTree.at(t)->lep_truthOrigin_0);
        v_lepton_truthOrigin.push_back(nTree.at(t)->lep_truthOrigin_1);
        v_lepton_truthOrigin.push_back(nTree.at(t)->lep_truthOrigin_2);


	//23-04-2018
	bool HO(false), LO(false), PO(false);
	for(unsigned int l=0; l<v_lepton_pt.size(); l++){
	  int ID = v_lepton_ID.at(l);
	  int TO = v_lepton_truthOrigin.at(l);
	  if(abs(ID)==11)      NEL++;
	  else if(abs(ID)==13) NMU++;
	  else cout<<"CHECK ME: lepton which is not an el nor a mu?!"<<endl;
	  //Are there (truth) fake leps? Origin?
	  //Class: prompt (0), heavy/light el (1/2), heavy/light mu (3/4)
	  for(unsigned int i=0; i<sizeof(heavy)/sizeof(*heavy); i++) { 
	    if(TO==heavy[i]) { 
	      FAKEINDEX = l;
	      if(debug) cout<<"heavy: "<< TO <<endl; 
	      HO=true; 
	      if(abs(ID)==11)      CLASS = 1; 
	      else if(abs(ID)==13) CLASS = 3;
              else cout<<"CHECK ME: l lepton which is not an el nor a mu?!"<<endl;
	    } 
	  }
	  for(unsigned int i=0; i<sizeof(light)/sizeof(*light); i++) { 
	    if(TO==light[i]) { 
	      FAKEINDEX = l;
	      if(debug) cout<<"light: "<< TO <<endl; 
	      LO=true; 
	      if(abs(ID)==11)      CLASS = 2;
              else if(abs(ID)==13) CLASS = 4;
              else cout<<"CHECK ME: l lepton which is not an el nor a mu?!"<<endl;
	    } 
	  }
	}//for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++)
	if(isPrompt==1) CLASS = 0;
	if((!HO)&&(!LO)&&(CLASS!=0)&&(isPrompt!=99)) { cout<<"**WARNING** Here I am and my TOs are:"<<endl; for(unsigned int l=0; l<v_lepton_pt.size(); l++) { cout<< v_lepton_truthOrigin.at(l) <<endl; cout<<endl; } cout<<"next bad event"<<endl; }

        nEl=NEL; nMu=NMU;
	if((NEL!=0&&NEL!=1&&NEL!=2&&NEL!=3)||(NMU!=0&&NMU!=1&&NMU!=2&&NMU!=3)) cout<<"** WARNING ** n. of e/mu is WRONG!!!!!!"<<endl;
	if(debug) { cout<<"Number of el: "<< NEL <<endl; cout<<"Number of mu: "<< NMU <<endl; }
	//Dividing up events in 4 regions
	switch(NEL) {
	case 3:
	  //eee
	  REGION=0;
	  if(NMU!=0) cout<<"HEY!!!! Something going wrong here!"<<endl;
	  break;
        case 2:
	  //eem
	  REGION=1;
	  break;
	case 1:
	  //emm
	  REGION=2;
	  break;
	case 0:
	  //mmm
	  REGION=3;
	  if(NEL!=0) cout<<"HEY!!!! Something going wrong here!"<<endl;
	  break;
	}//switch(NEL)
        if(REGION==0&&(CLASS==3||CLASS==4)) cout<<"TOO BAD -> Region: "<< REGION <<" Class: "<< CLASS <<" NEL: "<< NEL <<endl;
        if(REGION==3&&(CLASS==1||CLASS==2)) cout<<"TOO BAD -> Region: "<< REGION <<" Class: "<< CLASS <<" NEL: "<< NEL <<endl;
        if(REGION<0||REGION>3) cout<<"** WARNING ** Region: "<< REGION <<" -> what's going on?????"<<endl;
        if(CLASS<0||CLASS>4) cout<<"** WARNING ** Class: "<< CLASS <<" event number: "<< ievt <<" -> what's going on?????"<<endl;
	//if(debug) { cout<<"Region: "<< REGION <<endl; cout<<"Class: "<< CLASS <<endl; }

	Class=CLASS;
	Region=REGION;
        fakeIndex=FAKEINDEX;

	Target->cd();
	newtree->Fill();
      }//if(goodOne)
    }//for (Long64_t ievt=0; ievt<nentries;ievt++)
    Target->Write();

  }//for(files)
  //f->Write();

  return 0;
}//int main() 
