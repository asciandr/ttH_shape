///////////////////////// -*- C++ -*- /////////////////////////////
// Header file
// Author: A.Sciandra<andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TROOT.h"
#include "TTree.h"
#include "TString.h"
#include "THStack.h"
#include "TLegend.h"
#include <string.h>
#include <TLorentzVector.h>
#include <iostream>

#include "nominal.C"

using namespace std;

/************************/
/*** FSF Computation ***/
/**********************/
static int fakeInd (nominal  *nTree) {
  double MT(100000.), MZ(91200.);
  int FLEPIND=-1;
  // R21
  if (nTree->lep_ID_0==-nTree->lep_ID_1 && (abs(nTree->Mll01-MZ)<=10e3)) {FLEPIND=2;} 
  if (nTree->lep_ID_0==-nTree->lep_ID_2 && (abs(nTree->Mll02-MZ)<=10e3)) {FLEPIND=1;} 
  if ((nTree->lep_ID_0==-nTree->lep_ID_2) && (abs(nTree->Mll02-MZ)<=10e3) && (nTree->lep_ID_0==-nTree->lep_ID_1) && (abs(nTree->Mll01-MZ)<=10e3)) {
    if(abs(nTree->Mll01-MZ)< abs(nTree->Mll02-MZ) ) {FLEPIND=2;}
    else {FLEPIND=1;}
  }
  // R20.7
//  if (nTree->lepton_ID->at(0)==-nTree->lepton_ID->at(1) && (abs(nTree->Mll01-MZ)<=10e3)) {FLEPIND=2;} 
//  if (nTree->lepton_ID->at(0)==-nTree->lepton_ID->at(2) && (abs(nTree->Mll02-MZ)<=10e3)) {FLEPIND=1;} 
//  if ((nTree->lepton_ID->at(0)==-nTree->lepton_ID->at(2)) && (abs(nTree->Mll02-MZ)<=10e3) && (nTree->lepton_ID->at(0)==-nTree->lepton_ID->at(1)) && (abs(nTree->Mll01-MZ)<=10e3)) {
//    if(abs(nTree->Mll01-MZ)< abs(nTree->Mll02-MZ) ) {FLEPIND=2;}
//    else {FLEPIND=1;}
//  }
  return FLEPIND;
}

//static int fakeIndTry (nominal  *nTree) {
//  double MT(100000.), MZ(91200.);
//  int FLEPIND=-1;
//  if (nTree->lepton_ID->at(0)==-nTree->lepton_ID->at(1) && (abs(nTree->Mll01-MZ)<=91.2e3)) {FLEPIND=2;}
//  if (nTree->lepton_ID->at(0)==-nTree->lepton_ID->at(2) && (abs(nTree->Mll02-MZ)<=91.2e3)) {FLEPIND=1;}
//  if ((nTree->lepton_ID->at(0)==-nTree->lepton_ID->at(2)) && (abs(nTree->Mll02-MZ)<=91.2e3) && (nTree->lepton_ID->at(0)==-nTree->lepton_ID->at(1)) && (abs(nTree->Mll01-MZ)<=91.2e3)) {
//    if(abs(nTree->Mll01-MZ)< abs(nTree->Mll02-MZ) ) {FLEPIND=2;}
//    else {FLEPIND=1;}
//  }
//  return FLEPIND;
//}

//Get transverse mass between fake lepton candidate and Et_miss to cut WZ (VV) IN Z+jets CR
static double newTransMass (nominal  *nTree, float fake_lep_pt, float fake_lep_phi) {
  double m_t(-1.);
  TVector3 lp, lE;
  lp.SetPtThetaPhi(fake_lep_pt, 0, fake_lep_phi);
  
// R20.7
//static double newTransMass (nominal  *nTree, int fake_lep) 
//  double m_t(-1.);
//  TVector3 lp, lE;
//  lp.SetPtThetaPhi(nTree->lepton_pt->at(fake_lep),0, nTree->lepton_phi->at(fake_lep));

  lE.SetPtThetaPhi(nTree->MET_RefFinal_et        ,0, nTree->MET_RefFinal_phi        );
  m_t = sqrt(2*(lp.Pt()*lE.Pt()-(lE.Dot(lp))));   
  /*cout<<endl;
    cout<<"*****NEW EVENT*****"<<endl;
    cout<<"lp  =  "<< nTree->lepton_pt->at(fake_lep) <<" "<< nTree->lepton_phi->at(fake_lep) <<endl;
    cout<<"lE  =  "<< nTree->MET_RefFinal_et         <<" "<< nTree->MET_RefFinal_phi         <<endl;
    cout<<"m_t =  "<< m_t <<endl;*/
  return m_t;
}

/* NEW - Compute error on FFs */
static double fakeFactorErr3l(double den, int N1, int N2, vector<float> C, vector<float> T, vector<float> Z, vector<float> err_C, vector<float> err_T, vector<float> err_Z) {
  vector<double> v_d_l_b_e;
  vector<double> v_err_coeff;
  double d_l_b_e_dC(-999.), d_l_b_e_dT(-999.), d_l_b_e_dZ(-999.), d_l_b_e_dC1(-999.), d_l_b_e_dT1(-999.), d_l_b_e_dZ1(-999.);
  /*double den(-999.);
    den = (T.at(N1)*Z.at(N2) - T.at(N2)*Z.at(N1));*/
  for(int i=0; i<C.size(); i++) cout<<"C "<< C.at(i) <<endl;
  for(int i=0; i<T.size(); i++) cout<<"T "<< T.at(i) <<endl;
  for(int i=0; i<Z.size(); i++) cout<<"Z "<< Z.at(i) <<endl;
  d_l_b_e_dC  = Z.at(N2) / den;
  cout<<"d_l_b_e_dC = "<< d_l_b_e_dC <<endl;
  d_l_b_e_dT  = (C.at(N2)*Z.at(N1)*Z.at(N2)-C.at(N1)*Z.at(N2)*Z.at(N2)) / (den*den);
  cout<<"d_l_b_e_dZ = "<< d_l_b_e_dT <<endl;
  d_l_b_e_dZ  = (T.at(N2)*C.at(N1)*Z.at(N2)-T.at(N1)*C.at(N2)*Z.at(N2)) / (den*den);
  cout<<"d_l_b_e_dT = "<< d_l_b_e_dZ <<endl;
  d_l_b_e_dC1 = - Z.at(N1) / den;
  cout<<"d_l_b_e_dC1 = "<< d_l_b_e_dC1 <<endl;
  d_l_b_e_dT1 = (C.at(N1)*Z.at(N1)*Z.at(N2)-C.at(N2)*Z.at(N1)*Z.at(N1)) / (den*den);
  d_l_b_e_dZ1 = (T.at(N1)*C.at(N2)*Z.at(N1)-T.at(N2)*C.at(N1)*Z.at(N1)) / (den*den);
  //v_d_l_b_e.push_back();
  v_err_coeff.push_back(err_C.at(N1)); v_err_coeff.push_back(err_T.at(N1)); v_err_coeff.push_back(err_Z.at(N1)); v_err_coeff.push_back(err_C.at(N2)); v_err_coeff.push_back(err_T.at(N2)); v_err_coeff.push_back(err_Z.at(N2));
  v_d_l_b_e.push_back(d_l_b_e_dC); v_d_l_b_e.push_back(d_l_b_e_dT); v_d_l_b_e.push_back(d_l_b_e_dZ); v_d_l_b_e.push_back(d_l_b_e_dC1); v_d_l_b_e.push_back(d_l_b_e_dT1); v_d_l_b_e.push_back(d_l_b_e_dZ1);
  double err_l_b_e(-999.), sum(0.); 
  if(v_err_coeff.size()!=v_d_l_b_e.size()) cout<<"svegliati"<<endl;
  for(int i=0; i<v_d_l_b_e.size(); i++) {
    sum += ((v_err_coeff.at(i)*v_err_coeff.at(i))*(v_d_l_b_e.at(i)*v_d_l_b_e.at(i)));
  }
  err_l_b_e = sqrt(sum);
  return err_l_b_e;
}
/* NEW - Compute error on FFs */

/************************/
/*** FSF Application ***/
/**********************/
/* Is there at least a fake l? A fake lepton is defined as a lepton not coming from tau, top, W, Z, H, VV */
static bool  isFake    (nominal  *nTree) {
  bool yesFake = false;
  for(unsigned a=0; (int)a<nTree->lepton_pt->size(); a++) {
    int motherID(0);
    motherID = nTree->lepton_truthOrigin->at(a);
    if( motherID!=9 && motherID!=10 && motherID!=12 && motherID!=13 && motherID!=14 && motherID!=43 ) yesFake = true;
    if( yesFake ) break;
  }
  return yesFake;
}

/******************/
/*** TEXT FILE ***/
/****************/

/* Obtain name of the process */
static string getSampleName(string name, string fl) {
  string name_final = name.substr(0,name.find(fl));

  return name_final;
}

/*****************/
/*** PLOTTING ***/
/***************/

/* Obtain name of the samples from histos' names */
static string GetSampleName(string name) {
  size_t pos1  		= name.find("/");
  string name2 		= name.substr(pos1+1);
  size_t pos2  		= name2.find(".");
  string name3 		= name2.substr(0,pos2);
  /*size_t pos3  		= name3.find("_");
  string name4          = name3.substr(pos3+1);
  size_t pos4           = name4.find("_");
  string name5          = name4.substr(pos4+1);
  size_t pos5           = name5.find("_");
  string name_final 	= name4.substr(pos5+1);*/

  return name3;
  //return name_final;
}

/*Compare shapes WITH ERROR BARS in MC */
static TH1F *PlotMCError(double Lum, vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit) {//Compare MC shapes
  int n = histos.size()+1;
  vector<TH1F*> new_histos;
  vector<int>   colours;    
  new_histos    .reserve( n );
  colours       .reserve( n );
  colours.push_back(2);
  colours.push_back(4);
  colours.push_back(7);
  colours.push_back(11);
  colours.push_back(3);
  colours.push_back(13);
  colours.push_back(5);
  colours.push_back(46);
  colours.push_back(41);
  colours.push_back(8);
  colours.push_back(9);
  colours.push_back(12);

  TCanvas *cs = new TCanvas(outFile_name);
  TLegend *leg = new TLegend(0.9,0.7,0.7,0.9);
  leg->SetTextSize(0.04);
  gStyle->SetLegendBorderSize(0);
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    TH1F *hnew = (TH1F*)histos.at(i)->Clone(name.c_str());
    hnew->SetFillColor(colours.at(i));
    hnew->SetFillStyle(3003);
    hnew->SetLineColor(colours.at(i));
    hnew->SetMarkerColor(colours.at(i));
    if(hnew->Integral()!=0) hnew->Scale(Lum);
    new_histos.push_back( hnew );
  }
  /* Set Range */
  double max(-1000.);
  //max = new_histos.at(0)->GetMaximum()+4*(new_histos.at(0)->GetBinError(new_histos.at(0)->GetMaximumBin()));
  for(int i=0; i<new_histos.size();i++) {
    if( new_histos.at(i)->GetMaximum()+1.5*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin())>max ) max = new_histos.at(i)->GetMaximum()+1.5*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin());
  }
  new_histos.at(0)->GetYaxis()->SetRangeUser(0,max);
  TString Xtitle = (string)axisName;
  new_histos.at(0)->GetXaxis()->SetTitle(Form(Xtitle));
  TString Ytitle = string()+"Entries / %2.2f"+entriesUnit;
  new_histos.at(0)->GetYaxis()->SetTitle(Form(Ytitle,new_histos.at(0)->GetBinWidth(1)));
  //new_histos.at(0)->GetYaxis()->SetTitleOffset(1.3);
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    TLegendEntry *entry = leg->AddEntry(new_histos.at(i),GetSampleName(name).c_str(),"f");
    if(i==0&&new_histos.at(i)->Integral()!=0) 	new_histos.at(i)->Draw("p");
    else if(new_histos.at(i)->Integral()!=0)	new_histos.at(i)->Draw("Psame");
  }
  leg->Draw("same");

  gStyle->SetOptStat(0);
  cs->SaveAs(outFile_name,"RECREATE");

  return histos.at(0);
}


/*Compare shapes in MC */
static TH1F *PlotMC(double Lum, vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit) {//Compare MC shapes
  int n = histos.size()+1;
  vector<TH1F*> new_histos;
  vector<int>   colours;    
  new_histos    .reserve( n );
  colours       .reserve( n );
  colours.push_back(41);
  colours.push_back(8);
  colours.push_back(46);
  colours.push_back(11);
  colours.push_back(12);
  colours.push_back(13);
  colours.push_back(5);
  colours.push_back(7);
  colours.push_back(2);
  colours.push_back(4);
  colours.push_back(3);
  colours.push_back(12);

  TCanvas *cs = new TCanvas(outFile_name);
  TLegend *leg = new TLegend(0.9,0.7,0.7,0.9);
  leg->SetTextSize(0.04);
  gStyle->SetLegendBorderSize(0);
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    TH1F *hnew = (TH1F*)histos.at(i)->Clone(name.c_str());
    hnew->SetFillColor(colours.at(i));
    hnew->SetFillStyle(3003);
    hnew->SetLineColor(colours.at(i));
    hnew->SetMarkerColor(colours.at(i));
    if(hnew->Integral()!=0) hnew->Scale(Lum);
    new_histos.push_back( hnew );
  }
  /* Set Range */
  double max(-1000.);
  //max = new_histos.at(0)->GetMaximum()+4*(new_histos.at(0)->GetBinError(new_histos.at(0)->GetMaximumBin()));
  for(int i=0; i<new_histos.size();i++) {
    if( new_histos.at(i)->GetMaximum()+1.5*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin())>max ) max = new_histos.at(i)->GetMaximum()+1.5*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin());
  }
  new_histos.at(0)->GetYaxis()->SetRangeUser(0,max);
  TString Xtitle = (string)axisName;
  new_histos.at(0)->GetXaxis()->SetTitle(Form(Xtitle));
  TString Ytitle = string()+"Entries / %2.2f"+entriesUnit;
  new_histos.at(0)->GetYaxis()->SetTitle(Form(Ytitle,new_histos.at(0)->GetBinWidth(1)));
  //new_histos.at(0)->GetYaxis()->SetTitleOffset(1.3);
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    TLegendEntry *entry = leg->AddEntry(new_histos.at(i),GetSampleName(name).c_str(),"f");
    if(i==0&&new_histos.at(i)->Integral()!=0) 	new_histos.at(i)->Draw("HIST");
    else if(new_histos.at(i)->Integral()!=0)	new_histos.at(i)->Draw("HISTsame");
  }
  leg->Draw("same");

  gStyle->SetOptStat(0);
  cs->SaveAs(outFile_name,"RECREATE");

  return histos.at(0);
}

static double Zmass (nominal  *nTree, int fake_lep) {
  double mZ(-999.);
  int loop(0.);
  TLorentzVector lorVect1, lorVect2;
  for(unsigned a=0; (int)a<nTree->lepton_pt->size(); a++) {
    if(a!=fake_lep) { 
      if(loop==0) lorVect1.SetPtEtaPhiE(nTree->lepton_pt->at(a),nTree->lepton_eta->at(a),nTree->lepton_phi->at(a),nTree->lepton_E->at(a)); 
      if(loop==1) lorVect2.SetPtEtaPhiE(nTree->lepton_pt->at(a),nTree->lepton_eta->at(a),nTree->lepton_phi->at(a),nTree->lepton_E->at(a)); 
      if(loop==2) cout<<"************* YOU HAVE A PROBLEM! *************"<<endl;
      loop ++; 
    }
  }
  if(loop==2) mZ = (lorVect1+lorVect2).M()/1000.;
  else cout<<"************* YOU HAVE A big PROBLEM! *************"<<endl;

  return mZ;
}

static double threeLmass (nominal  *nTree) {
  double m3l(-999.);
  TLorentzVector lorVect1, lorVect2, lorVect3;
  lorVect1.SetPtEtaPhiE(nTree->lepton_pt->at(0),nTree->lepton_eta->at(0),nTree->lepton_phi->at(0),nTree->lepton_E->at(0));
  lorVect2.SetPtEtaPhiE(nTree->lepton_pt->at(1),nTree->lepton_eta->at(1),nTree->lepton_phi->at(1),nTree->lepton_E->at(1));
  lorVect3.SetPtEtaPhiE(nTree->lepton_pt->at(2),nTree->lepton_eta->at(2),nTree->lepton_phi->at(2),nTree->lepton_E->at(2));
  m3l = (lorVect1+lorVect2+lorVect3).M()/1000.;
 
  return m3l; 
}


