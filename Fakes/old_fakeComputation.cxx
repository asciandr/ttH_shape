///////////////////////// -*- C++ -*- /////////////////////////////
// fakeComputation.C
// Author: A.Sciandra <andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TChain.h"
#include "TFile.h"
#include "TString.h"
#include <string.h>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <string>
#include <ctime>
#include <iostream>
#include <fstream>
#include <sstream>

#include "optimisation_FSF.h"

using namespace std      ;

int main() {
  clock_t begin       = clock();
  bool    bjet        = false;
  bool    pTcut       = false;
  if(bjet&&pTcut)     {cout<<"***** WARNING ***** -> Check bjet and lep pT cuts!"<<endl;}
  bool    pTfakeLep   = false;
  int     Run         = 0;  
  bool    loose       = true; //Decide whether to compute the FF for 3 loose (true) or 3 tight leptons (false)
  bool    plot        = true; //Decide whether plotting or not
  bool    correction  = false;
  int     n_CR(0);             //Number of CRs 
  TString bString     = "&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)";
  TString pTString    = "&&(lepton_pt[0]/1000.>15.&&lepton_pt[1]/1000.>15.&&lepton_pt[2]/1000.>15.)";
  const char* outFold = "plots/";
  /****** FSFs ******/
  double  lambda[2][2]; //{Lambda_e_b,Lambda_e_l,Lambda_mu_b,Lambda_mu_l}
  lambda[0][0] = 1.;//1.26; //1.22; //Lambda_e_l
  lambda[1][0] = 1.;//0.86; //1.21; //Lambda_mu_l
  lambda[0][1] = 1.;//0.96; //1.24; //Lambda_e_b
  lambda[1][1] = 1.;//1.10; //1.18; //Lambda_mu_b
  /****** FSFs ******/
  // new CRs with low jet multiplicity (1 or 2) //
  const char*      CRs[] = {"(@lepton_pt.size()==3)&&(abs(total_charge)==1)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1)&&((lepton_ID[0]+lepton_ID[1]==0&&abs(Mll01/1000.-91.2)<10.)||(lepton_ID[0]+lepton_ID[2]==0&&abs(Mll02/1000.-91.2)<10.))&&(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&lepton_isolationGradient[0]==1&&lepton_isolationGradient[1]==1&&lepton_isolationGradient[2]==1&&(RunYear==2015||RunYear==2016)&&MET_RefFinal_et/1000.<50.",
			    "(@lepton_pt.size()==3)&&(abs(total_charge)==1)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1)&&!((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0))&&(abs((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2]))==1)&&(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&lepton_isolationGradient[0]==1&&lepton_isolationGradient[1]==1&&lepton_isolationGradient[2]==1&&(Sum$(m_jet_pt/1000.>30.)>=1)&&(RunYear==2015||RunYear==2016)"};
  //3 GRADIENT-ISOLATED LEPTONS
  /*const char*      CRs[] = {"(@lepton_pt.size()==3)&&(abs(total_charge)==1)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1)&&((lepton_ID[0]+lepton_ID[1]==0&&abs(Mll01/1000.-91.2)<10.)||(lepton_ID[0]+lepton_ID[2]==0&&abs(Mll02/1000.-91.2)<10.))&&(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&(RunYear==2015||RunYear==2016)&&MET_RefFinal_et/1000.<50.",
    "(@lepton_pt.size()==3)&&(abs(total_charge)==1)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1)&&!((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0))&&(abs((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2]))==1)&&(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&(Sum$(m_jet_pt/1000.>30.)>=1)&&(RunYear==2015||RunYear==2016)"};
  */
  // Check tightening of isolation effect 
  //const char*      CRs[] = {"(@lepton_pt.size()==3)&&(abs(total_charge)==1)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1)&&((lepton_ID[0]+lepton_ID[1]==0&&abs(Mll01/1000.-91.2)<10.)||(lepton_ID[0]+lepton_ID[2]==0&&abs(Mll02/1000.-91.2)<10.))&&(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&MET_RefFinal_et/1000.<50.&&((abs(lep_ID_1)==13&&lep_isolationFixedCutTightTrackOnly_1)||(abs(lep_ID_1)==11&&lep_isolationFixedCutTight_1&&lep_isTightLH_1))&&((abs(lep_ID_2)==13&&lep_isolationFixedCutTightTrackOnly_2)||(abs(lep_ID_2)==11&&lep_isolationFixedCutTight_2&&lep_isTightLH_2))&&(RunYear==2015||RunYear==2016)",
  //                         "(@lepton_pt.size()==3)&&(abs(total_charge)==1)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1)&&!((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0))&&(abs((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2]))==1)&&(@m_jet_pt.size()==1||@m_jet_pt.size()==2)&&(Sum$(m_jet_pt/1000.>30.)>=1)&&((abs(lep_ID_1)==13&&lep_isolationFixedCutTightTrackOnly_1)||(abs(lep_ID_1)==11&&lep_isolationFixedCutTight_1&&lep_isTightLH_1))&&((abs(lep_ID_2)==13&&lep_isolationFixedCutTightTrackOnly_2)||(abs(lep_ID_2)==11&&lep_isolationFixedCutTight_2&&lep_isTightLH_2))&&(RunYear==2015||RunYear==2016)"};
  // Check tightening of isolation effect 
  
  //Enriched background for each CR
  const char*      enrBack[] = {"Zjets","ttbar"};
  //Lepton flavour for looping
  const char*      flavour[] = {"e","mu"};
  //Define input folder
  TString inpFold = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v27/01/";
  //TString inpFold = "/afs/cern.ch/user/a/asciandr/work/ttH/loose_ntuples_v23/samples/";
  //TString inpFold = "/afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/Fakes/loose_ntuples/";
  //Root files for looping
  const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root","ttW_aMcNloP8.root","diboson.root","rare.root","singletop.root","Wjets_NNPDF.root","ttbar.root","Zjets_NNPDF.root","data.root"};
  //const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root","ttW_aMcNloP8.root","diboson.root","rare.root","singletop.root","Wjets.root","ttbar.root","Zjets.root","data.root"};
  //const char*      files[] = {"ttH.root","ttV_NLO.root","diboson.root","410080.root","410081.root","tHjb.root","tWH.root","VH.root","VVV.root","tZ.root","tWZ.root","singletop.root","Wjets_NNPDF.root","ttbar.root","Zjets_NNPDF.root","data.root"};
  
  /* Set Luminosity (inv pb) and reserve enough space for meaningful vectors */
  float Lum_tot = 36074.6;         //v27 Int Lum 
  //float Lum_tot = 36470.2;         //v23 Int Lum 
  //float Lum15 = 3212.96 ;
  //float Lum16 = 24799.9; //v22 Int Lum
  //float Lum15 = 3212.96 ;
  //float Lum16 = 18857.4; //v21 Int Lum
  //float Lum15 = 3193.68 ;
  //float Lum16 = 8510.8  ; //v18 Int Lum
  //float Lum15 = 3212.956160 ;
  //float Lum16 = 9994.733568 ; //v19 Int Lum
  float Lum(1.);
  Lum = Lum_tot       ;
  //Lum = Lum15 + Lum16       ;
  cout<<"--------------> Running at an integrated luminosity of: "<< Lum/1000 <<" fb^(-1): "<<endl;
  int   NMAX =                (sizeof(files) / sizeof(files[0])) + 4.;
  vector<float>               Signal;
  vector<float>               Back  ;
  /* Store coefficients for fake factors' computation and their uncertainties */
  vector<float>               C    ;
  vector<float>               T    ;
  vector<float>               Z    ;
  vector<float>               err_C;
  vector<float>               err_T;
  vector<float>               err_Z;
  /* Reserve Enough Space for the Vectors */ 
  Signal  	              .reserve(NMAX);
  Back    	              .reserve(NMAX);
  C    	                      .reserve(NMAX);
  T    	                      .reserve(NMAX);
  Z    	                      .reserve(NMAX);
  err_C                       .reserve(NMAX);
  err_T                       .reserve(NMAX);
  err_Z                       .reserve(NMAX);
  /* Define 6 histos for e and 6 for mu to compute FFs and their errors */
  vector<TH1F*>                v_hist_C_e    ;
  vector<TH1F*>                v_hist_T_e    ;
  vector<TH1F*>                v_hist_Z_e    ;
  vector<TH1F*>                v_hist_C_mu   ;
  vector<TH1F*>                v_hist_T_mu   ;
  vector<TH1F*>                v_hist_Z_mu   ;
  v_hist_C_e                   .reserve(NMAX);
  v_hist_T_e                   .reserve(NMAX);
  v_hist_Z_e                   .reserve(NMAX);
  v_hist_C_mu                  .reserve(NMAX);
  v_hist_T_mu                  .reserve(NMAX);
  v_hist_Z_mu                  .reserve(NMAX);

  cout<<endl;
  cout<<"   ///////////////////////// -*- C++ -*- /////////////////////////////"<<endl;
  cout<<"  // fakeComputation.C                                             //"<<endl;
  cout<<" // Author: A.Sciandra <andrea.sciandra@cern.ch>                  //"<<endl;
  cout<<"///////////////////////////////////////////////////////////////////"<<endl;

  cout<<endl;
  if ( plot  )      cout<<"!!  Plot option activated    !!"<<endl;
  if ( loose )      cout<<"!!  Computation for loose l  !!"<<endl;
  if ( correction ) {
    cout<<"!!  Applying FFs to the MC   !!"<<endl;
    cout<<"The FFs applied are:"<<endl;
    for(int i=0;i<2;i++) for(int j=0;j<2;j++) cout<< lambda[j][i] <<endl;;
  }

  cout<<endl;
  cout<<"******************************************************"<<endl;
  cout<<"*****     Starting Fake Factors computation      *****"<<endl;
  cout<<"******************************************************"<<endl;
  cout<<endl;
  cout<<"** Reserving capacity() = "<< C.capacity() <<" for meaningful vectors       **"<<endl;
  cout<<endl;
  
  // Open the files
  cout<<"** Opening root Input Files                                             **"<<endl;
  cout<<endl;
  vector<TFile*>              inputFile;
  vector<TTree*>              v_fChain;
  inputFile                   .reserve( NMAX );
  v_fChain                    .reserve( NMAX );

  cout<<endl;
  cout<<"** Looping on the following samples:                                    **"<<endl;
  cout<<endl;
  
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    //TString     string_i  = string()+files[i]   ;
    TString     string_i  = string()+inpFold+files[i]   ;
    TTree       *chain;
    inputFile   .push_back(new TFile(string_i));
    inputFile   .at(i)->GetObject("nominal",chain); 
    v_fChain    .push_back( chain );
    
    cout<< inputFile.at(i)->GetName() <<endl;
  }
  
  /****** Save numbers in output .txt ******/
  ofstream txtFile("numbers.txt");
  vector<string> lines;
  lines.reserve(10*NMAX);
  stringstream b;
  b<<"$\\"<<"mathcal{L} = "<<Lum/1000<<"\\"<<";"<<"\\"<<"text{fb}^{-1}$";
  string zeroLine = b.str();
  lines.push_back(zeroLine+"\n");
  string firstLine;
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) firstLine += string()+getSampleName(files[i],".")+"\t";
  firstLine += "MC tot";
  lines.push_back(firstLine+"\n");
  //cout<< firstLine <<endl;
  /****** Save numbers in output .txt ******/

  /* OUTPUT FILES */
  TFile *f = new TFile("f.root","RECREATE"); 
  //LOOP OVER DIFFERENT CRs
  for(int cr=0; cr<sizeof(CRs) / sizeof(CRs[0]); cr++) {
    n_CR ++;
    TString PreSelection;
    if( pTcut )       PreSelection = string()+CRs[cr]+pTString;
    else if( bjet )   PreSelection = string()+CRs[cr]+bString;
    else              PreSelection = string()+CRs[cr];
    string  EnrichedBack = enrBack[cr];

    cout<<endl;
    cout<<"******************************************************"<<endl;
    cout<<"********                CR n. "<< n_CR <<"              *********"<<endl;
    cout<<"******************************************************"<<endl;
    cout<<endl;
    cout<<"Here the enriched sample is "<< EnrichedBack <<endl;
    cout<<endl;
    cout    <<endl;
    cout    <<"********           	 Applying the following PRESELECTION for Skimming:                   ********"<<endl;
    cout    <<endl;
    cout    <<endl;
    cout    <<"''       "<< PreSelection <<"       ''"<<endl;
    cout    <<endl;
    cout    <<endl;
    cout    <<"**                       	                  Skimming root files        	      	      	      	      **"<<endl;
    
    // Skimmed TTreeS
    vector<TTree*>     v_fChainSkim;
    vector<nominal*>   nTree       ;  
    v_fChainSkim       .reserve( NMAX );
    nTree              .reserve( NMAX );
    
    
    for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
      v_fChainSkim .push_back( (TTree*)v_fChain.at(i)->CopyTree( PreSelection ) );
      nTree        .push_back( new nominal(v_fChainSkim.at(i))               );
    } 
  
    clock_t end1 = clock();
    double  elapsed_secs = double(end1 - begin) / CLOCKS_PER_SEC;
    
    cout    <<endl;
    cout    <<"**        Skimming complete... Time elapsed: "<< elapsed_secs <<" s   	  **"<<endl;
    cout    <<endl;
    cout    <<"**                                     Skimmed     TTree contain:                                  **"<<endl;
    cout    <<endl;
    for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) cout<< inputFile.at(i)->GetName() <<"  ->  "<< v_fChainSkim.at(i)->GetEntriesFast() <<"  *RAW* events        **"<<endl;
    cout    <<endl;
    cout    <<"**                            Approaching fake estimation...                            **"<<endl;
    cout    <<endl;    
    /* Defining histos for events (and errors) counting */
    int                      	n_tot(-1);
    vector<TH1F*>            	v_hN;
    v_hN                     	.reserve( NMAX );
    vector<TH1F*>            	v_hFakes;
    v_hFakes                 	.reserve( NMAX );
    vector<TH1F*>            	v_fakeOrig;
    v_fakeOrig               	.reserve( NMAX );
    vector<TH1F*>            	v_ptcone40, v_nonDef_ptcone40;
    v_ptcone40               	.reserve( NMAX ); 
    v_nonDef_ptcone40	        .reserve( NMAX );
    vector<TH1F*>            	v_promptptcone40;
    v_promptptcone40         	.reserve( NMAX );
 
    cout<<endl;
    cout<<"**           Reserving "<< NMAX <<" capacity() for vector of histos           **"<<endl;
    cout<<endl;

    for(int ff=0; ff<sizeof(flavour) / sizeof(flavour[0]); ff++) {
      int flavID(-99);
      if (ff==0) flavID = 11;
      if (ff==1) flavID = 13;
      if (ff!=0&&ff!=1) cout<<"!!!!!!!!! CHECK FLAVOURS!!!!!!!"<<endl;

      double dataYield(-999.), ttbarYield(-999.), ZjetsYield(-999.);
      double error_T(-999.), error_Z(-999.);

      //Summing up all MC yields
      string histName3 = string()+"htotMC_"+"_CR"+to_string(cr)+"_"+flavour[ff];
      TH1F   *hMC  = new TH1F(histName3.c_str(),"",11, -0.5, 10.5);
      hMC->Sumw2();
      //Summing up all MC yields BUT THE ENRICHED BACKGROUND (to be corrected from data!)
      string histName4 = string()+"htotMCnoEnrBack_"+"_CR"+to_string(cr)+"_"+flavour[ff];
      TH1F   *hMC_noEnr  = new TH1F(histName4.c_str(),"",11, -0.5, 10.5);
      hMC_noEnr->Sumw2();
      //Summing up all MC yields BUT ttbar and Z+jets
      string histName5 = string()+"htotMCnottZjets_"+"_CR"+to_string(cr)+"_"+flavour[ff];
      TH1F   *hMC_oth  = new TH1F(histName5.c_str(),"",11, -0.5, 10.5);
      hMC_oth->Sumw2();
      
      //HISTOS FOR COEFFICIENTS COMPUTATION
      //electrons
      string hist_e_C = string()+"h_e_C"+"_CR"+to_string(cr)+"_"+flavour[ff];
      string hist_e_T = string()+"h_e_T"+"_CR"+to_string(cr)+"_"+flavour[ff];
      string hist_e_Z = string()+"h_e_Z"+"_CR"+to_string(cr)+"_"+flavour[ff];
      TH1F   *h_e_C   = new TH1F(hist_e_C.c_str(),"",11, -0.5, 10.5);
      TH1F   *h_e_T   = new TH1F(hist_e_T.c_str(),"",11, -0.5, 10.5);
      TH1F   *h_e_Z   = new TH1F(hist_e_Z.c_str(),"",11, -0.5, 10.5);
      h_e_C->Sumw2(); h_e_T->Sumw2(); h_e_Z->Sumw2();

      //muons
      string hist_mu_C = string()+"h_mu_C"+"_CR"+to_string(cr)+"_"+flavour[ff];
      string hist_mu_T = string()+"h_mu_T"+"_CR"+to_string(cr)+"_"+flavour[ff];
      string hist_mu_Z = string()+"h_mu_Z"+"_CR"+to_string(cr)+"_"+flavour[ff];
      TH1F   *h_mu_C   = new TH1F(hist_mu_C.c_str(),"",11, -0.5, 10.5);
      TH1F   *h_mu_T   = new TH1F(hist_mu_T.c_str(),"",11, -0.5, 10.5);
      TH1F   *h_mu_Z   = new TH1F(hist_mu_Z.c_str(),"",11, -0.5, 10.5);
      h_mu_C->Sumw2(); h_mu_T->Sumw2(); h_mu_Z->Sumw2();

      cout<<endl;
      cout<<"******************************************************"<<endl;
      cout<<"**********        Lepton flavour: "<< flavour[ff] <<"        ***********"<<endl;
      cout<<"******************************************************"<<endl;
      cout<<"Flavour ID for selection: "<< flavID <<endl;
      cout<<endl;
      /****** Save numbers in output .txt ******/
      lines.push_back(string()+"Fake "+flavour[ff]+" in CR"+to_string(cr+1)+"\n");
      string numString;
      /****** Save numbers in output .txt ******/
      double FF = lambda[ff][cr];
      if ( correction ) {
	cout<<"FF extracted in this CR and for this flavour: "<< FF <<endl;
	cout<<endl;
      }
      
      //Position of data.root, ttbar.root and Zjets.root among diff root files
      int n_data(-999.), n_ttbar(-999.), n_Zjets(-999.);

      /* Loop over the different samples */
      for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {
	cout<<endl;
	cout<<"****                    Processing "<< inputFile.at(t)->GetName() <<"                    ****"<<endl;
	cout<<endl;
			      
	/* Defining histos for events counting */
	string histName  = string()+"hN_"+inputFile.at(t)->GetName()+"_CR"+to_string(cr)+"_"+flavour[ff]; 
	string histName2 = string()+"hFakes_"+inputFile.at(t)->GetName()+"_CR"+to_string(cr)+"_"+flavour[ff];
	
	TH1F   *hN   = new TH1F(histName.c_str(),"", 11, -0.5, 10.5);
	TH1F   *hF   = new TH1F(histName2.c_str(),"",11, -0.5, 10.5);	
	hN     ->Sumw2();
	hF     ->Sumw2();

	/*******************/
	/**** PLOTTING *****/
	/*******************/
	string histFakeOrigin = string()+"h_fakeOrigin_"+inputFile.at(t)->GetName()+"_CR"+to_string(cr)+"_"+flavour[ff];
	TH1F   *hfakOr = new TH1F(histFakeOrigin.c_str(),"",55,-4.5,50.5);
	hfakOr->Sumw2();
		
	string histnonDefptcone40 = string()+"h_nonDefptcone40_"+inputFile.at(t)->GetName()+"_CR"+to_string(cr)+"_"+flavour[ff];
	TH1F   *hnonDefptcone40 = new TH1F(histnonDefptcone40.c_str(),"",40,-0.5,39.5);
	hnonDefptcone40->Sumw2();

	string histptcone40 = string()+"h_ptcone40_"+inputFile.at(t)->GetName()+"_CR"+to_string(cr)+"_"+flavour[ff];
	TH1F   *hptcone40 = new TH1F(histptcone40.c_str(),"",40,-0.5,39.5);
	hptcone40->Sumw2();

	string histPromptptcone40 = string()+"h_Promptptcone40_"+inputFile.at(t)->GetName()+"_CR"+to_string(cr)+"_"+flavour[ff];
	TH1F   *hPromptptcone40 = new TH1F(histPromptptcone40.c_str(),"",40,-0.5,39.5);
	hPromptptcone40->Sumw2();
	
	//OLD STUFF
	string hist_m2l_Z = string()+"h_m2l_Z_"+"CR"+to_string(cr)+"_"+flavour[ff]+"_"+inputFile.at(t)->GetName();
        TH1F   *h_m2l_Z = new TH1F(hist_m2l_Z.c_str(),"", 200, 0., 400.);
        h_m2l_Z->Sumw2();
 	string hist_fakel_pt = string()+"h_fakel_pt_"+"CR"+to_string(cr)+"_"+flavour[ff]+"_"+inputFile.at(t)->GetName();
        TH1F   *h_fakel_pt = new TH1F(hist_fakel_pt.c_str(),"", 200, 0., 200.);
        h_fakel_pt->Sumw2();
	string hist_m3l = string()+"h_m3l_"+"CR"+to_string(cr)+"_"+flavour[ff]+"_"+inputFile.at(t)->GetName();
        TH1F   *hm3l = new TH1F(hist_m3l.c_str(),"", 200, 0., 400.);
        hm3l->Sumw2();
	string hist_mt = string()+"h_mt_"+"CR"+to_string(cr)+"_"+flavour[ff]+"_"+inputFile.at(t)->GetName();
        TH1F   *hmt = new TH1F(hist_mt.c_str(),"", 200, 0., 400.);
        hmt->Sumw2();
	string hist_etmiss = string()+"hist_etmiss_"+"CR"+to_string(cr)+"_"+flavour[ff]+"_"+inputFile.at(t)->GetName();
        TH1F   *h_etmiss = new TH1F(hist_etmiss.c_str(),"", 200, 0., 400.);
        h_etmiss->Sumw2();
	string hist_jets = string()+"hist_jets_"+"CR"+to_string(cr)+"_"+flavour[ff]+"_"+inputFile.at(t)->GetName();
        TH1F   *h_jets = new TH1F(hist_jets.c_str(),"", 12, -0.5, 11.5);
        h_jets->Sumw2();
        string hist_jet_pt = string()+"hist_jet_pt_"+"CR"+to_string(cr)+"_"+flavour[ff]+"_"+inputFile.at(t)->GetName();
        TH1F   *h_jet_pt = new TH1F(hist_jet_pt.c_str(),"", 200, 0., 400.);
        h_jet_pt->Sumw2();
	//Lepton pT in pT order
	string hist_l1_pt = string()+"hist_l1_pt_"+"CR"+to_string(cr)+"_"+flavour[ff]+"_"+inputFile.at(t)->GetName();
        TH1F   *h_l1_pt = new TH1F(hist_l1_pt.c_str(),"", 300, 0., 300.);
        h_l1_pt->Sumw2();
        string hist_l2_pt = string()+"hist_l2_pt_"+"CR"+to_string(cr)+"_"+flavour[ff]+"_"+inputFile.at(t)->GetName();
        TH1F   *h_l2_pt = new TH1F(hist_l2_pt.c_str(),"", 200, 0., 200.);
        h_l2_pt->Sumw2();
        string hist_l3_pt = string()+"hist_l3_pt_"+"CR"+to_string(cr)+"_"+flavour[ff]+"_"+inputFile.at(t)->GetName();
        TH1F   *h_l3_pt = new TH1F(hist_l3_pt.c_str(),"", 100, 0., 100.);
        h_l3_pt->Sumw2();	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	Long64_t nentries = v_fChainSkim.at(t)->GetEntriesFast();

	/* Looping over the events */
	for (Long64_t ievt=0; ievt<nentries;ievt++) {
	  int trash=nTree.at(t)->GetEntry(ievt);
	  /* BOOLEANS */
	  bool goodOne(false);
	  bool fake   (false); 
	  //Are there fakes?
	  fake        = isFake(nTree.at(t));
	  //Which is (most likely) the fake lepton? It depends on the considered CR
	  int fake_lep(-999);
	  if(n_CR==1) fake_lep = fakeInd(nTree.at(t));
	  //if(n_CR==1) fake_lep = fakeIndTry(nTree.at(t)); //To check M3l
	  if(n_CR==2) {
	    if (nTree.at(t)->lepton_pt->at(1)>nTree.at(t)->lepton_pt->at(2))     fake_lep = 2;
	    else         							 fake_lep = 1;	
	  }
	  //Flavour of the fake 
	  int fakeID(-999);
	  fakeID = nTree.at(t)->lepton_ID->at(fake_lep);
	  //Selection of events ( ON TOP OF SKIMMING!!! )
	  //Compute transverse mass btw Et_miss and lepton not being part of the best Z-candidate
	  float m_t(-999);
	  m_t = newTransMass(nTree.at(t),fake_lep)/1000.;
	  //Further selection
	  if ( (n_CR==1)&&m_t<50.&&abs(fakeID)==flavID                            ) goodOne = true; //CR1
	  if ( (n_CR==2)&&abs(fakeID)==flavID                                     ) goodOne = true; //CR2
	  if ( !( nTree.at(t)->RunYear==Run || (Run!=2015&&Run!=2016) )           ) goodOne = false;
	  if ( (pTfakeLep)&&(nTree.at(t)->lepton_pt->at(fake_lep)/1000.<15.)      ) goodOne = false;       
	  //Event weight
	  double weight(0.); 
	  if( !bjet ) weight = nTree.at(t)->mcWeightOrg*nTree.at(t)->xsec*(1./nTree.at(t)->totweight)*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight; 
	  else        weight = nTree.at(t)->mcWeightOrg*nTree.at(t)->xsec*(1./nTree.at(t)->totweight)*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->MV2c10_70_EventWeight;
	  //const double weight = 1.; 

	  if( goodOne ) {
	    hN->Fill(nTree.at(t)->lepton_pt->size(),weight);
	    if( fake ) hF->Fill(nTree.at(t)->lepton_pt->size(),weight);
	    if(plot) {
	      //PLOTTING
	      //hfakOr->Fill(nTree.at(t)->lepton_truthOrigin->at(fake_lep),weight);
	      if( nTree.at(t)->lepton_ptvarcone40->at(fake_lep)/1000.>10. ) hfakOr->Fill(nTree.at(t)->lepton_truthOrigin->at(fake_lep),weight);
	      hptcone40->Fill(nTree.at(t)->lepton_ptvarcone40->at(fake_lep)/1000.,weight);
	      if(nTree.at(t)->lepton_truthOrigin->at(fake_lep)==0) hnonDefptcone40->Fill(nTree.at(t)->lepton_ptvarcone40->at(fake_lep)/1000.,weight);
	      hPromptptcone40->Fill(nTree.at(t)->lepton_ptvarcone40->at(0)/1000.,weight);
	      
	      //OLD STUFF
	      h_m2l_Z->Fill(Zmass(nTree.at(t),fake_lep),weight);
	      h_fakel_pt->Fill(nTree.at(t)->lepton_pt->at(fake_lep)/1000.,weight);
	      hm3l->Fill(threeLmass(nTree.at(t)),weight);
	      hmt->Fill(m_t,weight);
	      h_etmiss->Fill(nTree.at(t)->MET_RefFinal_et/1000.,weight);	
	      h_jets->Fill(nTree.at(t)->m_jet_pt->size(),weight);	
	      for(int kj=0; kj<nTree.at(t)->m_jet_pt->size(); kj++) h_jet_pt->Fill(nTree.at(t)->m_jet_pt->at(kj),weight);
	      h_l1_pt->Fill(nTree.at(t)->lepton_pt->at(0)/1000.,weight);
	      h_l2_pt->Fill(nTree.at(t)->lepton_pt->at(1)/1000.,weight);
	      h_l3_pt->Fill(nTree.at(t)->lepton_pt->at(2)/1000.,weight);
	      //////////////////////////////////////////////////////////////////////////////
	    }
	  }//if( goodOne )
	}//for (Long64_t ievt=0; ievt<nentries;ievt++) {
	v_hN    .push_back( hN );
	v_hFakes.push_back( hF );
	//plotting
	if( (((string)inputFile.at(t)->GetName()).find("data"))==string::npos ) v_fakeOrig.push_back( hfakOr );
	if( (((string)inputFile.at(t)->GetName()).find("data"))==string::npos ) v_ptcone40.push_back( hptcone40 );
	if( (((string)inputFile.at(t)->GetName()).find("data"))==string::npos ) v_promptptcone40.push_back( hPromptptcone40 );
	if( (((string)inputFile.at(t)->GetName()).find("data"))==string::npos ) v_nonDef_ptcone40.push_back( hnonDefptcone40 );
         

	//Print numbers
	if( (((string)inputFile.at(t)->GetName()).find("data"))==string::npos ) cout<<"Number of "<< flavour[ff] <<" in "<< inputFile.at(t)->GetName() <<" events passing cuts at L="<< Lum/1000 <<" fb^(-1): "<< Lum*( v_hN.at(t)->GetBinContent(4) ) <<" +- "<< Lum*( v_hN.at(t)->GetBinError(4) ) <<" in CR n. "<< n_CR <<endl;
	if( (((string)inputFile.at(t)->GetName()).find("data"))==string::npos ) cout<<"Fake      "<< flavour[ff] <<" in "<< inputFile.at(t)->GetName() <<" events passing cuts at L="<< Lum/1000 <<" fb^(-1): "<< Lum*( v_hFakes.at(t)->GetBinContent(4) ) <<" +- "<< Lum*( v_hFakes.at(t)->GetBinError(4) ) <<" in CR n. "<< n_CR <<endl;
	if( (((string)inputFile.at(t)->GetName()).find("data"))==string::npos && v_hN.at(t)->GetBinContent(4)!=0 ) cout<<"Fakes in  "<< inputFile.at(t)->GetName() <<" (%) = "<< ( v_hFakes.at(t)->GetBinContent(4) ) / ( v_hN.at(t)->GetBinContent(4) )*100 <<" in CR n. "<< n_CR <<endl;
	//Keep ttbar and Z+jets Yields
	if( (((string)inputFile.at(t)->GetName()).find("ttbar"))!=string::npos||(((string)inputFile.at(t)->GetName()).find("410000.root"))!=string::npos ) {
	  n_ttbar    = t;
	  ttbarYield = Lum*( v_hN.at(t)->GetBinContent(4) );
	  error_T    = Lum*( v_hN.at(t)->GetBinError(4)   );
	}
	if( (((string)inputFile.at(t)->GetName()).find("Zjets"))!=string::npos ) {
	  n_Zjets    = t;
	  ZjetsYield = Lum*( v_hN.at(t)->GetBinContent(4) );
	  error_Z    = Lum*( v_hN.at(t)->GetBinError(4)   );
	}
	if( (((string)inputFile.at(t)->GetName()).find("data"))!=string::npos ) cout<<endl;
	if( (((string)inputFile.at(t)->GetName()).find("data"))!=string::npos ) cout<<"Data!! "<< flavour[ff] <<" passing cuts at L="<< Lum/1000 <<" fb^(-1): "<< v_hN.at(t)->GetBinContent(4)<<" +- "<< v_hN.at(t)->GetBinError(4) <<" in CR n. "<< n_CR <<endl;
	if( (((string)inputFile.at(t)->GetName()).find("data"))!=string::npos ) cout<<endl;
	if( (((string)inputFile.at(t)->GetName()).find("data"))!=string::npos ) {
	  dataYield = v_hN.at(t)->GetBinContent(4);
	  n_data    = t;
	}
	/****** Save numbers in output .txt ******/
	stringstream a;
	if( (((string)inputFile.at(t)->GetName()).find("data"))==string::npos ) {
	  a << Lum*( v_hN.at(t)->GetBinContent(4) ) <<" $\\pm$ "<< Lum*( v_hN.at(t)->GetBinError(4) )<<"\t";
	} else{
	  a << v_hN.at(t)->GetBinContent(4) <<" $\\pm$ "<< v_hN.at(t)->GetBinError(4)<<"\t";
	}
	numString += a.str();
	/****** Save numbers in output .txt ******/
      }//for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {

      /* Save plots in an output folder */
      if (plot) {
	TString pdfTitle = string()+outFold+"fakeCandidateOrigin_CR"+to_string(cr)+"_"+flavour[ff]+"_"".pdf";
	TString histName = string()+"fakeOrig_CR"+to_string(cr)+"_"+flavour[ff];
	TH1F *fakeOrig = new TH1F(histName,"",v_fakeOrig[0]->GetNbinsX(),v_fakeOrig[0]->GetBin(1),v_fakeOrig[0]->GetBin(v_fakeOrig[0]->GetNbinsX()));
	fakeOrig = PlotMC(Lum,v_fakeOrig,pdfTitle,"fake candidate origin","");
	
	TString pdfTitle3 = string()+outFold+"nonDefCandidatePtCone40_CR"+to_string(cr)+"_"+flavour[ff]+"_"".pdf";
        TString histName3 = string()+"nonDefPtCone40_CR"+to_string(cr)+"_"+flavour[ff];
        TH1F *nonDefptcone40 = new TH1F(histName3,"",v_fakeOrig[0]->GetNbinsX(),v_fakeOrig[0]->GetBin(1),v_fakeOrig[0]->GetBin(v_fakeOrig[0]->GetNbinsX()));
        nonDefptcone40 = PlotMC(Lum,v_nonDef_ptcone40,pdfTitle3,"non def candidate p_{T} cone 40 [GeV]"," [GeV]");

	TString pdfTitle1 = string()+outFold+"fakeCandidatePtCone40_CR"+to_string(cr)+"_"+flavour[ff]+"_"".pdf";
	TString histName1 = string()+"fakePtCone40_CR"+to_string(cr)+"_"+flavour[ff];
	TH1F *ptcone40 = new TH1F(histName1,"",v_fakeOrig[0]->GetNbinsX(),v_fakeOrig[0]->GetBin(1),v_fakeOrig[0]->GetBin(v_fakeOrig[0]->GetNbinsX()));
	ptcone40 = PlotMC(Lum,v_ptcone40,pdfTitle1,"fake candidate p_{T} cone 40 [GeV]"," [GeV]");

	TString pdfTitle2 = string()+outFold+"promptPtCone40_CR"+to_string(cr)+"_"+flavour[ff]+"_"".pdf";
	TString histName2 = string()+"promptPtCone40_CR"+to_string(cr)+"_"+flavour[ff];
	TH1F *promptptcone40 = new TH1F(histName2,"",v_fakeOrig[0]->GetNbinsX(),v_fakeOrig[0]->GetBin(1),v_fakeOrig[0]->GetBin(v_fakeOrig[0]->GetNbinsX()));
	promptptcone40 = PlotMC(Lum,v_promptptcone40,pdfTitle2,"prompt p_{T} cone 40 [GeV]"," [GeV]");
	
      }
	
      //Summing up MC yields
      hMC = (TH1F*)v_hN.at(0)->Clone();
      for(int s = 1; s<v_hN.size()-1;s++) hMC->Add(v_hN.at(s));
      cout<<"Number of "<< flavour[ff] <<" in total MC passing cuts at L="<< Lum/1000 <<" fb^(-1):      "<< Lum*( hMC->GetBinContent(4) ) <<" +- "<< Lum*( hMC->GetBinError(4) ) <<" in CR n. "<< n_CR <<endl;
      cout<<endl;
      //Summing up MC yields BUT THE ENRICHED BACK
      hMC_noEnr = (TH1F*)v_hN.at(0)->Clone();
      for(int s = 1; s<v_hN.size()-1;s++) {
	if( (((string)inputFile.at(s)->GetName()).find(EnrichedBack))==string::npos ) hMC_noEnr->Add(v_hN.at(s));
      }
      cout<<"Number of "<< flavour[ff] <<" in MC-"<< EnrichedBack <<" passing cuts at L="<< Lum/1000 <<" fb^(-1): "<< Lum*( hMC_noEnr->GetBinContent(4) ) <<" +- "<< Lum*( hMC_noEnr->GetBinError(4) ) <<" in CR n. "<< n_CR <<endl;
      cout<<endl;
      //Summing up all other MC (but ttbar and Z+jets) for computational purposes
      hMC_oth = (TH1F*)v_hN.at(0)->Clone();
      for(int s = 1; s<v_hN.size()-1;s++) {
	if( (((string)inputFile.at(s)->GetName()).find("Zjets"))==string::npos&&(((string)inputFile.at(s)->GetName()).find("ttbar"))==string::npos&&(((string)inputFile.at(s)->GetName()).find("410000"))==string::npos ) hMC_oth->Add(v_hN.at(s));
      }
      cout<<"Number of "<< flavour[ff] <<" in MC-(ttbar + Z+jets) passing cuts at L="<< Lum/1000 <<" fb^(-1): "<< Lum*( hMC_oth->GetBinContent(4) ) <<" +- "<< Lum*( hMC_oth->GetBinError(4) ) <<" in CR n. "<< n_CR <<endl;
      cout<<endl;
      /****** Save numbers in output .txt ******/
      stringstream a;
      a << Lum*( hMC->GetBinContent(4) ) <<" $\\pm$ "<< Lum*( hMC->GetBinError(4) )<<"\n";
      numString += a.str();
      lines.push_back(numString);
      /****** Save numbers in output .txt ******/

      //**** COMPUTATION ****//
      //**** THROUGH HISTOS ****//
      //electrons
      if (ff==0) {
	//N_data - N_MC_others
	h_e_C = (TH1F*)v_hN.at(n_data)->Clone();
	h_e_C ->Add(hMC_oth,-Lum);
	//cout<<"C from histogram = "<< h_e_C ->GetBinContent(4) <<" +- "<< h_e_C ->GetBinError(4) <<" in CR n. "<< n_CR <<endl;
	// N_MC_ttbar
	h_e_T = (TH1F*)v_hN.at(n_ttbar)->Clone();
	h_e_T ->Scale(Lum);
	//cout<<"T from histogram = "<< h_e_T ->GetBinContent(4) <<" +- "<< h_e_T ->GetBinError(4) <<" in CR n. "<< n_CR <<endl;
	// N_MC_Zjets
	h_e_Z = (TH1F*)v_hN.at(n_Zjets)->Clone();
	h_e_Z ->Scale(Lum);
	//cout<<"Z from histogram = "<< h_e_Z ->GetBinContent(4) <<" +- "<< h_e_Z ->GetBinError(4) <<" in CR n. "<< n_CR <<endl;
	//Store Histos
	v_hist_C_e .push_back(h_e_C);
	v_hist_T_e .push_back(h_e_T);
	v_hist_Z_e .push_back(h_e_Z);
      }
      //muons
      if (ff==1) {
	//N_data - N_MC_others
	h_mu_C = (TH1F*)v_hN.at(n_data)->Clone();
	h_mu_C ->Add(hMC_oth,-Lum);
	//cout<<"C from histogram = "<< h_mu_C ->GetBinContent(4) <<" +- "<< h_mu_C->GetBinError(4) <<" in CR n. "<< n_CR <<endl;
	// N_MC_ttbar
	h_mu_T = (TH1F*)v_hN.at(n_ttbar)->Clone();
	h_mu_T ->Scale(Lum);
	//cout<<"T from histogram = "<< h_mu_T ->GetBinContent(4) <<" +- "<< h_mu_T->GetBinError(4) <<" in CR n. "<< n_CR <<endl;
	// N_MC_Zjets
	h_mu_Z = (TH1F*)v_hN.at(n_Zjets)->Clone();
	h_mu_Z ->Scale(Lum);
	//cout<<"Z from histogram = "<< h_mu_Z ->GetBinContent(4) <<" +- "<< h_mu_Z->GetBinError(4) <<" in CR n. "<< n_CR <<endl;
	//Store Histos
	v_hist_C_mu .push_back(h_mu_C);
	v_hist_T_mu .push_back(h_mu_T);
	v_hist_Z_mu .push_back(h_mu_Z);
      }
      //**** THROUGH HISTOS ****//
      
      //Computations devoted to cross-checks
      double coef_C(999.), error_C(999.);
      if (ff==0) {
	coef_C  = h_e_C->GetBinContent(4);
	error_C = h_e_C->GetBinError  (4);
      }
      if (ff==1) {
	coef_C  = h_mu_C->GetBinContent(4);
	error_C = h_mu_C->GetBinError  (4);
      }
      cout<<"C = "<< coef_C <<", data = "<< dataYield <<", MC others = "<< Lum*( hMC_oth->GetBinContent(4) ) <<endl;
      C       .push_back( coef_C  );
      err_C   .push_back( error_C );
      
      double coef_T(999.);
      coef_T = ttbarYield;
      if(error_T>coef_T) error_T = coef_T;
      T       .push_back( coef_T  );
      err_T   .push_back( error_T );

      double coef_Z(999.);
      coef_Z = ZjetsYield;
      if(error_Z>coef_Z) error_Z = coef_Z;
      Z       .push_back( coef_Z  );
      err_Z   .push_back( error_Z );
      
      cout<<"C = "<< coef_C <<" +- "<< error_C <<", T = "<< coef_T <<" +- "<< error_T <<", Z = "<< coef_Z <<" +- "<< error_Z <<endl;

      v_hN    .clear();
      v_hFakes.clear();
      v_fakeOrig.clear();
      v_ptcone40.clear();
      v_promptptcone40.clear();
      v_nonDef_ptcone40.clear();
    }//for(int ff=0; ff<sizeof(flavour) / sizeof(flavour[0]); ff++) {
  }//for(int cr=0; cr<sizeof(CRs) / sizeof(CRs[0]); cr++) { 
  cout<<endl;
  cout<<"------------------------------------- LOOPS OVER! COMPUTATION STARTS! -------------------------------------"<<endl;

  //**** FFs' COMPUTATION THROUGH HISTOS ****//
  //Electrons from b-jets
  TH1F *h_lam_b_e = (TH1F*)v_hist_C_e.at(0)->Clone();
  TH1F *h_T  = (TH1F*)v_hist_T_e.at(0)->Clone();
  TH1F *h_T1 = (TH1F*)v_hist_T_e.at(1)->Clone();
  TH1F *h_C1 = (TH1F*)v_hist_C_e.at(1)->Clone();
  //Computing CRAMER's factor
  h_lam_b_e    ->Multiply(v_hist_Z_e.at(1));
  //cout<<"C*Z' = "<< h_lam_b_e->GetBinContent(4) <<" +- "<< h_lam_b_e->GetBinError(4) <<endl;
  h_C1         ->Multiply(v_hist_Z_e.at(0));
  //cout<<"C'*Z = "<< h_C1->GetBinContent(4) <<" +- "<< h_C1->GetBinError(4) <<endl;
  h_lam_b_e    ->Add(h_C1,-1);
  //cout<<"C*Z'-C'*Z = "<< h_lam_b_e->GetBinContent(4) <<" +- "<< h_lam_b_e->GetBinError(4) <<endl;
  h_T  ->Multiply(v_hist_Z_e.at(1));
  //cout<<"T*Z' = "<< h_T->GetBinContent(4) <<" +- "<< h_T->GetBinError(4) <<endl;
  h_T1 ->Multiply(v_hist_Z_e.at(0));
  //cout<<"T'*Z = "<< h_T1->GetBinContent(4) <<" +- "<< h_T1->GetBinError(4) <<endl;
  h_T  ->Add(h_T1,-1);
  //cout<<"T*Z'-T'*Z = "<< h_T->GetBinContent(4) <<" +- "<< h_T->GetBinError(4) <<endl;
  h_lam_b_e        ->Divide(h_T);
  //cout<<"lamda_e_b = "<< h_lam_b_e->GetBinContent(4) <<" +- "<< h_lam_b_e->GetBinError(4) <<endl;

  //Electrons from light jets
  TH1F *h_lam_l_e = (TH1F*)v_hist_T_e.at(0)->Clone();
  h_lam_l_e        ->Multiply(v_hist_C_e.at(1));  
  v_hist_T_e.at(1) ->Multiply(v_hist_C_e.at(0));  
  h_lam_l_e        ->Add(v_hist_T_e.at(1),-1);

  h_lam_l_e        ->Divide(h_T);
  //Muons from b-jets
  TH1F *h_lam_b_mu = (TH1F*)v_hist_C_mu.at(0)->Clone();
  TH1F *h_Tmu  = (TH1F*)v_hist_T_mu.at(0)->Clone();
  TH1F *h_T1mu = (TH1F*)v_hist_T_mu.at(1)->Clone();
  TH1F *h_C1mu = (TH1F*)v_hist_C_mu.at(1)->Clone();
  //h_lam_b_mu->Sumw2(); h_Tmu->Sumw2(); h_T1mu->Sumw2(); h_C1mu->Sumw2();
  //Computing CRAMER's factor
  h_lam_b_mu    ->Multiply(v_hist_Z_mu.at(1));
  h_C1mu        ->Multiply(v_hist_Z_mu.at(0));
  h_lam_b_mu    ->Add(h_C1mu,-1);
  h_Tmu  ->Multiply(v_hist_Z_mu.at(1));
  h_T1mu ->Multiply(v_hist_Z_mu.at(0));
  h_Tmu  ->Add(h_T1mu,-1);

  h_lam_b_mu        ->Divide(h_Tmu);
  //Muons from light jets
  TH1F *h_lam_l_mu = (TH1F*)v_hist_T_mu.at(0)->Clone();
  //h_lam_l_mu ->Sumw2();
  h_lam_l_mu        ->Multiply(v_hist_C_mu.at(1));  
  v_hist_T_mu.at(1) ->Multiply(v_hist_C_mu.at(0));  
  h_lam_l_mu        ->Add(v_hist_T_mu.at(1),-1);

  h_lam_l_mu        ->Divide(h_Tmu);

  //**** Compute RIGHT ERRORS on the FFS ****//
  //Error on l_b_e = (C.at(0)*Z.at(2) - C.at(2)*Z.at(0)) / (T.at(0)*Z.at(2) - T.at(2)*Z.at(0));
  double den(-999.), den1(-999.), err_l_b_e(-999.), err_l_b_mu(-999.);
  double err_l_l_e(-999.), err_l_l_mu(-999.); 
  den  = (T.at(0)*Z.at(2) - T.at(2)*Z.at(0));
  den1 = (T.at(1)*Z.at(3) - T.at(3)*Z.at(1));
  err_l_b_e  = fakeFactorErr3l(den , 0, 2, C, T, Z, err_C, err_T, err_Z);
  //cout<< err_l_b_e <<endl;
  err_l_b_mu = fakeFactorErr3l(den1, 1, 3, C, T, Z, err_C, err_T, err_Z);
  err_l_l_e  = fakeFactorErr3l(den , 0, 2, C, Z, T, err_C, err_Z, err_T);
  cout<<"!!!!!!!!!!! err_l_l_mu !!!!!!!!"<<endl;
  err_l_l_mu = fakeFactorErr3l(den1, 1, 3, C, Z, T, err_C, err_Z, err_T);
  //**** Compute RIGHT ERRORS on the FFS ****//

  cout<<"-------------------------------------   RESULTS FROM FF COMPUTATION   -------------------------------------"<<endl;
  cout<<endl;
  cout<<"       Electrons' fake factors: "<<endl;
  cout<<"       Lambda_e_b from histos = "<< h_lam_b_e->GetBinContent(4) <<" +- "<< err_l_b_e <<endl;
  cout<<"       Lambda_e_l from histos = "<< h_lam_l_e->GetBinContent(4) <<" +- "<< err_l_l_e <<endl;
  cout<<endl;
  cout<<"       Muons' fake factors: "<<endl;
  cout<<"       Lambda_mu_b from histos = "<< h_lam_b_mu->GetBinContent(4) <<" +- "<< err_l_b_mu <<endl;
  cout<<"       Lambda_mu_l from histos = "<< h_lam_l_mu->GetBinContent(4) <<" +- "<< err_l_l_mu <<endl;
  cout<<endl;
  cout<<"-------------------------------------   RESULTS FROM FF COMPUTATION   -------------------------------------"<<endl;
  cout<<endl;

  cout<<endl;
  cout<<"------------------------------------- FAKE FACTOR COMP COMES TO END!  -------------------------------------"<<endl;
  cout<<endl;

  clock_t end3 = clock();
  double  elapsed_secs3 = double(end3 - begin) / CLOCKS_PER_SEC;

  double  hours(-1), mins(-1), secs(-1);
  hours   = (int) elapsed_secs3 / 3600;
  mins    = (int) ((elapsed_secs3 / 3600 - ((int) elapsed_secs3 / 3600))*60);
  secs    = ( elapsed_secs3 / 3600 - hours - mins/60 )*3600; 
  cout    <<endl;
  cout    <<"**        FF computation has been completed! Time elapsed: "<< hours <<" h "<< mins  <<" m "<< secs <<" s        **"<<endl;
  cout    <<endl;
  /****** Save numbers in output .txt ******/
  for(int a=0; a<lines.size(); a++) txtFile << lines.at(a);
  txtFile.close();
  /****** Save numbers in output .txt ******/
  f->Write();
  //f->Close();

  return 0;
}
