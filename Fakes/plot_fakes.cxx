///////////////////////// -*- C++ -*- /////////////////////////////
// ttH->4l Shape Analysis
// Author: A.Sciandra <andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TChain.h"
#include "TFile.h"
#include "TRandom3.h"
#include <string.h>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <string>
#include <ctime>
#include <sstream>

#include "../ttHshape/header.h"
#include "../ttHshape/plot_shapes.h"

using namespace std      ;

int main() {
  clock_t begin       = clock();
  bool    plot        = true;
  bool    debug       = false;
  //Number of leps + 1
  //int     n_leps(4); 
  int     n_leps(5); 
  //const char* inFold  = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v26/02/";
  const char* inFold  = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v27/01/";
  //const char* inFold  = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v23/version_1/";
  const char* outFold = "/afs/cern.ch/user/a/asciandr/www/ttH/fake_plots/";
  //new PLI on 3rd and 4th leading lepton
  TString PreCut = "quadlep_type&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatch_3||lep_isTrigMatchDLT_0||lep_isTrigMatchDLT_1||lep_isTrigMatchDLT_2||lep_isTrigMatchDLT_3)&&!total_charge&&(lep_ID_0!=-lep_ID_1||Mll01>12e3&&abs(Mll01-91.2e3)>10e3)&&(lep_ID_0!=-lep_ID_2||Mll02>12e3&&abs(Mll02-91.2e3)>10e3)&&(lep_ID_0!=-lep_ID_3||Mll03>12e3&&abs(Mll03-91.2e3)>10e3)&&(lep_ID_1!=-lep_ID_2||Mll12>12e3&&abs(Mll12-91.2e3)>10e3)&&(lep_ID_1!=-lep_ID_3||Mll13>12e3&&abs(Mll13-91.2e3)>10e3)&&(lep_ID_2!=-lep_ID_3||Mll23>12e3&&abs(Mll23-91.2e3)>10e3)&&nJets_OR>1&&nJets_OR_MV2c10_70&&abs(Mllll0123-125e3)>5e3&&(lep_promptLeptonIso_TagWeight_2<-0.5&&lep_promptLeptonIso_TagWeight_3<-0.5)";
  //3L NON-PROMPT CR
  TString CR3L = "(trilep_type>0 && (lep_isTrigMatch_0 || lep_isTrigMatch_1 || lep_isTrigMatch_2 || matchDLTll01 || matchDLTll02 || matchDLTll12) && (nJets_OR_T==1 || nJets_OR_T==2) && ((((lep_ID_0+lep_ID_1==0 && abs(Mll01/1000.-91.2)<10.) || (lep_ID_0+lep_ID_2==0 && abs(Mll02/1000.-91.2)<10.)) && (MET_RefFinal_et/1000.<30.)) || (!((lep_ID_0+lep_ID_1==0) || (lep_ID_0+lep_ID_2==0)))) && ((lep_Pt_1>=lep_Pt_2 && lep_promptLeptonIso_TagWeight_2 < -0.5 && ((abs(lep_ID_2)==13 && lep_isolationLoose_2 ) || (abs(lep_ID_2)==11 && lep_isolationLoose_2 && lep_isTightLH_2))) || (lep_Pt_2>=lep_Pt_1 && lep_promptLeptonIso_TagWeight_1 < -0.5 && ((abs(lep_ID_1)==13 && lep_isolationLoose_1 ) || (abs(lep_ID_1)==11 && lep_isolationLoose_1 && lep_isTightLH_1)))) && (lep_ID_0!=-lep_ID_1 || (Mll01>12e3)) && (lep_ID_0!=-lep_ID_2 || (Mll02>12e3))) && !((trilep_type>0 && (lep_isTrigMatch_0 || lep_isTrigMatch_1 || lep_isTrigMatch_2 || matchDLTll01 || matchDLTll02 || matchDLTll12) && (lep_promptLeptonIso_TagWeight_1 < -0.5 && ((abs(lep_ID_1)==13 && lep_isolationLoose_1 ) || (abs(lep_ID_1)==11 && lep_isolationLoose_1 && lep_isTightLH_1))) && (lep_promptLeptonIso_TagWeight_2 < -0.5 && ((abs(lep_ID_2)==13 && lep_isolationLoose_2 ) || (abs(lep_ID_2)==11 && lep_isolationLoose_2 && lep_isTightLH_2))) && (abs(total_charge)==1) && (nJets_OR_T>=2) && (nJets_OR_T_MV2c10_70>0) && abs(Mlll012-91.2e3)>10e3 && (!nTaus_OR_Pt25) && (lep_ID_0!=-lep_ID_1 || (Mll01>12e3)) && (lep_ID_0!=-lep_ID_2 || (Mll02>12e3)) && lep_Pt_1>15e3 && lep_Pt_2>15e3))";
  //TString ICHEPSR4l = "quadlep_type&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatch_3||lep_isTrigMatchDLT_0||lep_isTrigMatchDLT_1||lep_isTrigMatchDLT_2||lep_isTrigMatchDLT_3)&&!total_charge&&(lep_ID_0!=-lep_ID_1||Mll01>12e3&&abs(Mll01-91.2e3)>10e3)&&(lep_ID_0!=-lep_ID_2||Mll02>12e3&&abs(Mll02-91.2e3)>10e3)&&(lep_ID_0!=-lep_ID_3||Mll03>12e3&&abs(Mll03-91.2e3)>10e3)&&(lep_ID_1!=-lep_ID_2||Mll12>12e3&&abs(Mll12-91.2e3)>10e3)&&(lep_ID_1!=-lep_ID_3||Mll13>12e3&&abs(Mll13-91.2e3)>10e3)&&(lep_ID_2!=-lep_ID_3||Mll23>12e3&&abs(Mll23-91.2e3)>10e3)&&nJets_OR>1&&nJets_OR_MV2c10_70&&Mllll0123>100e3&&Mllll0123<350e3&&abs(Mllll0123-125e3)>5e3&&(lep_promptLeptonIso_TagWeight_2<-0.5&&lep_promptLeptonIso_TagWeight_3<-0.5)";
  //GRAD-ISO
  //TString ICHEPSR4l = "@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(lepton_isolationGradient[0]==1&&lepton_isolationGradient[1]==1&&lepton_isolationGradient[2]==1&&lepton_isolationGradient[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(Mllll0123/1000>100.&&Mllll0123/1000<350.)&&(RunYear==2015||RunYear==2016)";
  //TString fixCutLooseSR4l ="@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(lepton_isolationFixedCutLoose[0]==1&&lepton_isolationFixedCutLoose[1]==1&&lepton_isolationFixedCutLoose[2]==1&&lepton_isolationFixedCutLoose[3]==1)&&(RunYear==2015||RunYear==2016)&&(((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0)||(lepton_ID[0]+lepton_ID[3]==0)||(lepton_ID[1]+lepton_ID[2]==0)||(lepton_ID[1]+lepton_ID[3]==0)||(lepton_ID[2]+lepton_ID[3]==0)))";
   
  const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root","rare.root","Zjets_NNPDF.root","Wjets_NNPDF.root","ttbar_3lep.root","diboson_bfilter.root","ttW_aMcNloP8.root","singletop.root"};
  //const char*      files[] = {"ttH_aMcNloP8.root"};

  //float Lum_tot = 36470.2;
  //v27 luminosity
  float Lum_tot = 36074.6;
  float Lum(1.);
  Lum = Lum_tot       ;
  cout<<endl;
  cout<<"--------------> Running at an integrated luminosity of: "<< Lum/1000 <<" fb^(-1): "<<endl;
  int   NMAX =                (sizeof(files) / sizeof(files[0])) + 4.;
  vector<float>               Signal;
  vector<float>               Back  ;
  Signal                      .reserve(NMAX);
  Back                        .reserve(NMAX);
  cout<<endl;
  cout<<"   ///////////////////////// -*- C++ -*- /////////////////////////////"<<endl;
  cout<<"  // ttH->4l Shape Analysis                                        //"<<endl;
  cout<<" // Author: A.Sciandra <andrea.sciandra@cern.ch>                  //"<<endl;
  cout<<"///////////////////////////////////////////////////////////////////"<<endl;
  cout<<endl;
  if ( debug )      cout<<"!!    DEBUG mode                !!"<<endl;
  if ( plot  )      cout<<"!!    Plot option activated     !!"<<endl;
  cout<<endl;
  cout<<"** Reserving capacity() = "<< Signal.capacity() <<" for meaningful vectors       **"<<endl;
  cout<<endl;

  vector<TH1F*>     v_hN;
  v_hN              .reserve(NMAX);

  cout<<endl;
  cout<<"** Opening root Input Files                                             **"<<endl;
  cout<<endl;
  cout<<"** Looping on the following samples:                                    **"<<endl;
  cout<<endl;
  vector<TFile*>              inputFile;
  vector<TTree*>              v_fChain;
  inputFile                   .reserve( NMAX );
  v_fChain                    .reserve( NMAX );
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TString     string_i  = string()+inFold+files[i]   ;
    TTree       *chain;
    inputFile   .push_back(new TFile(string_i));
    inputFile   .at(i)->GetObject("nominal",chain);
    v_fChain    .push_back( chain );

    cout<< inputFile.at(i)->GetName() <<endl;
  }
  TString PreSelection = PreCut;
  //TString PreSelection = CR3L;
  //TString PreSelection = ICHEPSR4l;
  //TString PreSelection = fixCutLooseSR4l;
  
  cout    <<endl;
  cout    <<"********                    Applying the following PRESELECTION for Skimming:                   ********"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"''       "<< PreSelection <<"       ''"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"**                                           Skimming root files                                         **"<<endl;

  vector<TTree*>     v_fChainSkim;
  vector<nominal*>   nTree       ;
  v_fChainSkim       .reserve( NMAX );
  nTree              .reserve( NMAX );

  // OUTPUT FILES //
  TFile *f = new TFile("shapes.root","RECREATE");
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    v_fChainSkim .push_back( (TTree*)v_fChain.at(i)->CopyTree( PreSelection ) );
    nTree        .push_back( new nominal(v_fChainSkim.at(i))               );
  }
  clock_t end1 = clock();
  double  elapsed_secs = double(end1 - begin) / CLOCKS_PER_SEC;
  cout    <<endl;

  string histName3 = string()+"htotMCfakes";
  TH1F   *hMCfakes  = new TH1F(histName3.c_str(),"",11, -0.5, 10.5);
  hMCfakes->Sumw2();

  // Plotting //
  vector<TH1F*> v_numFakeL, v_fakeLpT, v_fakeEorig, v_fakeMorig, v_fakeLindex;
  v_numFakeL.reserve(NMAX); v_fakeLpT.reserve(NMAX); v_fakeEorig.reserve(NMAX); v_fakeMorig.reserve(NMAX); v_fakeLindex.reserve(NMAX);

  //Numbers needed for the paper
  //int   heavy[10] 	= {6,8,9,25,26,27,28,29,32,33};   
  //int   light[16] 	= {0,3,5,7,23,24,30,31,34,35,36,37,38,39,40,42};
  int	bHad[3]		= {26,29,33};	
  int	cHad[3]		= {25,27,32};	
  int	lHad[7]		= {23,24,30,31,34,35,42};	
  int	JPsi[1]		= {28};	
  int	Phot[6]		= {5,36,37,38,39,40};	
  int	Unkn[1]		= {0};	
  //
  string hist_bHad = string()+"h_bHad";
  TH1F   *h_bHad = new TH1F(hist_bHad.c_str() ,"", 5, -0.5, 4.5);
  h_bHad->Sumw2();

  string hist_cHad = string()+"h_cHad";
  TH1F   *h_cHad = new TH1F(hist_cHad.c_str() ,"", 5, -0.5, 4.5);
  h_cHad->Sumw2();
 
  string hist_lHad = string()+"h_lHad";
  TH1F   *h_lHad = new TH1F(hist_lHad.c_str() ,"", 5, -0.5, 4.5);
  h_lHad->Sumw2();

  string hist_JPsi = string()+"h_JPsi";
  TH1F   *h_JPsi = new TH1F(hist_JPsi.c_str() ,"", 5, -0.5, 4.5);
  h_JPsi->Sumw2();

  string hist_Phot = string()+"h_Phot";
  TH1F   *h_Phot = new TH1F(hist_Phot.c_str() ,"", 5, -0.5, 4.5);
  h_Phot->Sumw2();

  string hist_Unkn = string()+"h_Unkn";
  TH1F   *h_Unkn = new TH1F(hist_Unkn.c_str() ,"", 5, -0.5, 4.5);
  h_Unkn->Sumw2();

  string hist_Other = string()+"h_Other";
  TH1F   *h_Other = new TH1F(hist_Other.c_str() ,"", 5, -0.5, 4.5);
  h_Other->Sumw2();
  //Numbers needed for the paper

  for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {
    cout<<endl;
    cout<<"****                    Processing "<< inputFile.at(t)->GetName() <<"                    ****"<<endl;
    cout<<endl;

    string histnumFakeL = string()+"h_numFakeL_"+inputFile.at(t)->GetName();
    TH1F   *h_numFakeL = new TH1F(histnumFakeL.c_str() ,"", 5, -0.5, 4.5);
    h_numFakeL->Sumw2();

    string histfakeLpT = string()+"h_fakeLpT_"+inputFile.at(t)->GetName();
    TH1F   *h_fakeLpT = new TH1F(histfakeLpT.c_str() ,"", 25, 0., 100.);
    h_fakeLpT->Sumw2();

    string histfakeEorig = string()+"h_fakeEorig_"+inputFile.at(t)->GetName();
    TH1F   *h_fakeEorig = new TH1F(histfakeEorig.c_str() ,"", 45, -0.5, 44.5);
    h_fakeEorig->Sumw2();

    string histfakeMorig = string()+"h_fakeMorig_"+inputFile.at(t)->GetName();
    TH1F   *h_fakeMorig = new TH1F(histfakeMorig.c_str() ,"", 45, -0.5, 44.5);
    h_fakeMorig->Sumw2();

    string histfakeLindex = string()+"h_fakeLindex_"+inputFile.at(t)->GetName();
    TH1F   *h_fakeLindex = new TH1F(histfakeLindex.c_str() ,"", 8, -2.5, 5.5);
    h_fakeLindex->Sumw2();

    Long64_t nentries = v_fChainSkim.at(t)->GetEntriesFast();
    // Looping over the events //
    for (Long64_t ievt=0; ievt<nentries;ievt++) {
    //for (Long64_t ievt=0; ievt<10;ievt++) {
      int trash=nTree.at(t)->GetEntry(ievt);
      const double weight = nTree.at(t)->mcWeightOrg*nTree.at(t)->xsec*(1./nTree.at(t)->totweight)*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->MV2c10_70_EventWeight;
      // BOOLEANS //
      // Further selection (on top of skimming)
      bool goodOne(true);//(false);
      bool isFake(false);
      //COUNTS //
      int numFakeL(0.), fakeIndex(-1.), ORIG(-1);
      for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++) { 
	int orig=nTree.at(t)->lepton_truthOrigin->at(l);
	if( orig!=9 && orig!=10 && orig!=12 && orig!=13 && orig!=14 && orig!=43 && orig!=44 ) { 
	  isFake=true; numFakeL++; fakeIndex=l; ORIG=orig;
	  h_fakeLpT->Fill(nTree.at(t)->lepton_pt->at(l)/1000.,weight); 
     	  if(abs(nTree.at(t)->lepton_ID->at(l))==11) h_fakeEorig->Fill(orig,weight);
     	  if(abs(nTree.at(t)->lepton_ID->at(l))==13) h_fakeMorig->Fill(orig,weight);
     	  h_fakeLindex->Fill(fakeIndex,weight); 
	}
      }
      if(isFake) h_numFakeL->Fill(numFakeL,weight);	
      //if(isFake && abs(nTree.at(t)->lepton_ID->at(fakeIndex))==13) {
      if(isFake && abs(nTree.at(t)->lepton_ID->at(fakeIndex))==11) {
        hMCfakes->Fill(1,weight);
        bool noneOfThem(true);
        for(unsigned int i=0; i<sizeof(bHad)/sizeof(*bHad); i++) 	{ if(ORIG==bHad[i])  { noneOfThem=false; h_bHad ->Fill(1,weight);  continue; } }; 
        for(unsigned int i=0; i<sizeof(cHad)/sizeof(*cHad); i++) 	{ if(ORIG==cHad[i])  { noneOfThem=false; h_cHad ->Fill(1,weight);  continue; } }; 
        for(unsigned int i=0; i<sizeof(lHad)/sizeof(*lHad); i++) 	{ if(ORIG==lHad[i])  { noneOfThem=false; h_lHad ->Fill(1,weight);  continue; } }; 
        for(unsigned int i=0; i<sizeof(JPsi)/sizeof(*JPsi); i++) 	{ if(ORIG==JPsi[i])  { noneOfThem=false; h_JPsi ->Fill(1,weight);  continue; } }; 
        for(unsigned int i=0; i<sizeof(Phot)/sizeof(*Phot); i++) 	{ if(ORIG==Phot[i])  { noneOfThem=false; h_Phot ->Fill(1,weight);  continue; } }; 
        for(unsigned int i=0; i<sizeof(Unkn)/sizeof(*Unkn); i++) 	{ if(ORIG==Unkn[i])  { noneOfThem=false; h_Unkn ->Fill(1,weight);  continue; } }; 
        if(noneOfThem)							h_Other->Fill(1,weight);
      }
    }//for (Long64_t ievt=0; ievt<nentries;ievt++)
    v_numFakeL	.push_back(h_numFakeL);
    v_fakeLpT	.push_back(h_fakeLpT);
    v_fakeEorig	.push_back(h_fakeEorig);
    v_fakeMorig	.push_back(h_fakeMorig);
    v_fakeLindex.push_back(h_fakeLindex);

  }//for(int t=0; t<sizeof(files) / sizeof(files[0]); t++)

  //Numbers for the paper
  //cout<<endl<<"tot fake muons = 	"<< Lum*hMCfakes->GetBinContent(2) <<endl<<endl;
  cout<<endl<<"tot fake electrons = 	"<< Lum*hMCfakes->GetBinContent(2) <<endl<<endl;
  cout<<endl<<"B-Had fakes = 		"<< Lum*h_bHad->GetBinContent(2) 	<<" ("<< 100*h_bHad->GetBinContent(2)/hMCfakes->GetBinContent(2) <<" %)"<<endl;
  cout<<endl<<"C-Had fakes = 		"<< Lum*h_cHad->GetBinContent(2) 	<<" ("<< 100*h_cHad->GetBinContent(2)/hMCfakes->GetBinContent(2) <<" %)"<<endl;
  cout<<endl<<"J/Psi fakes = 		"<< Lum*h_JPsi->GetBinContent(2) 	<<" ("<< 100*h_JPsi->GetBinContent(2)/hMCfakes->GetBinContent(2) <<" %)"<<endl;
  cout<<endl<<"L-Had fakes = 		"<< Lum*h_lHad->GetBinContent(2) 	<<" ("<< 100*h_lHad->GetBinContent(2)/hMCfakes->GetBinContent(2) <<" %)"<<endl;
  cout<<endl<<"PhConv fakes = 		"<< Lum*h_Phot->GetBinContent(2) 	<<" ("<< 100*h_Phot->GetBinContent(2)/hMCfakes->GetBinContent(2) <<" %)"<<endl;
  cout<<endl<<"Other fakes = 		"<< Lum*h_Other->GetBinContent(2) 	<<" ("<< 100*h_Other->GetBinContent(2)/hMCfakes->GetBinContent(2) <<" %)"<<endl;
  cout<<endl<<"Unknown fakes = 		"<< Lum*h_Unkn->GetBinContent(2)  	<<" ("<< 100*h_Unkn->GetBinContent(2)/hMCfakes->GetBinContent(2) <<" %)"<<endl<<endl;

  if (plot) {

    TString pdfTitle4 = string() + outFold + "fakeIndex.pdf";
    THStack *fakeIndex = new THStack("fakeIndex","");
    fakeIndex = stackPlotMConly(Lum,v_fakeLindex,pdfTitle4,"Fake lepton Index "," ");

    TString pdfTitle3 = string() + outFold + "fakeMorig.pdf";
    THStack *fakeMorig = new THStack("fakeMorig","");
    fakeMorig = stackPlotMConly(Lum,v_fakeMorig,pdfTitle3,"Fake mu truth origin "," ");

    TString pdfTitle2 = string() + outFold + "fakeEorig.pdf";
    THStack *fakeEorig = new THStack("fakeEorig","");
    fakeEorig = stackPlotMConly(Lum,v_fakeEorig,pdfTitle2,"Fake e truth origin "," ");

    TString pdfTitle = string() + outFold + "numFakeL.pdf";
    //TH1F *numFakeL = new TH1F("numFakeL","",v_numFakeL.at(0)->GetNbinsX(),v_numFakeL.at(0)->GetBin(1),v_numFakeL.at(0)->GetBin(v_numFakeL.at(0)->GetNbinsX()));
    THStack *numFakeL = new THStack("numFakeL","");
    numFakeL = stackPlotMConly(Lum,v_numFakeL,pdfTitle,"Number of fake l / ev","");

    TString pdfTitle1 = string() + outFold + "fakeLpT.pdf";
    THStack *fakeLpT = new THStack("fakeLpT","");
    fakeLpT = stackPlotMConly(Lum,v_fakeLpT,pdfTitle1,"Fake l pT [GeV] "," GeV");

  }

}//int main()
