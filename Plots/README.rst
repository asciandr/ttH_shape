Setup environment
------
Setup ATLAS and root::

  . setup.sh

Run plotting tool, e.g.::

  nohup python run.py file_list/tth_v29_gfw2_skim.txt tth_variables.txt tth_regions.txt tth_weight.txt >& log &
  tail -f log
