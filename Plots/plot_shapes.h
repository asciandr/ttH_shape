///////////////////////// -*- C++ -*- /////////////////////////////
// Author: A.Sciandra<andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TROOT.h"
#include "TTree.h"
#include "TString.h"
#include "THStack.h"
#include "TLegend.h"
#include <string.h>
#include <TLorentzVector.h>
#include <iostream>
#include <cstddef>        
#include "TLine.h"
#include "TH1F.h"
#include "TArrow.h"
#include "TLatex.h"

#include "atlasstyle-00-04-02/AtlasStyle.C"

using namespace std;

/* Obtain name of the samples from histos' names */
static string getSampleName(string name) {
  
  size_t pos1 = name.find_last_of("/");
  string name1=name.substr(pos1+1);
  size_t pos  		= name1.find(".root");
  string name_final	= name1.substr(0,pos);

  return name_final;
}

/* Compare MC shapes */
//WARNING: assumes the first sample to be the reference (SIGNAL) for shape comparison 
static TH1F *compShapes(vector<string> sampleNames, string varName, vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit) {
  //Is data your last input sample?
  int N_data = histos.size()-1; string nameD = histos.at(N_data)->GetName();
  bool isData = (nameD.find("data") != string::npos) ? true : false;
  unsigned int n = histos.size()+1;
  TH1F *h = new TH1F("h","",histos[0]->GetNbinsX(),histos[0]->GetXaxis()->GetBinLowEdge(1),histos[0]->GetXaxis()->GetBinLowEdge(histos[0]->GetNbinsX())+histos[0]->GetBinWidth(1));
  // Clone input histos
  vector<TH1F*> new_histos;  
  // Set of colours
  vector<int>   colours;
  new_histos    .reserve( n );
  colours       .reserve( n );
  // Set colours for different MC samples
  colours.push_back(2);
  colours.push_back(4);
  colours.push_back(7);
  colours.push_back(5);
  colours.push_back(3);
  colours.push_back(28);
  colours.push_back(40);
  colours.push_back(9);
  colours.push_back(46);
  colours.push_back(12);
  colours.push_back(6);
  colours.push_back(8);
  colours.push_back(29);
  colours.push_back(30);
  if(n>colours.size()) { for(unsigned int i=0; i<n; i++) { colours.push_back(14+5*i); } }  //avoid out of range

  unsigned int N_MC(0);
  N_MC = isData ? histos.size()-1 : histos.size();
  TCanvas *cs = new TCanvas(outFile_name);
  //TLegend *leg = new TLegend(0.13,0.45,0.28,0.85);
  TLegend *leg = new TLegend(0.73,0.42,0.93,0.885);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.06);
  //leg->SetTextSize(0.035);
  leg->SetFillStyle(0);
  for(unsigned int i=0; i<N_MC; i++) {
    string name  = histos.at(i)->GetName();
    TH1F *hnew = (TH1F*)histos.at(i)->Clone(name.c_str());
    hnew->SetFillColor(colours.at(i));
    new_histos.push_back( hnew );
    if(new_histos.at(i)->Integral()!=0) new_histos.at(i)->Scale(1./new_histos.at(i)->Integral());
    new_histos.at(i)->SetFillColor(colours.at(i));
    //new_histos.at(i)->SetFillStyle(3354);
    new_histos.at(i)->SetLineColor(colours.at(i));
    new_histos.at(i)->SetMarkerColor(colours.at(i));
    new_histos.at(i)->SetMarkerStyle(20);
    new_histos.at(i)->SetMarkerSize(0.85);
    //new_histos.at(i)->SetMarkerSize(0.5);
    //TLegendEntry *entry = leg->AddEntry(new_histos.at(i),(sampleNames[i]).c_str(),"f");
    TLegendEntry *entry = leg->AddEntry(new_histos.at(i),(sampleNames[i]).c_str(),"pe");
  }
  //Set labels
  new_histos.at(0)->GetXaxis()->SetTitle(Form(varName.c_str()));
  TString Ytitle = string()+"(1 / N) Entries / %4.1f "+entriesUnit;
  //TString Ytitle = string()+"(1 / N) Entries / %2.0f "+entriesUnit;
  new_histos.at(0)->GetYaxis()->SetTitle(Form(Ytitle,new_histos.at(0)->GetBinWidth(1)));
  new_histos.at(0)->GetYaxis()->SetTitleOffset(1.3);
  // Set Range //
  //double max = new_histos.at(0)->GetMaximum()+2*new_histos.at(0)->GetBinError(new_histos.at(0)->GetMaximumBin());
  //for(unsigned int i=1; i<N_MC; i++) { max = ((new_histos.at(i)->GetMaximum()+new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin())>max) && (new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin()) == new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin())) ) ? new_histos.at(i)->GetMaximum()+1.2*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin()) : max; }
  double max = 1.3*new_histos.at(0)->GetMaximum();
  for(unsigned int i=1; i<N_MC; i++) { max = (new_histos.at(i)->GetMaximum()>max/1.3) ? 1.3*new_histos.at(i)->GetMaximum() : max; }
  new_histos.at(0)->GetYaxis()->SetRangeUser(0,max);
  new_histos.at(0)->GetXaxis()->SetTitle("");
  new_histos.at(0)->GetXaxis()->SetTitleSize(0.1);
  //new_histos.at(0)->GetXaxis()->SetTitleSize(0.05);
  new_histos.at(0)->GetYaxis()->SetTitleSize(0.05);
  new_histos.at(0)->GetXaxis()->SetTitleOffset(0.6);
  new_histos.at(0)->GetYaxis()->SetTitleOffset(0.85);
  gStyle->SetOptStat(0);

  TPad *pad1 = new TPad("pad1","pad1",0.,0.33,1.,1.);
  pad1->SetTopMargin(1.2);
  pad1->SetBottomMargin(0.05);
  pad1->Draw();
  pad1->cd();

  //new_histos.at(0)->Draw("HIST");
  new_histos.at(0)->Draw("PE");
  for(unsigned int i=1; i<N_MC; i++) new_histos.at(i)->Draw("PEsame");
  leg->Draw("same"); 

  pad1->Update();
  pad1->RedrawAxis();
  TLine l;
  l.DrawLine(pad1->GetUxmin(), pad1->GetUymax(), pad1->GetUxmax(), pad1->GetUymax());
  l.DrawLine(pad1->GetUxmax(), pad1->GetUymin(), pad1->GetUxmax(), pad1->GetUymax());

  cs->cd();
  TPad *pad2 = new TPad("pad2","pad2",0.,0.,1.,0.3);
  pad2->SetTopMargin(0.05);
  pad2->SetBottomMargin(0.3);
  pad2->Draw();
  pad2->cd();

  //double y_ratio_max(6.), y_ratio_min(0.);
  double y_ratio_max(2.), y_ratio_min(0.);
  for(unsigned int i=1; i<N_MC; i++) {
    TH1F *hSigClone	= (TH1F*)new_histos.at(0)->Clone( "hSigClone"	);
    TH1F *hclone 	= (TH1F*)new_histos.at(i)->Clone( "hclone"	);
    //B / S
    /*hclone->Divide(new_histos.at(0));
    hclone->SetMarkerStyle(20);
    hclone->SetMarkerSize(0.5);*/
    //S / B
    hSigClone->Divide(hclone);
    hSigClone->SetMarkerStyle(20);
    hSigClone->SetMarkerSize(0.5);
    hSigClone->SetFillColor(colours.at(i));
    hSigClone->SetLineColor(colours.at(i));
    hSigClone->SetMarkerColor(colours.at(i));
    if(i==1) 	{
      //B / S
      /*hclone->GetYaxis()->SetRangeUser(y_ratio_min,y_ratio_max);
      hclone->GetXaxis()->SetLabelSize(0.08);
      hclone->GetYaxis()->SetLabelSize(0.085);
      hclone->GetYaxis()->SetTitle("bkg / sig");
      hclone->GetXaxis()->SetTitleSize(0.12);
      hclone->GetXaxis()->SetTitleOffset(0.8);
      hclone->GetYaxis()->SetTitleSize(0.12);
      hclone->GetYaxis()->SetTitleOffset(0.22);
      hclone->GetXaxis()->SetTitle(Form(varName.c_str()));
      hclone->Draw("PE");*/
      //S / B
      hSigClone->GetYaxis()->SetRangeUser(y_ratio_min,y_ratio_max);
      hSigClone->GetXaxis()->SetLabelSize(0.08);
      hSigClone->GetYaxis()->SetLabelSize(0.085);
      hSigClone->GetYaxis()->SetTitle("sig / bkg");
      hSigClone->GetXaxis()->SetTitleSize(0.12);
      hSigClone->GetXaxis()->SetTitleOffset(0.8);
      hSigClone->GetYaxis()->SetTitleSize(0.12);
      hSigClone->GetYaxis()->SetTitleOffset(0.22);
      hSigClone->GetXaxis()->SetTitle(Form(varName.c_str()));
      hSigClone->Draw("PE");
    } 
    //B / S
    //else	{ hclone->Draw("PEsame"); }
    //S / B
    else	{ hSigClone->Draw("PEsame"); }
  }

  //TLine *line = new TLine(0.1,0.122,0.9,0.122);
  TLine *line = new TLine(0.1,0.188,0.9,0.188);
  line->SetLineStyle(8);
  cs->cd(0);
  cs->cd(2);
  line->Draw("same");

  for(unsigned int i=1; i<N_MC; i++) {
    for(int i_bin=0;i_bin<new_histos.at(i)->GetNbinsX()+2;i_bin++){
      if (i_bin==0 || i_bin>new_histos.at(i)->GetNbinsX()) continue;
      float val=new_histos.at(i)->GetBinContent(i_bin);
      double maxRange = y_ratio_max;
      double minRange = y_ratio_min;

      int isUp=0; //1==up, 0==nothing, -1==down
      if ( val<minRange )       isUp=-1;
      else if (val>maxRange )   isUp=1;
      if (val==0) isUp=0;

      if (isUp!=0) {
        TArrow *arrow;
        if (isUp==1) arrow = new TArrow(new_histos.at(i)->GetXaxis()->GetBinCenter(i_bin),y_ratio_max-0.055*(y_ratio_max-y_ratio_min), new_histos.at(i)->GetXaxis()->GetBinCenter(i_bin),y_ratio_max-0.05,0.030,"|>");
        else         arrow = new TArrow(new_histos.at(i)->GetXaxis()->GetBinCenter(i_bin),y_ratio_min+0.055*(y_ratio_max-y_ratio_min), new_histos.at(i)->GetXaxis()->GetBinCenter(i_bin),y_ratio_min+0.05,0.030,"|>");
        arrow->SetFillColor(kBlue-7);
        arrow->SetFillStyle(3144);
        arrow->SetLineColor(kBlue-7);
        arrow->SetLineWidth(2);
        arrow->SetAngle(40);
        arrow->Draw("");
      }//if (isUp!=0)
    }//for(int i_bin=0;i_bin<new_histos.at(i)->GetNbinsX()+2;i_bin++)
  }//for(unsigned int i=1; i<N_MC; i++)

  pad2->Update();
  pad2->RedrawAxis();
  TLine l2;
  l2.DrawLine(pad2->GetUxmin(), pad2->GetUymax(), pad2->GetUxmax(), pad2->GetUymax());
  l2.DrawLine(pad2->GetUxmax(), pad2->GetUymin(), pad2->GetUxmax(), pad2->GetUymax());

  cs->cd();
  cs->SaveAs(outFile_name,"RECREATE");

  return h;
}//static TH1F *compShapes()
/* Compare MC shapes */

/* Produce Stack Plots */
static THStack *stackPlot(vector<string> sampleNames, string varName, vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit, Float_t BlindThreshold){ //Give a vector of histos and realise a stack plot from it

  //Is data your last input sample?
  int N_data = histos.size()-1; string nameD = histos.at(N_data)->GetName();
  bool isData = (nameD.find("data") != string::npos) ? true : false;
  //SetAtlasStyle();
  unsigned int n = histos.size()+1; 
  THStack *hs = new THStack("hs","");
  // Define sum of MC histos to plot error bands 
  TH1F *herr = new TH1F("herr","",histos.at(0)->GetNbinsX(),histos.at(0)->GetXaxis()->GetBinLowEdge(1),histos.at(0)->GetXaxis()->GetBinLowEdge(histos.at(0)->GetNbinsX())+histos.at(0)->GetBinWidth(1)); 
  // Clone input histos
  vector<TH1F*> new_histos; 
  // Set of colours
  vector<int>   colours;    
  new_histos    .reserve( n );
  colours       .reserve( n );
  // Set colours for different MC samples
  colours.push_back(2);
  colours.push_back(4);
  colours.push_back(7);
  colours.push_back(5);
  colours.push_back(3);
  colours.push_back(28);
  colours.push_back(40);
  colours.push_back(9);
  colours.push_back(46);
  colours.push_back(12);
  colours.push_back(6);
  colours.push_back(8);
  colours.push_back(29);
  colours.push_back(30);
  if(n>colours.size()) { for(unsigned int i=0; i<n; i++) { colours.push_back(14+5*i); } }
  
  unsigned int N_MC(0);
  N_MC = isData ? histos.size()-1 : histos.size();

  // Define sig and bgd histos to check bin by bin S/B
  TH1F *h_bgd, *h_SoverB;
  for(unsigned int i=0; i<N_MC; i++) {
    string name  = histos.at(i)->GetName();
    TH1F *hnew = (TH1F*)histos.at(i)->Clone(name.c_str());
    hnew->SetFillColor(colours.at(i));
    new_histos.push_back( hnew );
    // Assuming the 1st histogram is the signal...
    if (i==0) 		h_SoverB = (TH1F*)hnew->Clone(name.c_str());
    else if (i==1) 	h_bgd = (TH1F*)hnew->Clone(name.c_str());
    else if (i>1) 	h_bgd->Add(hnew);
  }
  h_SoverB->Divide(h_bgd);
 
  TCanvas *cs = new TCanvas(outFile_name);
  TLegend *leg = new TLegend(0.73,0.23,0.93,0.885);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.035);
  leg->SetFillStyle(0);
  for(unsigned int i=0; i<N_MC;i++) {
    new_histos.at(i)->SetFillColor(colours.at(i));
    new_histos.at(i)->SetLineColor(colours.at(i));
    new_histos.at(i)->SetMarkerColor(colours.at(i));
    //string name  = histos.at(i)->GetName();
    TLegendEntry *entry = leg->AddEntry(new_histos.at(i),(sampleNames[i]).c_str(),"f"); 
    hs->Add(new_histos.at(i));
    herr->Add(new_histos.at(i));
  }
  // -----DATA----- //
  //int N_data = histos.size()-1;
  //string nameD  = histos.at(N_data)->GetName();
  TH1F *hData   = (TH1F*)histos.at(N_data)->Clone(nameD.c_str());
  hData->SetMarkerColor(kBlack);
  hData->SetMarkerSize(1.);
  hData->SetMarkerStyle(21);
  hData->SetLineColor(kBlack);
  hData->GetXaxis()->SetTitle(Form(varName.c_str()));
  TString Ytitle = string()+"Entries / %2.0f "+entriesUnit;
  hData->GetYaxis()->SetTitle(Form(Ytitle,hData->GetBinWidth(1)));
  hData->GetYaxis()->SetTitleOffset(1.3);
  new_histos.push_back( hData );
  if(isData) TLegendEntry *entry = leg->AddEntry(hData,nameD.c_str(),"pe");

  // Check which bins are above the BlindThreshold
  // and plot shaded bin convering the whole bin if blinded
  TH1F *h_shadedBand=(TH1F*)histos.at(N_data)->Clone(nameD.c_str());;
  for (int i=0; i<h_SoverB->GetNbinsX(); i++) {
    if( h_SoverB->GetBinContent(i+1) > BlindThreshold ) {
      hData->SetBinContent(i+1,-99);
      h_shadedBand->SetBinContent(i+1,10000000);
    }
    else h_shadedBand->SetBinContent(i+1,-99);
  }
  h_shadedBand->SetMarkerSize(0);
  h_shadedBand->SetMarkerColor(0);
  h_shadedBand->SetLineColor(0);
  h_shadedBand->SetLineColorAlpha(kWhite,0);
  h_shadedBand->SetFillColor(kBlue);
  h_shadedBand->SetFillStyle(3353);

  // Set Range //
  double max(1000);
  max = (hs->GetMaximum()>hData->GetMaximum()+hData->GetBinError(hData->GetMaximumBin())) ? hs->GetMaximum()+1*hs->GetMaximum() : hData->GetMaximum()+2*hData->GetBinError(hData->GetMaximumBin());
  
  // Plot Stack MC, Data and Data/MC ratio // 
  //Clone Data for ratio plot
  TH1F *hData1 = (TH1F*)hData->Clone("hData1");

  TPad *pad1 = new TPad("pad1","pad1",0.,0.33,1.,1.);
  pad1->SetTopMargin(1.2);
  pad1->SetBottomMargin(0.05);
  pad1->Draw();
  pad1->cd();
  hData->GetYaxis()->SetRangeUser(0,max);
  hData->GetXaxis()->SetTitle("");
  hData->GetXaxis()->SetTitleSize(0.05);
  hData->GetYaxis()->SetTitleSize(0.05);
  hData->GetXaxis()->SetTitleOffset(0.6);
  hData->GetYaxis()->SetTitleOffset(0.7);
  gStyle->SetOptStat(0);
  if(isData) 	{ hData->Draw("pe"); hs->Draw("sameHIST"); 	}
  else 		hs->Draw("HIST");
  h_shadedBand->Draw("sameHIST");
  leg->Draw("same");

  //Draw error bands for MC stack plot
  herr->SetMarkerSize(0);
  herr->SetMarkerColor(colours.at(N_MC));
  herr->SetLineColor(0);
  herr->SetFillColor(kMagenta-4);
  herr->SetFillStyle(3353);
  herr->Draw("sameE2");
  TLegendEntry *errentry = leg->AddEntry(herr,"MC stat. unc.","f");
  
  if(isData) hData->Draw("PEsame");
  
  // Ratio plot //
  //Need a TH1 as a sum of all MC histos
  TH1F *hsum = (TH1F*)new_histos.at(0)->Clone("hsum");
  for(unsigned int i=1; i<N_MC;i++) {
    hsum->Add(new_histos.at(i));
  }

  //Redraw axes and borders
  pad1->Update();
  pad1->RedrawAxis();
  TLine l;
  l.DrawLine(pad1->GetUxmin(), pad1->GetUymax(), pad1->GetUxmax(), pad1->GetUymax());
  l.DrawLine(pad1->GetUxmax(), pad1->GetUymin(), pad1->GetUxmax(), pad1->GetUymax());

  cs->cd();
  TPad *pad2 = new TPad("pad2","pad2",0.,0.,1.,0.3);
  pad2->SetTopMargin(0.05);
  pad2->SetBottomMargin(0.3);
  pad2->Draw();
  pad2->cd();
  hData1->Divide(hsum);
  //hData1->GetYaxis()->SetRangeUser(0.5,1.5);
  hData1->GetYaxis()->SetRangeUser(0.,2.);
  hData1->SetMarkerStyle(20);
  hData1->SetMarkerSize(0.5);
  hData1->SetMarkerColor(kBlack);
  hData1->SetLineColor(kBlack);
  hData1->GetXaxis()->SetLabelSize(0.08);
  hData1->GetYaxis()->SetLabelSize(0.085);
  hData1->GetYaxis()->SetTitle("data / MC");
  hData1->GetXaxis()->SetTitleSize(0.12);
  hData1->GetXaxis()->SetTitleOffset(0.8);
  hData1->GetYaxis()->SetTitleSize(0.12);
  hData1->GetYaxis()->SetTitleOffset(0.22);
  for(int i=0; i<hData1->GetNbinsX(); i++) {
    if(hData->GetBinContent(i+1)!=0 && herr->GetBinContent(i+1)!=0) hData1->SetBinError(i+1,sqrt(hData->GetBinContent(i+1))/herr->GetBinContent(i+1)); 
    else hData1->SetBinError(i+1,0);
  }
  // Stat uncertainty in MC
  TH1F *herr1 = (TH1F*)herr->Clone("herr1");
  herr1->Divide(herr);
  if(isData) {
    hData1->Draw("epx");
    herr1->Draw("sameE2");
    hData1->Draw("SAMEepx");
  } 
  else {
    for(int i_bin=0;i_bin<hData1->GetNbinsX()+2;i_bin++) { hData1->SetBinContent(i_bin,1); hData1->SetBinError(i_bin,0); }
    hData1->Draw("epx");
    herr1->Draw("sameE2");
  }

  // Add arrows when the ratio is beyond the limits of the ratio plot
  double y_ratio_max(2.), y_ratio_min(0.);
  if(isData) {
    for(int i_bin=0;i_bin<hData1->GetNbinsX()+2;i_bin++){
      if (i_bin==0 || i_bin>hData1->GetNbinsX()) continue; //skip under/overflow bins
      float val=hData1->GetBinContent(i_bin);
      double maxRange = y_ratio_max;
      double minRange = y_ratio_min;

      int isUp=0; //1==up, 0==nothing, -1==down
      if ( val<minRange ) 	isUp=-1;
      else if (val>maxRange ) 	isUp=1;
      if (val==0) isUp=0;
      
      // blinding threshold
      if (hData->GetBinContent(i_bin)==-99) {	
        isUp=0;
      }
      else if (isUp!=0) {
        TArrow *arrow;
        if (isUp==1)	arrow = new TArrow(hData1->GetXaxis()->GetBinCenter(i_bin),y_ratio_max-0.055*(y_ratio_max-y_ratio_min), hData1->GetXaxis()->GetBinCenter(i_bin),y_ratio_max-0.05,0.030,"|>");
        else      	arrow = new TArrow(hData1->GetXaxis()->GetBinCenter(i_bin),y_ratio_min+0.055*(y_ratio_max-y_ratio_min), hData1->GetXaxis()->GetBinCenter(i_bin),y_ratio_min+0.05,0.030,"|>");
        arrow->SetFillColor(kBlue-7);
        arrow->SetFillStyle(3144);
        arrow->SetLineColor(kBlue-7);
        arrow->SetLineWidth(2);
        arrow->SetAngle(40); 
        arrow->Draw("");
      }
    }//for(int i_bin=0;i_bin<hData1->GetNbinsX()+2;i_bin++)
  }//if(isData)
 
  h_shadedBand->Draw("sameHIST");
  
  TLine *line = new TLine(0.1,0.1875,0.9,0.1875);
  line->SetLineStyle(8);
  cs->cd(0);
  cs->cd(2);
  line->Draw("same");

  Double_t chi2(-99.);
  Int_t ndf(-1.), igood(-1.);
  hData->Chi2TestX(hsum, chi2, ndf, igood, "UW");
  cout << "chi2 / NDOF :        " << chi2 <<" / " << ndf << endl;
  TLatex latex(0.1,0.1,"latex");
  latex.SetTextSize(0.027);
  latex.DrawLatex(0.13,0.31,Form("#chi^{2} / ndf = %.2f / %d = %.2f", chi2, ndf, chi2/ndf));
  
  cs->cd();
  cs->SaveAs(outFile_name,"RECREATE");

  return hs;
}//static THStack *stackPlot()

/* Produce Data-only Plots */
static TH1F *onlyDataPlot(vector<string> sampleNames, string varName, vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit){ //

  //Is data your last input sample?
  int N_data = histos.size()-1; string nameD = histos.at(N_data)->GetName();
  bool isData = (nameD.find("data") != string::npos) ? true : false;
  if (!isData) return 0;
  //SetAtlasStyle();
  TH1F *hs = new TH1F("hs","",histos[0]->GetNbinsX(),histos[0]->GetXaxis()->GetBinLowEdge(1),histos[0]->GetXaxis()->GetBinLowEdge(histos[0]->GetNbinsX())+histos[0]->GetBinWidth(1));
  TH1F *hData   = (TH1F*)histos.at(N_data)->Clone(nameD.c_str());
  hData->SetMarkerColor(kBlack);
  hData->SetMarkerSize(1.);
  hData->SetMarkerStyle(21);
  hData->SetLineColor(kBlack);
  hData->GetXaxis()->SetTitle(Form(varName.c_str()));
  TString Ytitle = string()+"Entries / %2.0f "+entriesUnit;
  hData->GetYaxis()->SetTitle(Form(Ytitle,hData->GetBinWidth(1)));
  hData->GetYaxis()->SetTitleOffset(1.3);

  TCanvas *cs = new TCanvas(outFile_name);
  TLegend *leg = new TLegend(0.63,0.65,0.83,0.885);
  leg->SetBorderSize(0);
  leg->SetTextSize(0.035);
  leg->SetFillStyle(0);
  TLegendEntry *entry = leg->AddEntry(hData,nameD.c_str(),"pe");

  hData->Draw("pe");
  leg->Draw("same");

  cs->SaveAs(outFile_name,"RECREATE");

  return hs;
}//

