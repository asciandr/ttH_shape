#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys = str(os.environ['ROOTSYS'])

listRootFiles = ""
variables = ""
selection = ""
weight = ""

try:
    listRootFiles = str(sys.argv[1])
    variables = str(sys.argv[2])
    selection = str(sys.argv[3])
    weight = str(sys.argv[4])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python run.py listRootFiles variables region(s) weight"
    sys.exit(1)
    pass

if not os.path.exists("./"+listRootFiles): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+variables):
    print "Input "+str(sys.argv[2])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+selection):
    print "Input "+str(sys.argv[3])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+weight):
    print "Input "+str(sys.argv[4])+" not found, aborting..."
    sys.exit(1)
    pass

print "Compiling macros..."
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L giveMePlots.C+")
if cmpFailure:
    print "Compilation failure, aborting..."
    sys.exit(1)
    pass

#Set integrated luminosity
#N.B. if not already included in the event weight!
Lum=1
###Lum=36074.6
#2016+2017 13 TeV R21
###Lum=65771.4

#Set blinding threshold: blind bins where the S/B is above this % in MC 
BlindThreshold=1.
#BlindThreshold=0.
#BlindThreshold=0.15

giveMePlots( listRootFiles, variables, selection, weight, Lum, BlindThreshold )

