INTRODUCTION
---------
Multi-purpose project gathering packages for several needs:

* BDT
* FIT
* Yields
* Plots
* Skim
* Fakes
* Template reco

Every (or almost) package has its own README file.
