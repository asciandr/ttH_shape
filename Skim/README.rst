Setup environment & Run skimming
------
Setup ATLAS and root::

  . setup.sh

Run skimming tool locally, e.g.::

  nohup python run.py file_list/test.txt selection.txt outFolder.txt >& log &
  tail -f log

or launch jobs on batch (1/ntuple to be skimmed)::

  ./submit_batch.sh test.txt 
