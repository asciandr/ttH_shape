#!/bin/bash

pwd
workdir=`pwd`
echo XSAMPTXT
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupROOT --skipConfirm
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/Skim/{skimNtuples.C,selection.txt,outFolder.txt,nominal.h,nominal.C,run.py} .
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/Skim/{header.h,XsecFile.txt} .
ls -haltr
### IF ON EOSPUBLIC
#source /afs/cern.ch/project/eos/installation/atlas/etc/setup.sh  # setup eos
#export EOS_MGM_URL=root://eospublic.cern.ch/
#eosmount ./eospublic
#ls -haltr eospublic/escience/UniTexas/HSG8/multileptons_ntuple_run2/25ns_v29/01/gfw2/2017-06-22/Data/XFILE
#python run.py ./eospublic/escience/UniTexas/HSG8/multileptons_ntuple_run2/25ns_v29/01/gfw2/2017-06-22/Data/XFILE selection.txt outFolder.txt
#ls -haltr eospublic/escience/UniTexas/HSG8/multileptons_ntuple_run2/25ns_v28/01/gfw2/Sys_1l3l4l_MVA/XFILE
#python run.py ./eospublic/escience/UniTexas/HSG8/multileptons_ntuple_run2/25ns_v28/01/gfw2/Sys_1l3l4l_MVA/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/ttHmultilepton_samples/v27/01/XFILE
#python run.py /eos/atlas/user/a/asciandr/ttHmultilepton_samples/v27/01/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/r/rwolff/public/25ns_v29/01/gfw2/2017-06-22/Sys_PLI/XFILE
#python run.py /eos/atlas/user/r/rwolff/public/25ns_v29/01/gfw2/2017-06-22/Sys_PLI/XFILE selection.txt outFolder.txt
###--- 2018 analysis ---###
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5/mc16a/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5/mc16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5/mc16c/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5/mc16c/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5/data/2015/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5/data/2015/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5/data/2016/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5/data/2016/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5/data/2017/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5/data/2017/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/specialFullGN2/mc16a/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/specialFullGN2/mc16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/specialFullGN2/mc16c/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/specialFullGN2/mc16c/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/specialFullGN2/Data15/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/specialFullGN2/Data15/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/specialFullGN2/Data16/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/specialFullGN2/Data16/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/specialFullGN2/Data17/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/specialFullGN2/Data17/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5_newTightDefs/mc16a/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5_newTightDefs/mc16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5_newTightDefs/mc16c/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5_newTightDefs/mc16c/XFILE selection.txt outFolder.txt
### v5 Peter's skim
#ls -haltr /eos/atlas/user/p/ponyisi/peterfw2_v5/mc16a/XFILE
#python run.py /eos/atlas/user/p/ponyisi/peterfw2_v5/mc16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/p/ponyisi/peterfw2_v5/mc16c/XFILE
#python run.py /eos/atlas/user/p/ponyisi/peterfw2_v5/mc16c/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/p/ponyisi/peterfw2_v5/data_16/XFILE
#python run.py /eos/atlas/user/p/ponyisi/peterfw2_v5/data_16/XFILE selection.txt outFolder.txt
### my skim of skim of v5 Peter's skim
#ls -haltr /eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN1/Peters_skim_v5/MC16a/XFILE
#python run.py /eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN1/Peters_skim_v5/MC16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN1/Peters_skim_v5/MC16c/XFILE
#python run.py /eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN1/Peters_skim_v5/MC16c/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN1/v5/hadded/XFILE
#python run.py /eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN1/v5/hadded/XFILE selection.txt outFolder.txt
### skim for partial unblinding
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5_newTightDefs_21052018/mc16a/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5_newTightDefs_21052018/mc16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5_newTightDefs_21052018/mc16c/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5_newTightDefs_21052018/mc16c/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5_newTightDefs_21052018/data/2015/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v5_newTightDefs_21052018/data/2015/XFILE selection.txt outFolder.txt
### GN2 v6_02
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v6_02/nominal/mc16a/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v6_02/nominal/mc16a/XFILE selection.txt outFolder.txt
#ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v6_02/nominal/mc16d/XFILE
#python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v6_02/nominal/mc16d/XFILE selection.txt outFolder.txt
ls -haltr /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v6_02/data/XFILE
python run.py /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/multilepton_Run2_Summer18/GN2/v6_02/data/XFILE selection.txt outFolder.txt

echo 'done.'
