#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys = str(os.environ['ROOTSYS'])

inputRootFiles = ""
selection = ""
outFolder = ""

try:
    inputRootFiles = str(sys.argv[1])
    selection = str(sys.argv[2])
    outFolder = str(sys.argv[3])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python run.py inputRootFiles selection outFolder"
    sys.exit(1)
    pass

if not os.path.exists("./"+inputRootFiles) and not os.path.exists(inputRootFiles): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+selection):
    print "Input "+str(sys.argv[2])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+outFolder):
    print "Input "+str(sys.argv[3])+" not found, aborting..."
    sys.exit(1)
    pass

print "Compiling macros..."
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L skimNtuples.C+")
if cmpFailure:
    print "Compilation failure, aborting..."
    sys.exit(1)
    pass

skimNtuples( inputRootFiles, selection, outFolder)

