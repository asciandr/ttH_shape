export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

setupATLAS --quiet

rootVersion=6.04.02-x86_64-slc6-gcc48-opt
localSetupROOT $rootVersion --quiet
echo "==> Setting up ROOT version: "$rootVersion
