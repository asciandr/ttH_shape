#!/bin/bash

listtxt=$*
while read samp
do
    echo $samp
    samptxt=`echo $samp| sed "s/.root/.txt/g" `
    echo $samptxt
    sampname=`echo $samp| sed "s/.root//g" `
    echo $sampname
    echo $samp > $samptxt
    cat job.sh | sed "s/XSAMPTXT/$samptxt/g" | sed "s/XFILE/$samp/g" > job_$sampname.sh
    chmod 755 job_$sampname.sh
    echo job_$sampname.sh
#    queue="1nh"
    queue="8nh"
    bsub -q $queue job_$sampname.sh
    echo $queue
done < $*
