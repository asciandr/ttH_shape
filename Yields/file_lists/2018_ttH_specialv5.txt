$t\bar{t}H$			;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/ttH.root
$t\bar{t}Z$			;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/ttZ.root
$t\bar{t}$ dilep		;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/ttbar_2lep_PP8.root
$ZZ$				;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/ZZ.root
### $t\bar{t}$ nonallhad	;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/ttbar_nonallhad.root
$t\bar{t}\gamma$		;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/ttgamma.root
$t\bar{t}t\bar{t}$		;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/tttt.root
$t\bar{t}W$			;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/ttW.root
$t\bar{t}WW$			;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/ttWW.root
$WZ\to qq\ell\ell$		;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/WqqZll.root
$WW$				;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/WW.root
$WZ\to3\ell$			;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/WZ.root
data				;/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN2/special_v5/data.root
