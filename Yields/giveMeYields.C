#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <string>
#include <cstring>
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TChain.h"
#include "TTree.h"
#include "TString.h"
#include "TSystem.h"
#include "TROOT.h"
#include "TLorentzVector.h"
#include "TStopwatch.h"
#include "TStyle.h"
#include "TGraph.h"

#include <cstdlib>
#include <iostream>
#include <map>
#include <string>

#include "TChain.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"

using namespace std;

void giveMeYields( TString inputRootNames, TString selection, TString weight, Float_t lum )
{
  //Read inputRootNames and store name of input root files in a vector
  ifstream ifs(inputRootNames.Data());
  string aLine;
  vector<TString> v_fileNames, v_sampleNames;
  if (ifs.fail()) {
    cerr<<"Failure when reading a file: "<< inputRootNames <<endl;
    return;
  }
  else {
    while (getline(ifs,aLine)) {
      TString ts_aLine=aLine.data();
      if (ts_aLine.BeginsWith("#")) continue;
      size_t pos0 = aLine.find_first_of(";");
      string fileName = aLine.substr(pos0+1,aLine.size());
      v_fileNames.push_back(fileName);
      size_t pos1 = aLine.find_first_of('\t');
      string sampleName = aLine.substr(0,pos1);
      v_sampleNames.push_back(sampleName);
    }
  }
  //Read selection txt file and save selection string
  TString mySelection;
  ifstream ifs2(selection.Data());
  string aLine2;
  if (ifs2.fail()) {
    cerr<<"Failure when reading a file: "<< selection <<endl;
    return;
  }
  else {
    int count(0.);
    while (getline(ifs2,aLine2)) {
      TString ts_aLine=aLine2.data(); 
      if (ts_aLine.BeginsWith("#")) continue;
      count ++;
      mySelection = aLine2.substr(0,aLine2.size());
    }
    if(count>1) cout<<"#WARNING# You set more than one selection string, I'll take the LAST ONE"<<endl;
  }
  cout<<endl; cout<<"Selection: "<< mySelection <<endl; cout<<endl; 

  //Read weight txt file and save selection string
  TString myWeight;
  ifstream ifs3(weight.Data());
  string aLine3;
  if (ifs3.fail()) {
    cerr<<"Failure when reading a file: "<< weight <<endl;
    return;
  }
  else {
    int count(0.);
    while (getline(ifs3,aLine3)) {
      TString ts_aLine=aLine3.data();
      if (ts_aLine.BeginsWith("#")) continue;
      count ++;
      myWeight = aLine3.substr(0,aLine3.size());
    }
    if(count>1) cout<<"#WARNING# You set more than one weight string, I'll take the LAST ONE"<<endl;
  }
  cout<<endl; cout<<"Weight: "<< myWeight <<endl; cout<<endl;
  cout<<endl; cout<<"scaling to set luminosity: "<< lum <<endl; cout<<endl;

  //Write yields to .tex file
  ofstream myfile("yields.tex");
  if (myfile.is_open()) {
    myfile<<"\\documentclass[10pt]{article} \n\\usepackage{siunitx} \n\\usepackage[margin=0.1in,landscape,papersize={210mm,350mm}]{geometry} \n\\begin{document} \n\\begin{table}[htbp] \n\\begin{center} \n\\begin{tabular}{|c|c|c|}\n";
  } else cout << "Unable to open file";

  // Check n. of MC samples (i.e. check whether any dataset is there)
  // N.B. data is assumed to be the last one in the list
  bool theresData(false);
  for(unsigned int i=0; i<v_fileNames.size(); i++) {
    if (v_fileNames[i].Contains("data"))        theresData=true;
  }
  unsigned int N_MC = theresData ? v_fileNames.size()-1 : v_fileNames.size();
  // tot MC expectation
  TH1F *totYieldsHisto          = new TH1F("totYieldsHisto","",7,0.,7.);
  totYieldsHisto->Sumw2();

  //Looping over root files
  for(unsigned int i=0; i<v_fileNames.size(); i++) {
    cout<<"File   name: "<< 	v_fileNames[i] 		<<endl;
    cout<<"Sample name: "<< 	v_sampleNames[i] 	<<endl;
    TFile *myTFile = TFile::Open( v_fileNames[i] );
    TTree *mySkimTree;
    myTFile->GetObject("nominal",mySkimTree);
    TH1F *yieldHisto = new TH1F("yieldHisto","",7,0.,7.);
    yieldHisto->Sumw2();
    if (!v_fileNames[i].Contains("data")) 	{ mySkimTree->Draw("quadlep_type>>yieldHisto", "("+myWeight+")*("+mySelection+")"); yieldHisto->Scale(lum); }
    else 					{ mySkimTree->Draw("quadlep_type>>yieldHisto", "("+mySelection+")"); }
    double err_expectedEvents(0.);
    double expectedEvents = yieldHisto->IntegralAndError(0,yieldHisto->GetXaxis()->GetNbins()+1,err_expectedEvents);
    cout<<"expected exents: "<< expectedEvents <<" +- "<< err_expectedEvents <<endl; 
    myfile <<"\t \\hline "<< v_sampleNames[i] <<" & \\num[round-mode=figures,round-precision=3]{"<< expectedEvents <<"} $\\pm$ \\num[round-mode=figures,round-precision=1]{"<< err_expectedEvents <<"} \\\\ \n";
    if (i<N_MC)                 totYieldsHisto->Add(yieldHisto);
    else {
     double err_tot_expectedEvents(0.);
     double tot_expectedEvents = totYieldsHisto->IntegralAndError(0,totYieldsHisto->GetXaxis()->GetNbins()+1,err_tot_expectedEvents);
     cout<<"tot MC:             "<< tot_expectedEvents <<" +- "<< err_tot_expectedEvents <<endl;
     myfile <<"\t \\hline tot MC  & \\num[round-mode=figures,round-precision=3]{"<< tot_expectedEvents <<"} $\\pm$ \\num[round-mode=figures,round-precision=1]{"<< err_tot_expectedEvents <<"} \\\\ \n";
    }
    myTFile->Close();
  }//for(unsigned int i=0; i<v_fileNames.size(); i++)
  myfile <<"\\hline \n\\end{tabular} \n\\caption{Expected (and observed) yields in this region} \n\\end{center} \n\\end{table} \n\\end{document}";
  myfile.close();
}//void
