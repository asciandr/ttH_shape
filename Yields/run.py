#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime

rootsys = str(os.environ['ROOTSYS'])

listRootFiles = ""
selection = ""
weight = ""

try:
    listRootFiles = str(sys.argv[1])
    selection = str(sys.argv[2])
    weight = str(sys.argv[3])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python run.py listRootFiles selection weight"
    sys.exit(1)
    pass

if not os.path.exists("./"+listRootFiles): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+selection):
    print "Input "+str(sys.argv[2])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+weight):
    print "Input "+str(sys.argv[3])+" not found, aborting..."
    sys.exit(1)
    pass

print "Compiling macros..."
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L giveMeYields.C+")
if cmpFailure:
    print "Compilation failure, aborting..."
    sys.exit(1)
    pass

#Set integrated luminosity
#N.B. if not already included in the event weight!
Lum=1
###Lum=36074.6

giveMeYields( listRootFiles, selection, weight, Lum )

