Setup environment
------
Setup ATLAS and root::

  . setup.sh

Run plotting tool to automatise generation of TRex plots in batch, e.g.::

  nohup python run.py test.txt variables.txt regions.txt weight.txt >& log &
  tail -f log

