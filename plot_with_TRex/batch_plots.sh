#!/bin/bash

PACKAGE_folder=/afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/Plots/

ls -haltr ${PACKAGE_folder}/

cp -rf ${PACKAGE_folder}/atlasstyle-00-04-02/			.
cp -rf ${PACKAGE_folder}/*.C 					.
cp -rf ${PACKAGE_folder}/*.h 					.
cp -rf ${PACKAGE_folder}/*.sh 					.
cp -rf ${PACKAGE_folder}/*.py 					.
cp -rf ${PACKAGE_folder}/file_list/				.
cp -rf ${PACKAGE_folder}/*.txt					.

ls -haltr
. setup.sh
python run.py file_list/fit_inputs_v6.txt 4L_variables.txt regions.txt weight.txt
#python run.py file_list/skimmed_hadded_v6.txt variables.txt regions.txt weight.txt

echo 'Plots are ready.'
