#!/bin/usr/python 

from ROOT import *
gROOT.SetBatch(1)
from array import array
import sys
import os
import os.path
import time
from datetime import datetime
import subprocess

rootsys = str(os.environ['ROOTSYS'])

listRootFiles = ""
variables = ""
selection = ""
weight = ""

try:
    listRootFiles = str(sys.argv[1])
    variables = str(sys.argv[2])
    selection = str(sys.argv[3])
    weight = str(sys.argv[4])
except IndexError:
    print "Argument missing."
    print "\tUsage==> python run.py listRootFiles variables region(s) weight"
    sys.exit(1)
    pass

if not os.path.exists("./"+listRootFiles): 
    print "Input "+str(sys.argv[1])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+variables):
    print "Input "+str(sys.argv[2])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+selection):
    print "Input "+str(sys.argv[3])+" not found, aborting..."
    sys.exit(1)
    pass
if not os.path.exists("./"+weight):
    print "Input "+str(sys.argv[4])+" not found, aborting..."
    sys.exit(1)
    pass

print "Compiling macros..."
cmpFailure = 0
cmpFailure += gROOT.ProcessLine(".L parser.C+")
if cmpFailure:
    print "Compilation failure, aborting..."
    sys.exit(1)
    pass

parser( listRootFiles, variables, selection, weight )

#command = './magic_script.sh my_weight my_regions my_variables my_variableName my_bins my_lower my_upper'
#process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
#output, error = process.communicate()


my_weight = open('my_weight', 'r')
my_regions = open('my_regions', 'r')
my_regionName = open('my_regionName', 'r')
my_regionName_lines=my_regionName.readlines()
my_variables = open('my_variables', 'r')
my_variableName = open('my_variableName', 'r')
my_bins = open('my_bins', 'r')
my_lower = open('my_lower', 'r')
my_upper = open('my_upper', 'r')

for w in my_weight:
    WEIGHT=w.strip()

idx=0
for reg_idx,region in enumerate(my_regions):

    for var, varname, bins, low, up in zip(my_variables, my_variableName, my_bins, my_lower, my_upper):
        REGION=var.strip().split('/')[0]
        #print "I am the split var name:\t"+REGION
        checkWords =("XNTUPLE_PATH","XWEIGHT","XREGION","XVARIABLE","XVAR_TITLE","XVAR_LABEL","XBINS","XLOW","XUP","XSELECTION")
        #repWords   =("/eos/atlas/user/a/asciandr/2018_ttHmultilepton_samples/GN1/3L4L_skim_Peters_v5/",WEIGHT,my_regionName_lines[reg_idx].strip()+"_"+REGION,var.strip(),varname.strip(),varname.strip(),bins.strip(),low.strip(),up.strip(),region.strip())
        repWords   =("/afs/cern.ch/user/a/asciandr/work/WWZ/WWZ_shape/Plots/v6_fit_inputs/",WEIGHT,my_regionName_lines[reg_idx].strip()+"_"+REGION,var.strip(),varname.strip(),varname.strip(),bins.strip(),low.strip(),up.strip(),region.strip())
        
        f1 = open('TEMPLATE.config', 'r')
        f2 = open('whatever_'+str(idx)+'.config', 'w')

        for line in f1:
            for check, rep in zip(checkWords, repWords):
                line = line.replace(check, rep)
            f2.write(line)
        f2.close()
        f1.close()
        idx=idx+1 


for x in range(0, idx):
    command = './split_fits.sh whatever_'+str(x)+'.config'
    process = subprocess.Popen(command.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()


