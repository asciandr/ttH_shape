% Automated plotting
% Andrea Sciandra 
% andrea.sciandra@cern.ch

%--------------- % 
%---  JOB    --- % 
%--------------- % 

Job: XJOB_NAME
  CmeLabel: 13 TeV
  POI: mu_XS_WVZ
  ReadFrom: NTUP
  NtuplePaths: XNTUPLE_PATH
  LumiLabel: 80 fb^{-1}
  Label: VALIDATION PLOT
  Lumi: 1.
  MCweight: XWEIGHT
  PlotOptions: CHI2,NOXERR,NOENDERR
  NtupleName: nominal
  DebugLevel: 9
  HistoChecks: NOCRASH
  SplitHistoFiles: TRUE
  ImageFormat: png,pdf,eps
  Selection: 1
  AtlasLabel: \t

% --------------- % 
% ---  FIT    --- % 
% --------------- % 

Fit: fit
  UseMinos: all
  FitType: BONLY 
  FitRegion: CRSR
  POIAsimov: 1
  FitBlind: FALSE

% --------------- % 
% ---  LIMIT  --- % 
% --------------- % 

Limit: limit
  LimitType: ASYMPTOTIC
  LimitBlind: FALSE
  POIAsimov: 1 

Region: XREGION
  Type: VALIDATION
  DataType: DATA
  Variable: XVARIABLE,XBINS,XLOW,XUP
  VariableTitle: XVAR_TITLE
  %Label: XVAR_LABEL
  YaxisTitle: Events / bin
  Selection: XSELECTION
  LogScale: TRUE
  %Ymax: 1E6
  Ymin: 0.1


% --------------- % 
% --- SAMPLES --- % 
% --------------- % 

%Splitting VH into signal (WH->WWW*,WZZ* and ZH->ZWW*) and background (others)

%Sample: lllvjj
%  Type: BACKGROUND
%  Title: VBS WZ
%  NtupleFiles: lllvjj
%  FillColor: 11
%  LineColor: 1

%Sample: WZ
%  Type: BACKGROUND
%  Title: WZ
%  NtupleFiles: WZ,lllvjj
%  FillColor: 46
%  LineColor: 1

%Sample: lllljj
%  Type: BACKGROUND
%  Title: VBS ZZ
%  NtupleFiles: lllljj
%  FillColor: 4
%  LineColor: 1

Sample: ZZ
  Type: BACKGROUND
  Title: ZZ
  NtupleFiles: ZZ,lllljj
  FillColor: 428
  LineColor: 1

%Sample: Zjets
%  Type: BACKGROUND
%  Title: Z+j
%  %Title: Z+j/\gamma
%  NtupleFiles: Zjets,Zgamma
%  FillColor: 3
%  LineColor: 1

%Sample: Zgamma
%  Type: BACKGROUND
%  Title: Zgamma
%  NtupleFiles: Zgamma
%  FillColor: 876
%  LineColor: 1

Sample: others
  Type: BACKGROUND
  Title: others
  NtupleFiles: others,Zjets,ttbar_2lep_PP8,ttgamma,Zgamma,WZ,ttW,VH
  FillColor: 5
  %FillColor: 6
  LineColor: 1

%Sample: tZ
%  Type: BACKGROUND
%  Title: tZ
%  NtupleFiles: tZ
%  FillColor: 877
%  LineColor: 1

Sample: ttZ
  Type: BACKGROUND
  Title: ttZ
  NtupleFiles: t\bar{t}Z
  FillColor: 4
  %FillColor: 8
  LineColor: 1

%Sample: tWZ
%  Type: BACKGROUND
%  Title: tWZ
%  NtupleFiles: tWZ
%  FillColor: 5
%  %FillColor: 876
%  %FillColor: 920
%  LineColor: 1

%Sample: VH
%  Type: BACKGROUND
%  Title: VH
%  NtupleFiles: VH
%  FillColor: 5
%  LineColor: 1

Sample: ttH
  Type: BACKGROUND 
  Title: t\bar{t}H
  NtupleFiles: ttH
  FillColor: 2
  LineColor: 1


Sample: data
  Type: DATA
  Title: data
  NtupleFiles: data

% ----------------------------- % 
% --- Parameter(s) of Interest --- % 
% ----------------------------- % 

NormFactor: mu_XS_WVZ
  Title: mu_XS(WVZ)
  Nominal: 1
  Min: -10
  Max: 20
  Samples: WVZ

