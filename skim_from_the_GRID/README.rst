Setup environment & Run skimming
------
Setup ATLAS and root::

  . setup.sh

Run skimming tool locally, e.g.::

  nohup python run.py file_list/test.txt selection.txt outFolder.txt >& log &
  tail -f log

In order to launch skimming of ntuples stored on the GRID scratch disks, configure VO access::

  voms-proxy-init -voms atlas --out /afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/skim_from_the_GRID/grid_proxy --valid 72:0
  chmod 755 /afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/skim_from_the_GRID/grid_proxy

and launch jobs on batch (1/sample)::

  ./submit_batch.sh test.txt 
