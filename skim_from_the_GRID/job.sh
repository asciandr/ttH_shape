#!/bin/bash

pwd
workdir=`pwd`
echo XSAMPTXT
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
localSetupROOT --skipConfirm
cp -rf /afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/skim_from_the_GRID/{skimNtuples.C,selection.txt,outFolder.txt,nominal.h,nominal.C,run.py,XsecFile.txt,header.h,startup.C} .
ls -haltr
### IF ON THE GRID 
ls -haltr /afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/skim_from_the_GRID/grid_proxy
cp /afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/skim_from_the_GRID/grid_proxy /tmp/x509up_u76083
voms-proxy-init -voms atlas -n
lsetup rucio
rucio download XFILE
#hadd -k XFILE.root XFILE/*.root
### --- N.B. hadd not working with very large root files, by default TTreeMerger max output TTree size is 100 GB. Setting it to 1 TB.
### --- see https://root-forum.cern.ch/t/root-6-04-14-hadd-100gb-and-rootlogon/24581
root.exe -b -l -q startup.C+
LD_PRELOAD=${workdir}/startup_C.so hadd -k XFILE.root XFILE/*.root
ls -haltr XFILE.root
python run.py XFILE.root selection.txt outFolder.txt
echo 'done.'
