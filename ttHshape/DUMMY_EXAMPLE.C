#include "TChain.h"
#include "TFile.h"
#include "TRandom3.h"
#include <string.h>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <string>
#include <ctime>
#include <sstream>

#include "header.h"
#include "plot_shapes.h"

using namespace std      ;

void DUMMY_EXAMPLE()
{

  const char* inFold  = "/afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/BDT/v27_looser_v27_Ml2l3_ntuples/";
  vector<TFile*> v_outFiles; v_outFiles.reserve(10);

  /* SR definition */
  TString looser4l = "@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(RunYear==2015||RunYear==2016)";

  const char*      files[] = {"BDT_ttH_aMcNloP8.root","BDT_ttZ_aMcNloP8.root"};

  int   NMAX =                (sizeof(files) / sizeof(files[0])) + 4.;


  cout<<endl;
  cout<<"** Opening root Input Files                                             **"<<endl;
  cout<<endl;
  cout<<"** Looping on the following samples:                                    **"<<endl;
  cout<<endl;
  vector<TFile*>              inputFile;
  vector<TTree*>              v_fChain;
  inputFile                   .reserve( NMAX );
  v_fChain                    .reserve( NMAX );
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TString     string_i  = string()+inFold+files[i]   ;
    TTree       *chain;
    inputFile   .push_back(new TFile(string_i));
    inputFile   .at(i)->GetObject("nominal",chain);
    v_fChain    .push_back( chain );

    cout<< inputFile.at(i)->GetName() <<endl;
  }
  TFile *fdummy = new TFile("dummy_out.root","RECREATE");
  /* selection for skimming */
  TString PreSelection = looser4l;
  cout    <<endl;
  cout    <<"********                    Applying the following PRESELECTION for Skimming:                   ********"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"''       "<< PreSelection <<"       ''"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"**                                           Skimming root files                                         **"<<endl;

  vector<TTree*>     v_fChainSkim;
  vector<nominal*>   nTree       ;
  v_fChainSkim       .reserve( NMAX );
  nTree              .reserve( NMAX );

  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    v_fChainSkim .push_back( (TTree*)v_fChain.at(i)->CopyTree( PreSelection ) );
    nTree        .push_back( new nominal(v_fChainSkim.at(i))               );
  }

  cout    <<"**                                     Skimmed     TTree contains:                                  **"<<endl;
  cout    <<endl;
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) cout<< inputFile.at(i)->GetName() <<"  ->  "<< v_fChainSkim.at(i)->GetEntriesFast() <<"  *RAW* events        **"<<endl;
  cout    <<endl;
 
  for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {
    cout<<endl;
    cout<<"****                    Processing "<< inputFile.at(t)->GetName() <<"                    ****"<<endl;
    cout<<endl;

    /* OUTPUT FILES */
    string out_name="";// = string()+"shapesfile_4lep_v27_"+files[t];
    if(t==0) 		out_name=string()+"DUMMYfile_4lep_v27_ttH.root";
    else if(t==1)     	out_name=string()+"DUMMYfile_4lep_v27_ttZ.root";  
    else 		cout<<"WHAT ARE YOU DOING?? CHECK ME!"<<endl;

    TFile *fout = new TFile(out_name.c_str(),"RECREATE");


    string hist_etmiss = string()+"hist_etmiss_"+files[t];
    TH1F   *h_etmiss = new TH1F(hist_etmiss.c_str(),"", 20, 0., 300.);
    h_etmiss->Sumw2();

    Long64_t nentries = v_fChainSkim.at(t)->GetEntriesFast();
    /* Looping over the events */
    for (Long64_t ievt=0; ievt<nentries; ievt++) {
      int trash=nTree.at(t)->GetEntry(ievt);
      /* weight each event in the proper way */
      const double weight = nTree.at(t)->mcWeightOrg*nTree.at(t)->xsec*(1./nTree.at(t)->totweight)*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->MV2c10_70_EventWeight;
      h_etmiss         ->Fill(nTree.at(t)->MET_RefFinal_et/1000.,weight);

    }//for (Long64_t ievt=0; ievt<nentries; ievt++) 
    fdummy->Add(h_etmiss); 
    fout->Add(h_etmiss);
    fout->Write();
    //fout->Close();

  }//for(int t=0; t<sizeof(files) / sizeof(files[0]); t++)
  cout<<"Templates are ready!"<<endl;


}
