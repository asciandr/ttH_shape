///////////////////////// -*- C++ -*- /////////////////////////////
// ttH->4l Shape Analysis
// Author: A.Sciandra <andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TChain.h"
#include "TFile.h"
#include "TRandom3.h"
#include <string.h>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <string>
#include <ctime>
#include <sstream>

#include "compare_plot_shapes.h"

using namespace std      ;

int main() {
  clock_t begin       = clock();
  bool    plot        = true;
  bool    debug       = true;//false;
  //Number of leps + 1
  int     n_leps(5); 
  const char* inFold  = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/FW2_v28_nominal/";
  const char* outFold = "/afs/cern.ch/user/a/asciandr/www/ttH/new_reco_shapes/";
  TString preMVA ="quadlep_type&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatch_3||lep_isTrigMatchDLT_0||lep_isTrigMatchDLT_1||lep_isTrigMatchDLT_2||lep_isTrigMatchDLT_3)&&!total_charge&&(lep_ID_0!=-lep_ID_1||Mll01>12e3&&abs(Mll01-91.2e3)>10e3)&&(lep_ID_0!=-lep_ID_2||Mll02>12e3&&abs(Mll02-91.2e3)>10e3)&&(lep_ID_0!=-lep_ID_3||Mll03>12e3&&abs(Mll03-91.2e3)>10e3)&&(lep_ID_1!=-lep_ID_2||Mll12>12e3&&abs(Mll12-91.2e3)>10e3)&&(lep_ID_1!=-lep_ID_3||Mll13>12e3&&abs(Mll13-91.2e3)>10e3)&&(lep_ID_2!=-lep_ID_3||Mll23>12e3&&abs(Mll23-91.2e3)>10e3)&&nJets_OR>1&&nJets_OR_MV2c10_70&&abs(Mllll0123-125e3)>5e3&&(lep_promptLeptonIso_TagWeight_2<-0.5&&lep_promptLeptonIso_TagWeight_3<-0.5)&&(!(lep_ID_0+lep_ID_1)||!(lep_ID_0+lep_ID_2)||!(lep_ID_0+lep_ID_3)||!(lep_ID_1+lep_ID_2)||!(lep_ID_1+lep_ID_3)||!(lep_ID_2+lep_ID_3))";
   
  const char*      files[] = {"ttH_aMcNloP8.root"};//,"ttZ_aMcNloP8.root"};
  //const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root"};

  float Lum_tot = 36074.6;
  float Lum(1.);
  Lum = Lum_tot       ;
  cout<<endl;
  cout<<"--------------> Running at an integrated luminosity of: "<< Lum/1000 <<" fb^(-1): "<<endl;
  int   NMAX =                (sizeof(files) / sizeof(files[0])) + 4.;
  vector<float>               Signal;
  vector<float>               Back  ;
  Signal                      .reserve(NMAX);
  Back                        .reserve(NMAX);
  cout<<endl;
  cout<<"   ///////////////////////// -*- C++ -*- /////////////////////////////"<<endl;
  cout<<"  // ttH->4l Shape Analysis                                        //"<<endl;
  cout<<" // Author: A.Sciandra <andrea.sciandra@cern.ch>                  //"<<endl;
  cout<<"///////////////////////////////////////////////////////////////////"<<endl;
  cout<<endl;
  if ( debug )      cout<<"!!    DEBUG mode                !!"<<endl;
  if ( plot  )      cout<<"!!    Plot option activated     !!"<<endl;
  cout<<endl;
  cout<<"** Reserving capacity() = "<< Signal.capacity() <<" for meaningful vectors       **"<<endl;
  cout<<endl;

  vector<TH1F*>     v_hN;
  v_hN              .reserve(NMAX);

  cout<<endl;
  cout<<"** Opening root Input Files                                             **"<<endl;
  cout<<endl;
  cout<<"** Looping on the following samples:                                    **"<<endl;
  cout<<endl;
  vector<TFile*>              inputFile;
  vector<TTree*>              v_fChain;
  inputFile                   .reserve( NMAX );
  v_fChain                    .reserve( NMAX );
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TString     string_i  = string()+inFold+files[i]   ;
    TTree       *chain;
    inputFile   .push_back(new TFile(string_i));
    inputFile   .at(i)->GetObject("nominal",chain);
    v_fChain    .push_back( chain );

    cout<< inputFile.at(i)->GetName() <<endl;
  }
  TString PreSelection = preMVA;
  
  cout    <<endl;
  cout    <<"********                    Applying the following PRESELECTION for Skimming:                   ********"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"''       "<< PreSelection <<"       ''"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"**                                           Skimming root files                                         **"<<endl;

  vector<TTree*>     v_fChainSkim;
  vector<nominal*>   nTree       ;
  v_fChainSkim       .reserve( NMAX );
  nTree              .reserve( NMAX );

  /* OUTPUT FILES */
  TFile *f = new TFile("shapes.root","RECREATE");
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    v_fChainSkim .push_back( (TTree*)v_fChain.at(i)->CopyTree( PreSelection ) );
    nTree        .push_back( new nominal(v_fChainSkim.at(i))               );
  }
  clock_t end1 = clock();
  double  elapsed_secs = double(end1 - begin) / CLOCKS_PER_SEC;
  cout    <<endl;

  string histName3 = string()+"htotMC";
  TH1F   *hMC  = new TH1F(histName3.c_str(),"",11, -0.5, 10.5);
  hMC->Sumw2();

  /* Plotting */
  vector<TH1F*> v_PME, v_Ml2l3met, v_Mllll0123;
  vector<TH1F*> v_best_Z_Mll, v_HT_lep;
  vector<TH1F*> v_best_Z_other_Mll, v_MET_RefFinal_et;
  v_PME.reserve(NMAX); v_Ml2l3met.reserve(NMAX); v_Mllll0123.reserve(NMAX);
  v_best_Z_Mll.reserve(NMAX);
  v_best_Z_other_Mll.reserve(NMAX);
  v_MET_RefFinal_et.reserve(NMAX);
  v_HT_lep.reserve(NMAX);

  for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {
    cout<<endl;
    cout<<"****                    Processing "<< inputFile.at(t)->GetName() <<"                    ****"<<endl;
    cout<<endl;

    double weight[]= {nTree.at(t)->scale_nom*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->MV2c10_70_EventWeight,nTree.at(t)->scale_nom*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->MV2c10_70_EventWeight*nTree.at(t)->lep_SFObjLoose_0*nTree.at(t)->lep_SFObjLoose_1*nTree.at(t)->lep_SFObjTight_2*nTree.at(t)->lep_SFObjTight_3};

    for(int w=0; w<sizeof(weight) / sizeof(weight[0]); w++) {
      stringstream ss;
      ss << w;
      string number = ss.str();

      cout<<"WEIGHT: "<< w <<endl;

      string histName  = string()+"hN_"+inputFile.at(t)->GetName()+number;
      TH1F   *hN    = new TH1F(histName.c_str() ,"", 11, -0.5, 10.5);
      hN     ->Sumw2();

      string histPME = string()+"h_PME_"+inputFile.at(t)->GetName()+number;
      TH1F   *hPME    = new TH1F(histPME.c_str() ,"", 14, -2., 5.);
      hPME     ->Sumw2();

      string histMl2l3met = string()+"h_Ml2l3met_"+inputFile.at(t)->GetName()+number;
      TH1F   *hMl2l3met = new TH1F(histMl2l3met.c_str() ,"", 10, 0., 500.);
      hMl2l3met     ->Sumw2();

      string histMllll0123 = string()+"h_Mllll0123_"+inputFile.at(t)->GetName()+number;
      TH1F   *hMllll0123 = new TH1F(histMllll0123.c_str() ,"", 15, 0., 600.);
      hMllll0123     ->Sumw2(); 

      string histbest_Z_Mll = string()+"h_best_Z_Mll_"+inputFile.at(t)->GetName()+number;
      TH1F   *hbest_Z_Mll = new TH1F(histbest_Z_Mll.c_str() ,"", 10, 40., 150.);
      hbest_Z_Mll     ->Sumw2();

      string histbest_Z_other_Mll = string()+"h_best_Z_other_Mll_"+inputFile.at(t)->GetName()+number;
      TH1F   *hbest_Z_other_Mll = new TH1F(histbest_Z_other_Mll.c_str() ,"", 10, 40., 150.);
      hbest_Z_other_Mll     ->Sumw2();

      string histMET_RefFinal_et = string()+"h_MET_RefFinal_et_"+inputFile.at(t)->GetName()+number;
      TH1F   *hMET_RefFinal_et = new TH1F(histMET_RefFinal_et.c_str() ,"", 20, 0., 300.);
      hMET_RefFinal_et     ->Sumw2();

      string histHT_lep = string()+"h_HT_lep_"+inputFile.at(t)->GetName()+number;
      TH1F   *hHT_lep = new TH1F(histHT_lep.c_str() ,"", 10, 0., 500.);
      hHT_lep     ->Sumw2();

      Long64_t nentries = v_fChainSkim.at(t)->GetEntriesFast();
      /* Looping over the events */
      for (Long64_t ievt=0; ievt<nentries;ievt++) {
      //for (Long64_t ievt=0; ievt<1;ievt++) 
        int trash=nTree.at(t)->GetEntry(ievt);
        /* BOOLEANS */
        bool goodOne(false);
        double myweight(-1000.);
        if(w==0) myweight = (w==0) ? nTree.at(t)->scale_nom*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->MV2c10_70_EventWeight : nTree.at(t)->scale_nom*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->MV2c10_70_EventWeight*nTree.at(t)->lep_SFObjLoose_0*nTree.at(t)->lep_SFObjLoose_1*nTree.at(t)->lep_SFObjTight_2*nTree.at(t)->lep_SFObjTight_3;
        //const int njets = nTree.at(t)->m_jet_pt->size();
        //int nbjets(0.);
        //vector<int> v_bjetIndex; v_bjetIndex.reserve(20);
        goodOne=true;//further selection on top of skimming?
        float mindRlj(99.), maxdRlj(-99.);
        int orig_mindRlj(-999.);
        /* min dR(lep,b-tagged jet) */
        float mindRlb(99.), maxdRlb(-99.);
        int orig_mindRlb(-999.), lepInd_mindRlb(-999.);
        if ( goodOne ) {
          hPME->Fill(nTree.at(t)->fourLep_PME,myweight);	
          hMl2l3met->Fill(nTree.at(t)->Ml2l3met/1000.,myweight);
	  hMllll0123->Fill(nTree.at(t)->Mllll0123/1000.,myweight);
 	  hbest_Z_Mll->Fill(nTree.at(t)->best_Z_Mll/1000.,myweight);
 	  hbest_Z_other_Mll->Fill(nTree.at(t)->best_Z_other_Mll/1000.,myweight);
          hMET_RefFinal_et->Fill(nTree.at(t)->MET_RefFinal_et/1000.,myweight);
          hHT_lep->Fill(nTree.at(t)->HT_lep/1000.,myweight);

        }//if ( goodOne )
      }//for (Long64_t ievt=0; ievt<nentries;ievt++)
      v_PME.push_back(hPME);
      v_Ml2l3met.push_back(hMl2l3met);
      v_Mllll0123.push_back(hMllll0123);
      v_best_Z_Mll.push_back(hbest_Z_Mll);
      v_best_Z_other_Mll.push_back(hbest_Z_other_Mll);
      v_MET_RefFinal_et.push_back(hMET_RefFinal_et);
      v_HT_lep.push_back(hHT_lep);
    }//for(weights)
  }//for(int t=0; t<sizeof(files) / sizeof(files[0]); t++)

  if (plot) {

    TString fakepdfTitle = string() + outFold + "PME.pdf";
    TH1F *fakePME = new TH1F("fakePME","",v_PME.at(0)->GetNbinsX(),v_PME.at(0)->GetBin(1),v_PME.at(0)->GetBin(v_PME.at(0)->GetNbinsX()));
    fakePME = PlotMC(Lum,v_PME,fakepdfTitle,"PME "," ");

    TString pdfTitle = string() + outFold + "PME.pdf";
    TH1F *PME = new TH1F("PME","",v_PME.at(0)->GetNbinsX(),v_PME.at(0)->GetBin(1),v_PME.at(0)->GetBin(v_PME.at(0)->GetNbinsX()));
    PME = PlotMC(Lum,v_PME,pdfTitle,"PME "," ");
    
    TString pdfTitle1 = string() + outFold + "Ml2l3met.pdf";
    TH1F *Ml2l3met = new TH1F("Ml2l3met","",v_PME.at(0)->GetNbinsX(),v_PME.at(0)->GetBin(1),v_PME.at(0)->GetBin(v_PME.at(0)->GetNbinsX()));
    Ml2l3met = PlotMC(Lum,v_Ml2l3met,pdfTitle1,"M_{l2l3met} [GeV] "," GeV");
   
    TString pdfTitle2 = string() + outFold + "Mllll0123.pdf";
    TH1F *Mllll0123 = new TH1F("Mllll0123","",v_PME.at(0)->GetNbinsX(),v_PME.at(0)->GetBin(1),v_PME.at(0)->GetBin(v_PME.at(0)->GetNbinsX()));
    Mllll0123 = PlotMC(Lum,v_Mllll0123,pdfTitle2,"M_{4l} [GeV] "," GeV");

    TString pdfTitle3 = string() + outFold + "best_Z_Mll.pdf";
    TH1F *best_Z_Mll = new TH1F("best_Z_Mll","",v_PME.at(0)->GetNbinsX(),v_PME.at(0)->GetBin(1),v_PME.at(0)->GetBin(v_PME.at(0)->GetNbinsX()));
    best_Z_Mll = PlotMC(Lum,v_best_Z_Mll,pdfTitle3," best Z M_{ll} [GeV] "," GeV");

    TString pdfTitle4 = string() + outFold + "best_Z_other_Mll.pdf";
    TH1F *best_Z_other_Mll = new TH1F("best_Z_other_Mll","",v_PME.at(0)->GetNbinsX(),v_PME.at(0)->GetBin(1),v_PME.at(0)->GetBin(v_PME.at(0)->GetNbinsX()));
    best_Z_other_Mll = PlotMC(Lum,v_best_Z_other_Mll,pdfTitle4," best Z other M_{ll} [GeV] "," GeV");

    TString pdfTitle5 = string() + outFold + "MET_RefFinal_et.pdf";
    TH1F *MET_RefFinal_et = new TH1F("MET_RefFinal_et","",v_PME.at(0)->GetNbinsX(),v_PME.at(0)->GetBin(1),v_PME.at(0)->GetBin(v_PME.at(0)->GetNbinsX()));
    MET_RefFinal_et = PlotMC(Lum,v_MET_RefFinal_et,pdfTitle5," E_{T}^{miss} [GeV] "," GeV");

    TString pdfTitle6 = string() + outFold + "HT_lep.pdf";
    TH1F *HT_lep = new TH1F("HT_lep","",v_PME.at(0)->GetNbinsX(),v_PME.at(0)->GetBin(1),v_PME.at(0)->GetBin(v_PME.at(0)->GetNbinsX()));
    HT_lep = PlotMC(Lum,v_HT_lep,pdfTitle6," H_{T}^{lep} [GeV] "," GeV");
 
  }

}//int main()
