///////////////////////// -*- C++ -*- /////////////////////////////
// Author: A.Sciandra<andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TROOT.h"
#include "TTree.h"
#include "TString.h"
#include "THStack.h"
#include "TLegend.h"
#include <string.h>
#include <TLorentzVector.h>
#include <iostream>
#include "TLine.h"
#include "TH1F.h"
#include "TROOT.h"
#include "TTree.h"
#include "TString.h"
#include "THStack.h"
#include <string.h>
#include <TLorentzVector.h>
#include <iostream>

#include "nominal.C"

using namespace std;

/* Obtain name of the samples from histos' names */
static string getSampleName(string name) {
  size_t pos		= name.find("_aMcNloP8.root");
  //string name1		= name.substr(pos,name.size());
  //size_t pos1  		= name1.find(".root");
  string myname 	= name.substr(pos-3,pos);

  return myname;
}

/*Compare shapes WITH ERROR BARS in MC */
static TH1F *PlotMCError(double Lum, vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit) {//Compare MC shapes
  int n = histos.size()+1;
  vector<TH1F*> new_histos;
  vector<int>   colours;    
  new_histos    .reserve( n );
  colours       .reserve( n );
  colours.push_back(2);
  colours.push_back(4);
  colours.push_back(7);
  colours.push_back(11);
  colours.push_back(3);
  colours.push_back(13);
  colours.push_back(5);
  colours.push_back(46);
  colours.push_back(41);
  colours.push_back(8);
  colours.push_back(9);
  colours.push_back(12);

  TCanvas *cs = new TCanvas(outFile_name);
  TLegend *leg = new TLegend(0.83,0.88,0.63,0.55);
  leg->SetTextSize(0.04);
  gStyle->SetLegendBorderSize(0);
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    TH1F *hnew = (TH1F*)histos.at(i)->Clone(name.c_str());
    hnew->SetFillColor(colours.at(i));
    hnew->SetFillStyle(3003);
    hnew->SetLineColor(colours.at(i));
    hnew->SetMarkerColor(colours.at(i));
    if(hnew->Integral()!=0&&(((name).find("data"))==string::npos)) hnew->Scale(Lum);
    new_histos.push_back( hnew );
  }
  /* Set Range */
  double max(-1000.);
  //max = new_histos.at(0)->GetMaximum()+4*(new_histos.at(0)->GetBinError(new_histos.at(0)->GetMaximumBin()));
  for(int i=0; i<new_histos.size();i++) {
    if( new_histos.at(i)->GetMaximum()+1.5*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin())>max ) max = new_histos.at(i)->GetMaximum()+1.5*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin());
  }
  new_histos.at(0)->GetYaxis()->SetRangeUser(0,max);
  TString Xtitle = (string)axisName;
  new_histos.at(0)->GetXaxis()->SetTitle(Form(Xtitle));
  TString Ytitle = string()+"Entries / %2.2f"+entriesUnit;
  new_histos.at(0)->GetYaxis()->SetTitle(Form(Ytitle,new_histos.at(0)->GetBinWidth(1)));
  //new_histos.at(0)->GetYaxis()->SetTitleOffset(1.3);
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    TLegendEntry *entry = leg->AddEntry(new_histos.at(i),getSampleName(name).c_str(),"f");
    if(i==0&&new_histos.at(i)->Integral()!=0) 	new_histos.at(i)->Draw("p");
    else if(new_histos.at(i)->Integral()!=0)	new_histos.at(i)->Draw("Psame");
  }
  leg->Draw("same");

  gStyle->SetOptStat(0);
  cs->SaveAs(outFile_name,"RECREATE");

  return histos.at(0);
}

/*Compare shapes in MC with ratio to appreciate discrimination */
static TH1F *PlotMCRatio(double Lum, vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit) {//Compare MC shapes
  int n = histos.size()+1;
  vector<TH1F*> new_histos;
  vector<int>   colours;    
  new_histos    .reserve( n );
  colours       .reserve( n );
  colours.push_back(2);
  colours.push_back(4);
  colours.push_back(7);
  colours.push_back(11);
  colours.push_back(3);
  colours.push_back(13);
  colours.push_back(5);
  colours.push_back(46);
  colours.push_back(41);
  colours.push_back(8);
  colours.push_back(9);
  colours.push_back(12);

  TCanvas *cs = new TCanvas(outFile_name);
  TLegend *leg = new TLegend(0.83,0.88,0.63,0.55);
  leg->SetTextSize(0.04);
  gStyle->SetLegendBorderSize(0);
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    TH1F *hnew = (TH1F*)histos.at(i)->Clone(name.c_str());
    hnew->SetFillColor(colours.at(i));
    hnew->SetFillStyle(3003);
    hnew->SetLineColor(colours.at(i));
    hnew->SetMarkerColor(colours.at(i));
    if(hnew->Integral()!=0) hnew->Scale(Lum);
    new_histos.push_back( hnew );
  }
  /* Set Range */
  double max(-1000.);
  //max = new_histos.at(0)->GetMaximum()+4*(new_histos.at(0)->GetBinError(new_histos.at(0)->GetMaximumBin()));
  for(int i=0; i<new_histos.size();i++) {
    if(new_histos.at(i)->Integral()!=0) new_histos.at(i)->Scale(1./new_histos.at(i)->Integral());
    if( new_histos.at(i)->GetMaximum()+new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin())>max ) max = new_histos.at(i)->GetMaximum()+1.5*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin());
  }
  //cout<<"MAX = "<< max <<endl;
  new_histos.at(0)->GetYaxis()->SetRangeUser(0,max);
  TString Xtitle = (string)axisName;
  new_histos.at(0)->GetXaxis()->SetTitle(Form(Xtitle));
  TString Ytitle = string()+"Entries / %2.2f"+entriesUnit;
  new_histos.at(0)->GetYaxis()->SetTitle(Form(Ytitle,new_histos.at(0)->GetBinWidth(1)));
  //Pad 1
  TPad *pad1 = new TPad("pad1","pad1",0.,0.33,1.,1.);
  pad1->SetTopMargin(1.2);
  pad1->SetBottomMargin(0.05);
  pad1->Draw();
  pad1->cd();
  //new_histos.at(0)->GetYaxis()->SetTitleOffset(1.3);
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    //TLegendEntry *entry = leg->AddEntry(new_histos.at(i),getSampleName(name).c_str(),"f");
    if(i==0&&new_histos.at(i)->Integral()!=0) 	new_histos.at(i)->DrawNormalized("HIST");
    else if(new_histos.at(i)->Integral()!=0)	new_histos.at(i)->DrawNormalized("HISTsame");
  }
  TLegendEntry *entry = leg->AddEntry(new_histos.at(0),"ttH","f");
  entry = leg->AddEntry(new_histos.at(1),"ttZ","f");
  leg->Draw("same");

  //Plot ttH/ttZ ratio 
  TH1F *ratio = (TH1F*)new_histos.at(0)->Clone("ratio");
  cs->cd();
  TPad *pad2 = new TPad("pad2","pad2",0.,0.,1.,0.3);
  pad2->SetTopMargin(0.05);
  pad2->SetBottomMargin(0.3);
  pad2->Draw();
  pad2->cd();
  ratio->Divide(new_histos.at(1));
  ratio->GetYaxis()->SetRangeUser(0.,2.);
  ratio->SetMarkerStyle(20);
  ratio->SetMarkerSize(0.5);
  ratio->SetMarkerColor(kBlack);
  ratio->SetLineColor(kBlack);
  ratio->GetXaxis()->SetLabelSize(0.08);
  ratio->GetYaxis()->SetLabelSize(0.085);
  ratio->GetYaxis()->SetTitle("ttH/ttZ");
  ratio->GetXaxis()->SetTitleSize(0.12);
  ratio->GetXaxis()->SetTitleOffset(0.8);
  ratio->GetYaxis()->SetTitleSize(0.12);
  ratio->GetYaxis()->SetTitleOffset(0.22);

  TLine *line = new TLine(0.1,0.187,0.9,0.187);
  line->SetLineStyle(8);
  ratio->Draw("epx");
  cs->cd(0);
  cs->cd(2);
  line->Draw("same");
 

  gStyle->SetOptStat(0);
  cs->SaveAs(outFile_name,"RECREATE");

  return histos.at(0);
}



/*Compare shapes in MC */
static TH1F *PlotMC(double Lum, vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit) {//Compare MC shapes
  int n = histos.size()+1;
  vector<TH1F*> new_histos;
  vector<int>   colours;    
  new_histos    .reserve( n );
  colours       .reserve( n );
  colours.push_back(2);
  colours.push_back(4);
  colours.push_back(7);
  colours.push_back(11);
  colours.push_back(3);
  colours.push_back(13);
  colours.push_back(5);
  colours.push_back(46);
  colours.push_back(41);
  colours.push_back(8);
  colours.push_back(9);
  colours.push_back(12);

  TCanvas *cs = new TCanvas(outFile_name);
  TLegend *leg = new TLegend(0.83,0.88,0.63,0.55);
  leg->SetTextSize(0.04);
  gStyle->SetLegendBorderSize(0);
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    TH1F *hnew = (TH1F*)histos.at(i)->Clone(name.c_str());
    hnew->SetFillColor(colours.at(i));
    hnew->SetFillStyle(3003);
    hnew->SetLineColor(colours.at(i));
    hnew->SetMarkerColor(colours.at(i));
    if(hnew->Integral()!=0) hnew->Scale(Lum);
    new_histos.push_back( hnew );
  }
  /* Set Range */
  double max(-1000.);
  //max = new_histos.at(0)->GetMaximum()+4*(new_histos.at(0)->GetBinError(new_histos.at(0)->GetMaximumBin()));
  for(int i=0; i<new_histos.size();i++) {
    if(new_histos.at(i)->Integral()!=0) new_histos.at(i)->Scale(1./new_histos.at(i)->Integral());
    if( new_histos.at(i)->GetMaximum()+new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin())>max ) max = new_histos.at(i)->GetMaximum()+1.5*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin());
  }
  //cout<<"MAX = "<< max <<endl;
  new_histos.at(0)->GetYaxis()->SetRangeUser(0,max);
  TString Xtitle = (string)axisName;
  new_histos.at(0)->GetXaxis()->SetTitle(Form(Xtitle));
  TString Ytitle = string()+"Entries / %2.2f"+entriesUnit;
  new_histos.at(0)->GetYaxis()->SetTitle(Form(Ytitle,new_histos.at(0)->GetBinWidth(1)));
  //new_histos.at(0)->GetYaxis()->SetTitleOffset(1.3);
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    string dummy = i==0 ? "ttH v27" : "ttH v28"; 
    TLegendEntry *entry = leg->AddEntry(new_histos.at(i),dummy.c_str(),"f");
    //TLegendEntry *entry = leg->AddEntry(new_histos.at(i),getSampleName(name).c_str(),"f");
    if(i==0&&new_histos.at(i)->Integral()!=0) 	new_histos.at(i)->DrawNormalized("HIST");
    else if(new_histos.at(i)->Integral()!=0)	new_histos.at(i)->DrawNormalized("HISTsame");
  }
  leg->Draw("same");

  gStyle->SetOptStat(0);
  cs->SaveAs(outFile_name,"RECREATE");

  return histos.at(0);
}

/* Produce Stack Plots */
static THStack *stackPlot(vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit){ //Give a vector of histos and realise a stack plot from it
  int n = histos.size()+1; 
  THStack *hs = new THStack("hs","");
  double Lum = 36074.6;
  //clone input histos
  vector<TH1F*> new_histos; 
  //colours
  vector<int>   colours;    
  new_histos    .reserve( n );
  colours       .reserve( n );
  // Set colours for different MC samples
  colours.push_back(2);
  colours.push_back(4);
  colours.push_back(7);
  colours.push_back(11);
  colours.push_back(13);
  colours.push_back(3);
  colours.push_back(5);
  colours.push_back(46);
  colours.push_back(41);
  colours.push_back(8);
  colours.push_back(9);
  colours.push_back(12);
  
  int N_MC(0);
  N_MC = histos.size()-1;

  for(int i=0; i<N_MC; i++) {
    string name  = histos.at(i)->GetName();
    TH1F *hnew = (TH1F*)histos.at(i)->Clone(name.c_str());
    hnew->SetFillColor(colours.at(i));
    hnew->Scale(Lum);
    new_histos.push_back( hnew );
  }
 
  TCanvas *cs = new TCanvas(outFile_name);
  TLegend *leg = new TLegend(0.8,0.7,0.6,0.88);
  leg->SetTextSize(0.04);
  gStyle->SetLegendBorderSize(0);
  for(int i=0; i<N_MC;i++) {
    new_histos.at(i)->SetFillColor(colours.at(i));
    new_histos.at(i)->SetLineColor(colours.at(i));
    new_histos.at(i)->SetMarkerColor(colours.at(i));
    string name  = getSampleName(histos.at(i)->GetName());
    TLegendEntry *entry = leg->AddEntry(new_histos.at(i),name.c_str(),"f"); 
    //if(i==0) hs->Add(new_histos.at(i));
    hs->Add(new_histos.at(i));
  }
  // -----DATA----- //
  int N_data = histos.size()-1;
  string nameD  = getSampleName(histos.at(N_data)->GetName());
  TH1F *hData   = (TH1F*)histos.at(N_data)->Clone(nameD.c_str());
  hData->SetMarkerColor(kBlack);
  hData->SetMarkerSize(1.);
  hData->SetMarkerStyle(21);
  hData->SetLineColor(kBlack);
  TString Xtitle = (string)axisName;
  hData->GetXaxis()->SetTitle(Form(Xtitle));
  TString Ytitle = string()+"Entries / %2.0f"+entriesUnit;
  hData->GetYaxis()->SetTitle(Form(Ytitle,hData->GetBinWidth(1)));
  hData->GetYaxis()->SetTitleOffset(1.3);
  new_histos.push_back( hData );
  TLegendEntry *entry = leg->AddEntry(hData,nameD.c_str(),"pe");

  // Set Range //
  double max(1000);
  max = hs->GetMaximum()+0.2*hs->GetMaximum();//3*(hs->GetBinError(hs->GetMaximumBin()));
  //max = new_histos.at(0)->GetMaximum()+3*(new_histos.at(0)->GetBinError(new_histos.at(0)->GetMaximumBin()));
  //for(int i=1; i<new_histos.size();i++) {
  //  if( new_histos.at(i)->GetMaximum()+3*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin())>max ) max = new_histos.at(i)->GetMaximum()+3*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin());
  //}
  
  // Plot Stack MC, Data and Data/MC ratio // 
  //Clone Data for ratio plot
  TH1F *hData1 = (TH1F*)hData->Clone("hData1");

  TPad *pad1 = new TPad("pad1","pad1",0.,0.33,1.,1.);
  pad1->SetTopMargin(1.2);
  pad1->SetBottomMargin(0.05);
  pad1->Draw();
  pad1->cd();
  //hData->GetXaxis()->SetRangeUser(0,50);
  hData->GetYaxis()->SetRangeUser(0,max);
  hData->GetXaxis()->SetTitle("");
  hData->GetXaxis()->SetTitleSize(0.05);
  hData->GetYaxis()->SetTitleSize(0.05);
  hData->GetXaxis()->SetTitleOffset(0.6);
  hData->GetYaxis()->SetTitleOffset(0.7);
  gStyle->SetOptStat(0);
  hData->Draw("");
  hs->Draw("sameHIST");
  hData->Draw("PEsame");
  leg->Draw("same");
  
  
  // Ratio plot //
  //Need a TH1 as a sum of all MC histos
  TH1F *hsum = (TH1F*)new_histos.at(0)->Clone("hsum");
  for(int i=1; i<N_MC;i++) {
    hsum->Add(new_histos.at(i));
  }

  cs->cd();
  TPad *pad2 = new TPad("pad2","pad2",0.,0.,1.,0.3);
  pad2->SetTopMargin(0.05);
  pad2->SetBottomMargin(0.3);
  pad2->Draw();
  pad2->cd();
  //cout<< hData1->GetBinContent(2) <<endl;
  //cout<< hsum  ->GetBinContent(2) <<endl;
  hData1->Divide(hsum);
  //cout<< hData1->GetBinContent(2) <<endl;
  hData1->GetYaxis()->SetRangeUser(0.,2.);
  hData1->SetMarkerStyle(20);
  hData1->SetMarkerSize(0.5);
  hData1->SetMarkerColor(kBlack);
  hData1->SetLineColor(kBlack);
  hData1->GetXaxis()->SetLabelSize(0.08);
  hData1->GetYaxis()->SetLabelSize(0.085);
  hData1->GetYaxis()->SetTitle("Data / MC");
  //hData1->GetXaxis()->SetTitle(Form("#DeltaR"));
  hData1->GetXaxis()->SetTitleSize(0.12);
  hData1->GetXaxis()->SetTitleOffset(0.8);
  hData1->GetYaxis()->SetTitleSize(0.12);
  hData1->GetYaxis()->SetTitleOffset(0.22);
  
  TLine *line = new TLine(0.1,0.187,0.9,0.187);
  line->SetLineStyle(8);
  //line->SetLineColor(kBlack);
  hData1->Draw("epx");
  cs->cd(0);
  cs->cd(2);
  line->Draw("same");
  
  //hData1->Draw("APsame");
  
  cs->cd();
  cs->SaveAs(outFile_name,"RECREATE");

  return hs;
}

/* Produce Stack Plots with Log scale */
static THStack *stackPlotLog(vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit){ //Give a vector of histos and realise a stack plot from it
  int n = histos.size()+1; 
  THStack *hs = new THStack("hs","");
  double Lum = 36074.6;
  //clone input histos
  vector<TH1F*> new_histos; 
  //colours
  vector<int>   colours;    
  new_histos    .reserve( n );
  colours       .reserve( n );
  // Set colours for different MC samples
  colours.push_back(2);
  colours.push_back(4);
  colours.push_back(7);
  colours.push_back(11);
  colours.push_back(13);
  colours.push_back(3);
  colours.push_back(5);
  colours.push_back(46);
  colours.push_back(41);
  colours.push_back(8);
  colours.push_back(9);
  colours.push_back(12);
  
  int N_MC(0);
  N_MC = histos.size()-1;

  for(int i=0; i<N_MC; i++) {
    string name  = histos.at(i)->GetName();
    TH1F *hnew = (TH1F*)histos.at(i)->Clone(name.c_str());
    hnew->SetFillColor(colours.at(i));
    hnew->Scale(Lum);
    new_histos.push_back( hnew );
  }
 
  TCanvas *cs = new TCanvas(outFile_name);
  cs->SetLogy();
  TLegend *leg = new TLegend(0.8,0.7,0.6,0.88);
  leg->SetTextSize(0.04);
  gStyle->SetLegendBorderSize(0);
  for(int i=0; i<N_MC;i++) {
    new_histos.at(i)->SetFillColor(colours.at(i));
    new_histos.at(i)->SetLineColor(colours.at(i));
    new_histos.at(i)->SetMarkerColor(colours.at(i));
    string name  = getSampleName(histos.at(i)->GetName());
    TLegendEntry *entry = leg->AddEntry(new_histos.at(i),name.c_str(),"f"); 
    //if(i==0) hs->Add(new_histos.at(i));
    hs->Add(new_histos.at(i));
  }
  // -----DATA----- //
  int N_data = histos.size()-1;
  string nameD  = getSampleName(histos.at(N_data)->GetName());
  TH1F *hData   = (TH1F*)histos.at(N_data)->Clone(nameD.c_str());
  hData->SetMarkerColor(kBlack);
  hData->SetMarkerSize(1.);
  hData->SetMarkerStyle(21);
  hData->SetLineColor(kBlack);
  TString Xtitle = (string)axisName;
  hData->GetXaxis()->SetTitle(Form(Xtitle));
  TString Ytitle = string()+"Entries / %2.0f"+entriesUnit;
  hData->GetYaxis()->SetTitle(Form(Ytitle,hData->GetBinWidth(1)));
  hData->GetYaxis()->SetTitleOffset(1.3);
  new_histos.push_back( hData );
  TLegendEntry *entry = leg->AddEntry(hData,nameD.c_str(),"pe");

  // Set Range //
  double max(1000);
  max = hs->GetMaximum()+0.2*hs->GetMaximum();//3*(hs->GetBinError(hs->GetMaximumBin()));
  //max = new_histos.at(0)->GetMaximum()+3*(new_histos.at(0)->GetBinError(new_histos.at(0)->GetMaximumBin()));
  //for(int i=1; i<new_histos.size();i++) {
  //  if( new_histos.at(i)->GetMaximum()+3*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin())>max ) max = new_histos.at(i)->GetMaximum()+3*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin());
  //}
  
  // Plot Stack MC, Data and Data/MC ratio // 
  //Clone Data for ratio plot
  TH1F *hData1 = (TH1F*)hData->Clone("hData1");

  TPad *pad1 = new TPad("pad1","pad1",0.,0.33,1.,1.);
  pad1->SetTopMargin(1.2);
  pad1->SetBottomMargin(0.05);
  pad1->Draw();
  pad1->cd();
  //hData->GetXaxis()->SetRangeUser(0,50);
  hData->GetYaxis()->SetRangeUser(0,max);
  hData->GetXaxis()->SetTitle("");
  hData->GetXaxis()->SetTitleSize(0.05);
  hData->GetYaxis()->SetTitleSize(0.05);
  hData->GetXaxis()->SetTitleOffset(0.6);
  hData->GetYaxis()->SetTitleOffset(0.7);
  gStyle->SetOptStat(0);
  hData->Draw("");
  hs->Draw("sameHIST");
  hData->Draw("PEsame");
  leg->Draw("same");
  
  
  // Ratio plot //
  //Need a TH1 as a sum of all MC histos
  TH1F *hsum = (TH1F*)new_histos.at(0)->Clone("hsum");
  for(int i=1; i<N_MC;i++) {
    hsum->Add(new_histos.at(i));
  }

  cs->cd();
  TPad *pad2 = new TPad("pad2","pad2",0.,0.,1.,0.3);
  pad2->SetTopMargin(0.05);
  pad2->SetBottomMargin(0.3);
  pad2->Draw();
  pad2->cd();
  //cout<< hData1->GetBinContent(2) <<endl;
  //cout<< hsum  ->GetBinContent(2) <<endl;
  hData1->Divide(hsum);
  //cout<< hData1->GetBinContent(2) <<endl;
  hData1->GetYaxis()->SetRangeUser(0.,2.);
  hData1->SetMarkerStyle(20);
  hData1->SetMarkerSize(0.5);
  hData1->SetMarkerColor(kBlack);
  hData1->SetLineColor(kBlack);
  hData1->GetXaxis()->SetLabelSize(0.08);
  hData1->GetYaxis()->SetLabelSize(0.085);
  hData1->GetYaxis()->SetTitle("Data / MC");
  //hData1->GetXaxis()->SetTitle(Form("#DeltaR"));
  hData1->GetXaxis()->SetTitleSize(0.12);
  hData1->GetXaxis()->SetTitleOffset(0.8);
  hData1->GetYaxis()->SetTitleSize(0.12);
  hData1->GetYaxis()->SetTitleOffset(0.22);
  
  TLine *line = new TLine(0.1,0.187,0.9,0.187);
  line->SetLineStyle(8);
  //line->SetLineColor(kBlack);
  hData1->Draw("epx");
  cs->cd(0);
  cs->cd(2);
  line->Draw("same");
  
  //hData1->Draw("APsame");
  
  cs->cd();
  cs->SaveAs(outFile_name,"RECREATE");

  return hs;
}


/* Produce MC-only Stack Plots */
static THStack *stackPlotMConly(double Lum, vector<TH1F*> histos, const char *outFile_name, const char *axisName, const char *entriesUnit){ //Give a vector of histos and realise a stack plot from it
  int n = histos.size()+1; 
  THStack *hs = new THStack("hs","");
  //Artificial histo for labelling, etc...
  TH1 *hdummy = (TH1F*)histos.at(0)->Clone(histos.at(0)->GetName());
  hdummy->SetFillColor(10);
  hdummy->SetLineColor(10);
  hdummy->SetMarkerColor(10);
  //Give integrated luminosity as input!
  //double Lum = 36074.6;
  //clone input histos
  vector<TH1F*> new_histos; 
  //colours
  vector<int>   colours;    
  new_histos    .reserve( n );
  colours       .reserve( n );
  // Set colours for different MC samples
  colours.push_back(2);
  colours.push_back(4);
  colours.push_back(7);
  colours.push_back(11);
  colours.push_back(13);
  colours.push_back(3);
  colours.push_back(5);
  colours.push_back(46);
  colours.push_back(41);
  colours.push_back(8);
  colours.push_back(9);
  colours.push_back(12);
  
  for(int i=0; i<histos.size(); i++) {
    string name  = histos.at(i)->GetName();
    TH1F *hnew = (TH1F*)histos.at(i)->Clone(name.c_str());
    hnew->SetFillColor(colours.at(i));
    hnew->Scale(Lum);
    new_histos.push_back( hnew );
  }
 
  TCanvas *cs = new TCanvas(outFile_name);
  TLegend *leg = new TLegend(0.83,0.88,0.63,0.55);
  leg->SetTextSize(0.035);
  gStyle->SetLegendBorderSize(0);
  for(int i=0; i<histos.size(); i++) {
    new_histos.at(i)->SetFillColor(colours.at(i));
    new_histos.at(i)->SetLineColor(colours.at(i));
    new_histos.at(i)->SetMarkerColor(colours.at(i));
    string name  = getSampleName(histos.at(i)->GetName());
    TLegendEntry *entry = leg->AddEntry(new_histos.at(i),name.c_str(),"f"); 
    hs->Add(new_histos.at(i));
  }

  // Set Range //
  double max(1000);
  max = hs->GetMaximum()+0.2*hs->GetMaximum();//3*(hs->GetBinError(hs->GetMaximumBin()));
  //max = new_histos.at(0)->GetMaximum()+3*(new_histos.at(0)->GetBinError(new_histos.at(0)->GetMaximumBin()));
  //for(int i=1; i<new_histos.size();i++) {
  //  if( new_histos.at(i)->GetMaximum()+3*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin())>max ) max = new_histos.at(i)->GetMaximum()+3*new_histos.at(i)->GetBinError(new_histos.at(i)->GetMaximumBin());
  //}
  
  TString Xtitle = (string)axisName;
  TString Ytitle = string()+"Entries / %2.0f"+entriesUnit;
  hdummy->GetXaxis()->SetTitle(Form(Xtitle));
  hdummy->GetYaxis()->SetTitle(Form(Ytitle,new_histos.at(0)->GetBinWidth(1)));
  hdummy->GetYaxis()->SetTitleOffset(1.3);
  hdummy->GetYaxis()->SetRangeUser(0,max);
  gStyle->SetOptStat(0);
  hdummy->Draw("");
  hs->Draw("HISTsame");
  leg->Draw("same");
  
  cs->cd();
  cs->SaveAs(outFile_name,"RECREATE");

  return hs;
}


