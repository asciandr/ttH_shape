///////////////////////// -*- C++ -*- /////////////////////////////
// Header file
// Author: A.Sciandra<andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TROOT.h"
#include "TTree.h"
#include "TString.h"
#include "THStack.h"
#include <string.h>
#include <TLorentzVector.h>
#include <iostream>

#include "nominal.C"

using namespace std;

//function giving combinations of OS lepton pairs ordered in pT
static vector<vector<int>> OScombs(vector<int> *lepton_ID, vector<float> *lepton_pt) {
  vector<vector<int>> combs;
  //Identify OS lepton pairs
  int indices[3]={0};
  int charge[4]={0};
  for (unsigned int i=0; i<lepton_pt->size(); i++) {
    charge[i]=lepton_ID->at(i)/fabs(lepton_ID->at(i)); 
  }//for (unsigned int i=0; i<lepton_pt->size(); i++)  
  if (!((charge[0]+charge[1]==0)||(charge[0]+charge[2]==0))) cout<<"**** OScombs **** PLEASE CHECK ME ****"<<endl;
  indices[0]                   = (charge[0]+charge[1]==0) ? 1 : 2;
  if(indices[0]==1) indices[1] = (charge[0]+charge[2]==0) ? 2 : 3;
  else              indices[1] = 3;
  for(unsigned int i=1; i<lepton_pt->size(); i++) if(i!=indices[0]&&i!=indices[1]) indices[2] = i;
  //Pairs are 0 with indices[0] and indices[1]
  //and indices[2] with indices[0] and indices[1]
  //Ordering in pT included
  //COMB0
  int i0 = (lepton_pt->at(indices[2])>=lepton_pt->at(indices[1])) ? indices[2] : indices[1];
  int i1 = (lepton_pt->at(indices[2])<lepton_pt->at(indices[1]))  ? indices[2] : indices[1];
  int i2 = (lepton_pt->at(0)>=lepton_pt->at(indices[0])) ? 0 : indices[0];
  int i3 = (lepton_pt->at(0)<lepton_pt->at(indices[0]))  ? 0 : indices[0];
  vector<int> ind0;
  ind0.push_back(i0); ind0.push_back(i1); ind0.push_back(i2); ind0.push_back(i3);
  combs.push_back(ind0);
  //COMB1
  i0 = (lepton_pt->at(indices[2])>=lepton_pt->at(indices[0])) ? indices[2] : indices[0];
  i1 = (lepton_pt->at(indices[2])<lepton_pt->at(indices[0]))  ? indices[2] : indices[0];
  i2 = (lepton_pt->at(indices[1])>=lepton_pt->at(0)) ? indices[1] : 0;
  i3 = (lepton_pt->at(indices[1])<lepton_pt->at(0))  ? indices[1] : 0;
  vector<int> ind1;
  ind1.push_back(i0); ind1.push_back(i1); ind1.push_back(i2); ind1.push_back(i3);
  combs.push_back(ind1);
  //COMB2
  i0 = (lepton_pt->at(0)>=lepton_pt->at(indices[1])) ? 0 : indices[1];
  i1 = (lepton_pt->at(0)<lepton_pt->at(indices[1]))  ? 0 : indices[1];
  i2 = (lepton_pt->at(indices[2])>=lepton_pt->at(indices[0])) ? indices[2] : indices[0];
  i3 = (lepton_pt->at(indices[2])<lepton_pt->at(indices[0]))  ? indices[2] : indices[0];
  vector<int> ind2;
  ind2.push_back(i0); ind2.push_back(i1); ind2.push_back(i2); ind2.push_back(i3);
  combs.push_back(ind2);
  //COMB3
  i0 = (lepton_pt->at(0)>=lepton_pt->at(indices[0])) ? 0 : indices[0];
  i1 = (lepton_pt->at(0)<lepton_pt->at(indices[0]))  ? 0 : indices[0];
  i2 = (lepton_pt->at(indices[1])>=lepton_pt->at(indices[2])) ? indices[1] : indices[2];
  i3 = (lepton_pt->at(indices[1])<lepton_pt->at(indices[2]))  ? indices[1] : indices[2];
  vector<int> ind3;
  ind3.push_back(i0); ind3.push_back(i1); ind3.push_back(i2); ind3.push_back(i3);
  combs.push_back(ind3);

  return combs;
}

//Inv Mass computation
float invMass(TLorentzVector l1, TLorentzVector l2) {
  float mass(-99.);
  TLorentzVector l3 = l1+l2; 
   
  return l3.M();
}

//deltR computation
float deltaR(float eta1, float eta2, float phi1, float phi2) {
  float DEta = fabs(eta1 - eta2);
  float DPhi = acos(cos(fabs(phi1 - phi2)));
  return sqrt(pow(DEta, 2) + pow(DPhi, 2));
}

/* Fill Histos */
static THStack *fillHistos( nominal *nTree, double weight, TH1F* hm3l, TH1F* h_etmiss, TH1F* h_jets, TH1F* h_jet_pt, TH1F* h_l1_pt, TH1F* h_l2_pt, TH1F* h_l3_pt, TH1F* h_m2l_Z, TH1F *h_special_m2l ) {
  //3l invariant mass
  TLorentzVector l1;
  TLorentzVector l2;
  TLorentzVector l3;
  l1.SetPtEtaPhiE(nTree->lepton_pt->at(0),nTree->lepton_eta->at(0),nTree->lepton_phi->at(0),nTree->lepton_E->at(0));
  l2.SetPtEtaPhiE(nTree->lepton_pt->at(1),nTree->lepton_eta->at(1),nTree->lepton_phi->at(1),nTree->lepton_E->at(1));
  l3.SetPtEtaPhiE(nTree->lepton_pt->at(2),nTree->lepton_eta->at(2),nTree->lepton_phi->at(2),nTree->lepton_E->at(2));
  
  hm3l->Fill( (l1+l2+l3).M()/1000.,weight );
  h_etmiss->Fill( nTree->MET_RefFinal_et/1000.,weight );
  h_jets->Fill( nTree->m_jet_pt->size(),weight );
  for(int a=0; a<nTree->m_jet_pt->size(); a++) h_jet_pt->Fill( nTree->m_jet_pt->at(a)/1000.,weight );
  
  // Leptons' pT in pT order
  float pT[3] ;
  int   ind[3];
  for ( int i=0; i<3; i++ ) {
    pT[i] = -99.;
    ind[i]= -99.; 
  }
  //for ( int i=0; i<nTree->lepton_pt->size(); i++) cout<<"Initial leps : "<< nTree->lepton_pt->at(i) <<endl;
  //cout<<endl;
  for ( int i=1; i<nTree->lepton_pt->size(); i++) {
    if( i==1 ) {
      pT[0]  =  nTree->lepton_pt->at(0)/1000.;
      ind[0] = 0;
    }
    if (nTree->lepton_pt->at(i)/1000.>pT[0]) {
      pT[0]  = nTree->lepton_pt->at(i)/1000.;
      ind[0] = i;
    }
  }
  bool first(true);
  for ( int i=0; i<nTree->lepton_pt->size(); i++) {
    if( i!=ind[0]&&first ) {
      pT[1]  = nTree->lepton_pt->at(i)/1000.;
      ind[1] = i;
    }
    if( i!=ind[0]&&i!=ind[1]&&nTree->lepton_pt->at(i)/1000.>pT[1]&&!first ) {
      pT[1]  = nTree->lepton_pt->at(i)/1000.;
      ind[1] = i;
    }
    first = false;
  }
  for ( int i=0; i<nTree->lepton_pt->size(); i++) {
    if( i!=ind[0]&&i!=ind[1] ) {
      pT[2]  = nTree->lepton_pt->at(i)/1000.;
      ind[2] = i;
    }
  }
  //Don't want to order them in pT anymore...
  h_l1_pt->Fill( nTree->lepton_pt->at(0)/1000.,weight );
  h_l2_pt->Fill( nTree->lepton_pt->at(1)/1000.,weight );
  h_l3_pt->Fill( nTree->lepton_pt->at(2)/1000.,weight );
  //for(int i=0; i<3; i++) cout<<"Ordered leps: ind "<< ind[i] <<" and pT = "<< pT[i]<<endl;
  
  // Invariant mass of the 2l closer to Z-peak //
  // ! the first lepton is OS ! //
  int OSSF(0.);
  double M_Z(-999.);
  double diff1(-999.), diff2(-999.);
  for(int a=1; a<nTree->lepton_pt->size(); a++) {
    if((nTree->lepton_ID->at(0)+nTree->lepton_ID->at(a)==0)){
      OSSF ++;
      TLorentzVector l1, l2;
      double Mass(-99);
      l1.SetPtEtaPhiE(nTree->lepton_pt->at(0),nTree->lepton_eta->at(0),nTree->lepton_phi->at(0),nTree->lepton_E->at(0));
      l2.SetPtEtaPhiE(nTree->lepton_pt->at(a),nTree->lepton_eta->at(a),nTree->lepton_phi->at(a),nTree->lepton_E->at(a));
      Mass = (l1+l2).M();
      //cout<< nTree->lepton_ID->at(0) <<"  "<< nTree->lepton_ID->at(a) <<endl;
      //cout<< Mass <<endl;
      if(OSSF==1) {
	diff1 = abs(Mass/1000.-91.);
       	M_Z = Mass;
      }
      if(OSSF==2) diff2 = abs(Mass/1000.-91.);
      if((OSSF==2)&&(diff2<diff1)) M_Z = Mass;
    }
  }
  if(M_Z!=-999.) h_m2l_Z->Fill(M_Z/1000.,weight);
  
  //Special plot - OSSF l pair invariant mass for events with M_3l < 91 GeV
  if((l1+l2+l3).M()/1000.<91.) h_special_m2l->Fill( M_Z/1000.,weight );
    
  return 0;
}

