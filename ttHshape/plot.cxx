///////////////////////// -*- C++ -*- /////////////////////////////
// ttH->4l Shape Analysis
// Author: A.Sciandra <andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TChain.h"
#include "TFile.h"
#include "TRandom3.h"
#include <string.h>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <string>
#include <ctime>
#include <sstream>

#include "header.h"
#include "plot_shapes.h"

using namespace std      ;

int main() {
  clock_t begin       = clock();
  bool    plot        = true;
  bool    debug       = false;
  //Number of leps + 1
  int     n_leps(5); 
  const char* inFold  = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v27/01/";
  //const char* inFold  = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v26/02/";
  //const char* inFold  = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v24/02/";
  const char* outFold = "/afs/cern.ch/user/a/asciandr/www/ttH/reco_shapes/";
  TString fixCutLooseSR4l ="@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(lepton_isolationFixedCutLoose[0]==1&&lepton_isolationFixedCutLoose[1]==1&&lepton_isolationFixedCutLoose[2]==1&&lepton_isolationFixedCutLoose[3]==1)&&(RunYear==2015||RunYear==2016)&&(((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0)||(lepton_ID[0]+lepton_ID[3]==0)||(lepton_ID[1]+lepton_ID[2]==0)||(lepton_ID[1]+lepton_ID[3]==0)||(lepton_ID[2]+lepton_ID[3]==0)))";
  //TString fixCutLooseSR4l ="@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(lepton_isolationFixedCutLoose[0]==1&&lepton_isolationFixedCutLoose[1]==1&&lepton_isolationFixedCutLoose[2]==1&&lepton_isolationFixedCutLoose[3]==1)&&(RunYear==2015||RunYear==2016)&&(!((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0)||(lepton_ID[0]+lepton_ID[3]==0)||(lepton_ID[1]+lepton_ID[2]==0)||(lepton_ID[1]+lepton_ID[3]==0)||(lepton_ID[2]+lepton_ID[3]==0)))";
   
  const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root","rare.root","Zjets_NNPDF.root","Wjets_NNPDF.root","ttbar_3lep.root","diboson_bfilter.root","ttW_aMcNloP8.root","singletop.root"};
  //const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root"};
  //const char*      files[] = {"ttH_aMcNloP8.root"};

  float Lum_tot = 36074.6;
  //float Lum_tot = 36470.2;
  float Lum(1.);
  Lum = Lum_tot       ;
  cout<<endl;
  cout<<"--------------> Running at an integrated luminosity of: "<< Lum/1000 <<" fb^(-1): "<<endl;
  int   NMAX =                (sizeof(files) / sizeof(files[0])) + 4.;
  vector<float>               Signal;
  vector<float>               Back  ;
  Signal                      .reserve(NMAX);
  Back                        .reserve(NMAX);
  cout<<endl;
  cout<<"   ///////////////////////// -*- C++ -*- /////////////////////////////"<<endl;
  cout<<"  // ttH->4l Shape Analysis                                        //"<<endl;
  cout<<" // Author: A.Sciandra <andrea.sciandra@cern.ch>                  //"<<endl;
  cout<<"///////////////////////////////////////////////////////////////////"<<endl;
  cout<<endl;
  if ( debug )      cout<<"!!    DEBUG mode                !!"<<endl;
  if ( plot  )      cout<<"!!    Plot option activated     !!"<<endl;
  cout<<endl;
  cout<<"** Reserving capacity() = "<< Signal.capacity() <<" for meaningful vectors       **"<<endl;
  cout<<endl;

  vector<TH1F*>     v_hN;
  v_hN              .reserve(NMAX);

  cout<<endl;
  cout<<"** Opening root Input Files                                             **"<<endl;
  cout<<endl;
  cout<<"** Looping on the following samples:                                    **"<<endl;
  cout<<endl;
  vector<TFile*>              inputFile;
  vector<TTree*>              v_fChain;
  inputFile                   .reserve( NMAX );
  v_fChain                    .reserve( NMAX );
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TString     string_i  = string()+inFold+files[i]   ;
    TTree       *chain;
    inputFile   .push_back(new TFile(string_i));
    inputFile   .at(i)->GetObject("nominal",chain);
    v_fChain    .push_back( chain );

    cout<< inputFile.at(i)->GetName() <<endl;
  }
  TString PreSelection = fixCutLooseSR4l;
  
  cout    <<endl;
  cout    <<"********                    Applying the following PRESELECTION for Skimming:                   ********"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"''       "<< PreSelection <<"       ''"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"**                                           Skimming root files                                         **"<<endl;

  vector<TTree*>     v_fChainSkim;
  vector<nominal*>   nTree       ;
  v_fChainSkim       .reserve( NMAX );
  nTree              .reserve( NMAX );

  /* OUTPUT FILES */
  TFile *f = new TFile("shapes.root","RECREATE");
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    v_fChainSkim .push_back( (TTree*)v_fChain.at(i)->CopyTree( PreSelection ) );
    nTree        .push_back( new nominal(v_fChainSkim.at(i))               );
  }
  clock_t end1 = clock();
  double  elapsed_secs = double(end1 - begin) / CLOCKS_PER_SEC;
  cout    <<endl;

  string histName3 = string()+"htotMC";
  TH1F   *hMC  = new TH1F(histName3.c_str(),"",11, -0.5, 10.5);
  hMC->Sumw2();

  /* Plotting */
  vector<TH1F*> v_mindRlb, v_mindRlj;
  v_mindRlb.reserve(NMAX); v_mindRlj.reserve(NMAX);
  vector<TH1F*> v_orig_mindRlb, v_orig_mindRlj;
  v_orig_mindRlb.reserve(NMAX); v_orig_mindRlj.reserve(NMAX);
  vector<TH1F*> v_otherOrig_dRlj_min;
  v_otherOrig_dRlj_min.reserve(NMAX);
  vector<TH1F*> v_dR01, v_dR23, v_phi23, v_eta23;
  v_dR01.reserve(NMAX); v_dR23.reserve(NMAX); v_phi23.reserve(NMAX); v_eta23.reserve(NMAX);
  vector<TH1F*> v_dR23_rest, v_closB_pt;
  v_dR23_rest.reserve(NMAX); v_closB_pt.reserve(NMAX);
  vector<TH1F*> v_pp, v_Ml2l3, v_Ml2l3met; v_pp.reserve(NMAX); v_Ml2l3.reserve(NMAX); v_Ml2l3met.reserve(NMAX);

  for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {
    cout<<endl;
    cout<<"****                    Processing "<< inputFile.at(t)->GetName() <<"                    ****"<<endl;
    cout<<endl;

    string histName  = string()+"hN_"+inputFile.at(t)->GetName();
    TH1F   *hN    = new TH1F(histName.c_str() ,"", 11, -0.5, 10.5);
    hN     ->Sumw2();

    string hist_dRlb_min = string()+"hist_dRlb_min_"+inputFile.at(t)->GetName();
    TH1F   *h_dRlb_min = new TH1F(hist_dRlb_min.c_str(),"", 60, -1.5, 4.5);
    h_dRlb_min->Sumw2();

    string hist_orig_dRlb_min = string()+"hist_orig_dRlb_min_"+inputFile.at(t)->GetName();
    TH1F   *h_orig_dRlb_min = new TH1F(hist_orig_dRlb_min.c_str(),"", 46, -0.5, 45.5);
    h_orig_dRlb_min->Sumw2();

    string hist_dRlj_min = string()+"hist_dRlj_min_"+inputFile.at(t)->GetName();
    TH1F   *h_dRlj_min = new TH1F(hist_dRlj_min.c_str(),"", 60, -1.5, 4.5);
    h_dRlj_min->Sumw2();

    string hist_orig_dRlj_min = string()+"hist_orig_dRlj_min_"+inputFile.at(t)->GetName();
    TH1F   *h_orig_dRlj_min = new TH1F(hist_orig_dRlj_min.c_str(),"", 46, -0.5, 45.5);
    h_orig_dRlj_min->Sumw2();

    string hist_otherdRlj_min = string()+"hist_otherdRlj_min_"+inputFile.at(t)->GetName();
    TH1F   *h_otherdRlj_min = new TH1F(hist_otherdRlj_min.c_str(),"", 60, -1.5, 4.5);
    h_otherdRlj_min->Sumw2();

    string hist_otherOrig_dRlj_min = string()+"hist_otherOrig_dRlj_min_"+inputFile.at(t)->GetName();
    TH1F   *h_otherOrig_dRlj_min = new TH1F(hist_otherOrig_dRlj_min.c_str(),"", 46, -0.5, 45.5);
    h_otherOrig_dRlj_min->Sumw2();

    string hist_dR01 = string()+"hist_dR01_"+inputFile.at(t)->GetName();
    TH1F   *h_dR01 = new TH1F(hist_dR01.c_str(),"", 12, -1.5, 4.5);
    h_dR01->Sumw2();

    string hist_dR23 = string()+"hist_dR23_"+inputFile.at(t)->GetName();
    TH1F   *h_dR23 = new TH1F(hist_dR23.c_str(),"", 12, -1.5, 4.5);
    h_dR23->Sumw2();

    string hist_dR23_rest = string()+"hist_dR23_rest_"+inputFile.at(t)->GetName();
    TH1F   *h_dR23_rest = new TH1F(hist_dR23_rest.c_str(),"", 32, -1.5, 6.5);
    h_dR23_rest->Sumw2();

    string hist_phi23 = string()+"hist_phi23_"+inputFile.at(t)->GetName();
    TH1F   *h_phi23 = new TH1F(hist_phi23.c_str(),"", 12, -1.5, 4.5);
    h_phi23->Sumw2();

    string hist_eta23 = string()+"hist_eta23_"+inputFile.at(t)->GetName();
    TH1F   *h_eta23 = new TH1F(hist_eta23.c_str(),"", 12, -1.5, 4.5);
    h_eta23->Sumw2();

    string hist_closB_pt = string()+"hist_closB_pt_"+inputFile.at(t)->GetName();
    TH1F   *h_closB_pt = new TH1F(hist_closB_pt.c_str(),"",100,20.,220.); 
    h_closB_pt->Sumw2();

    string hist_pp = string()+"hist_pp_"+inputFile.at(t)->GetName();
    TH1F   *h_pp = new TH1F(hist_pp.c_str(),"",25,0.,500.);
    h_pp->Sumw2();

    string hist_Ml2l3 = string()+"hist_Ml2l3_"+inputFile.at(t)->GetName();
    TH1F   *h_Ml2l3 = new TH1F(hist_Ml2l3.c_str(),"",50,0.,500.);
    h_Ml2l3->Sumw2();

    string hist_Ml2l3met = string()+"hist_Ml2l3met_"+inputFile.at(t)->GetName();
    TH1F   *h_Ml2l3met = new TH1F(hist_Ml2l3met.c_str(),"",14,0.,700.);
    h_Ml2l3met->Sumw2();

    Long64_t nentries = v_fChainSkim.at(t)->GetEntriesFast();
    /* Looping over the events */
    for (Long64_t ievt=0; ievt<nentries;ievt++) {
    //for (Long64_t ievt=0; ievt<1;ievt++) {
      int trash=nTree.at(t)->GetEntry(ievt);
      const double weight = nTree.at(t)->mcWeightOrg*nTree.at(t)->xsec*(1./nTree.at(t)->totweight)*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->MV2c10_70_EventWeight;
      /* BOOLEANS */
      bool goodOne(false);
      const int njets = nTree.at(t)->m_jet_pt->size();
      int nbjets(0.);
      vector<int> v_bjetIndex; v_bjetIndex.reserve(20);
      goodOne=true;//further selection on top of skimming?
      float mindRlj(99.), maxdRlj(-99.);
      int orig_mindRlj(-999.);
      /* min dR(lep,b-tagged jet) */
      float mindRlb(99.), maxdRlb(-99.);
      int orig_mindRlb(-999.), lepInd_mindRlb(-999.);
      if ( goodOne ) {
        /* loop over all jets */
	for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++) {
          for(unsigned int j=0; j<njets; j++) {
            float dR_lj(0.);
            dR_lj=deltaR(double(nTree.at(t)->lepton_eta->at(l)),double(nTree.at(t)->m_jet_eta->at(j)),double(nTree.at(t)->lepton_phi->at(l)),double(nTree.at(t)->m_jet_phi->at(j)));
            if (dR_lj<mindRlj) { mindRlj=dR_lj; orig_mindRlj=nTree.at(t)->lepton_truthOrigin->at(l); }
            if (dR_lj>maxdRlj) maxdRlj=dR_lj;
          }
        }
        /* loop over all jets */

        /* Define b-tagged jets */
        for(unsigned int i=0; i<nTree.at(t)->m_jet_pt->size(); i++) {
          if (nTree.at(t)->m_jet_flavor_weight_MV2c10->at(i)>0.8244) {
            nbjets++;
            v_bjetIndex.push_back(i);
          }
        }
        /* Define b-tagged jets */
        /* loop over b-tagged jets */
	int closest_b(-99.);
	for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++) {
          for(unsigned int j=0; j<v_bjetIndex.size(); j++) {
	    float dR_lj(0.);
	    dR_lj=deltaR(double(nTree.at(t)->lepton_eta->at(l)),double(nTree.at(t)->m_jet_eta->at(v_bjetIndex[j])),double(nTree.at(t)->lepton_phi->at(l)),double(nTree.at(t)->m_jet_phi->at(v_bjetIndex[j])));
            if (dR_lj<mindRlb) { closest_b=v_bjetIndex[j]; mindRlb=dR_lj; orig_mindRlb=nTree.at(t)->lepton_truthOrigin->at(l); lepInd_mindRlb=l; }
            if (dR_lj>maxdRlb) maxdRlb=dR_lj;
	  }//for(unsigned int j=0; j<v_bjetIndex.size(); j++)
        }//for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++)
        /* loop over b-tagged jets */
        h_dRlb_min->Fill(mindRlb,weight);
        h_orig_dRlb_min->Fill(orig_mindRlb,weight);
        h_dRlj_min->Fill(mindRlj,weight);
        h_orig_dRlj_min->Fill(orig_mindRlj,weight);
        /* OS lepton closest to another jet */
        float otherMindRlj(99.), otherMaxdRlj(-99.);
        int otherOrig_mindRlj(-999.), otherLepInd_mindRlb(-999.); 
	for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++) {
	  if ( (l==lepInd_mindRlb) || (abs(nTree.at(t)->lepton_ID->at(lepInd_mindRlb))/nTree.at(t)->lepton_ID->at(lepInd_mindRlb) + abs(nTree.at(t)->lepton_ID->at(l))/nTree.at(t)->lepton_ID->at(l) != 0) ) continue;
	  for(unsigned int j=0; j<njets; j++) {
            float dR_lj(0.);
            dR_lj=deltaR(double(nTree.at(t)->lepton_eta->at(l)),double(nTree.at(t)->m_jet_eta->at(j)),double(nTree.at(t)->lepton_phi->at(l)),double(nTree.at(t)->m_jet_phi->at(j)));
            if (dR_lj<otherMindRlj) { otherMindRlj=dR_lj; otherOrig_mindRlj=nTree.at(t)->lepton_truthOrigin->at(l); otherLepInd_mindRlb=l; }
            if (dR_lj>otherMaxdRlj) otherMaxdRlj=dR_lj;
          }  
        } //for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++)
        /* OS lepton closest to another jet */
        //dR01
	float dR01(-99.);
	dR01=deltaR(double(nTree.at(t)->lepton_eta->at(lepInd_mindRlb)),double(nTree.at(t)->lepton_eta->at(otherLepInd_mindRlb)),double(nTree.at(t)->lepton_phi->at(lepInd_mindRlb)),double(nTree.at(t)->lepton_phi->at(otherLepInd_mindRlb)));        
        if(debug) { cout<<"Lep0: "<< lepInd_mindRlb <<" Lep1: "<< otherLepInd_mindRlb <<endl; cout<<"ID1: "<< nTree.at(t)->lepton_ID->at(lepInd_mindRlb) <<" ID2: "<< nTree.at(t)->lepton_ID->at(otherLepInd_mindRlb) <<endl; }
	//dR23
 	float dR23(-99.), phi23(-99.), eta23(-99.);	
  	int lep[2], count(0.);	
	for(unsigned int l=0; l<nTree.at(t)->lepton_pt->size(); l++) { if( l!=lepInd_mindRlb&&l!=otherLepInd_mindRlb ) { lep[count]=l; count++; } }
	dR23=deltaR(double(nTree.at(t)->lepton_eta->at(lep[0])),double(nTree.at(t)->lepton_eta->at(lep[1])),double(nTree.at(t)->lepton_phi->at(lep[0])),double(nTree.at(t)->lepton_phi->at(lep[1])));
	phi23=abs(nTree.at(t)->lepton_phi->at(lep[0])-nTree.at(t)->lepton_phi->at(lep[1]));
	eta23=abs(nTree.at(t)->lepton_eta->at(lep[0])-nTree.at(t)->lepton_eta->at(lep[1]));
	if(debug) { cout<<"Lep2: "<< lep[0] <<" lep3: "<< lep[1] <<endl; }

        h_otherdRlj_min->Fill(otherMindRlj,weight);
        h_otherOrig_dRlj_min->Fill(otherOrig_mindRlj,weight);
        h_dR01->Fill(dR01,weight);
        h_dR23->Fill(dR23,weight);
        h_phi23->Fill(phi23,weight);
        h_eta23->Fill(eta23,weight);

	TLorentzVector l2, l3, lTOT;
        l2.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep[0])),double(nTree.at(t)->lepton_eta->at(lep[0])),double(nTree.at(t)->lepton_phi->at(lep[0])),double(nTree.at(t)->lepton_E->at(lep[0])));
        l3.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep[1])),double(nTree.at(t)->lepton_eta->at(lep[1])),double(nTree.at(t)->lepton_phi->at(lep[1])),double(nTree.at(t)->lepton_E->at(lep[1])));
	lTOT=l2+l3;
	//if(debug) cout<<"BEFORE - px: "<< lTOT.Px() <<" py: "<< lTOT.Py() <<" pz: "<< lTOT.Pz() <<endl;
	//if(debug) cout<<"BEFORE - sqrt(px^2+py^2) "<< sqrt(lTOT.Px()*lTOT.Px()+lTOT.Py()*lTOT.Py()) <<" pt "<< lTOT.Pt() <<endl;

        //Project MET on l2+l3	
        Double_t pp;
        TVector3 v3_lTOT=lTOT.Vect();
        TLorentzVector lMET;
        lMET.SetPtEtaPhiE(double(nTree.at(t)->MET_RefFinal_et),0,double(nTree.at(t)->MET_RefFinal_phi),double(nTree.at(t)->MET_RefFinal_et));
        pp=v3_lTOT.Perp(lMET.Vect());
        h_pp->Fill(pp/1000.,weight);
        h_Ml2l3met->Fill((l2+l3+lMET).M()/1000.,weight);
        h_Ml2l3->Fill((l2+l3).M()/1000.,weight);

	//Transform to the mother particle rest frame
	//BOOST VECTOR TO REST FRAME
	TVector3 b=lTOT.BoostVector();
   	//lTOT.Boost(-b);		
	//if(debug) cout<<"AFTER - px: "<< lTOT.Px() <<" py: "<< lTOT.Py() <<" pz: "<< lTOT.Pz() <<endl;
	l2.Boost(-b);
	l3.Boost(-b);
	float dR23_rest=l2.DeltaR(l3);
        h_dR23_rest->Fill(dR23_rest,weight);
        //h_dR23_rest->Fill(abs(l2.Phi()-l3.Phi()));
	TLorentzVector l_closestB;
	l_closestB.SetPtEtaPhiE(double(nTree.at(t)->m_jet_pt->at(closest_b)),double(nTree.at(t)->m_jet_eta->at(closest_b)),double(nTree.at(t)->m_jet_phi->at(closest_b)),double(nTree.at(t)->m_jet_E->at(closest_b)));	
	l_closestB.Boost(-b);
	h_closB_pt->Fill(l_closestB.Pt()/1000.,weight);
	//Transform to the mother particle rest frame
	
      }//if ( goodOne )
    }//for (Long64_t ievt=0; ievt<nentries;ievt++)
    v_mindRlb.push_back(h_dRlb_min);
    v_orig_mindRlb.push_back(h_orig_dRlb_min);
    v_mindRlj.push_back(h_dRlj_min);
    v_orig_mindRlj.push_back(h_orig_dRlj_min);
    //v_mindRlj.push_back(h_otherdRlj_min);
    v_otherOrig_dRlj_min.push_back(h_otherOrig_dRlj_min);
    v_dR01.push_back(h_dR01);
    v_dR23.push_back(h_dR23);
    v_phi23.push_back(h_phi23);
    v_eta23.push_back(h_eta23);
    v_dR23_rest.push_back(h_dR23_rest);
    v_closB_pt.push_back(h_closB_pt);
    v_pp.push_back(h_pp);
    v_Ml2l3.push_back(h_Ml2l3);
    v_Ml2l3met.push_back(h_Ml2l3met);
  }//for(int t=0; t<sizeof(files) / sizeof(files[0]); t++)

  if (plot) {
    TString pdfTitle = string() + outFold + "mindR_LepbJ.pdf";
    TH1F *mindRlb = new TH1F("mindRlb","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    mindRlb = PlotMC(Lum,v_mindRlb,pdfTitle,"min dR(l , b-tag jet) "," ");
    //THStack *mindRlb = new THStack("mindRlb","");
    //mindRlb = stackPlot(v_mindRlb,pdfTitle,"min dR(l , b-tag jet) "," ");

    TString pdfTitle1 = string() + outFold + "origin_mindR_LepbJ.pdf";
    TH1F *orig_mindRlb = new TH1F("orig_mindRlb","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    orig_mindRlb = PlotMC(Lum,v_orig_mindRlb,pdfTitle1,"origin min dR(l , b-tag jet) "," ");

    TString pdfTitle2 = string() + outFold + "mindR_LepJ.pdf";
    TH1F *mindRlj = new TH1F("mindRlj","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    mindRlj = PlotMC(Lum,v_mindRlj,pdfTitle2,"min dR(l , jet) "," ");

    TString pdfTitle3 = string() + outFold + "origin_mindR_LepJ.pdf";
    TH1F *orig_mindRlj = new TH1F("orig_mindRlj","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    orig_mindRlj = PlotMC(Lum,v_orig_mindRlj,pdfTitle3,"origin min dR(l , jet) "," ");
  
    TString pdfTitle4 = string() + outFold + "otherOrigin_mindR_LepJ.pdf";
    TH1F *otherOrig_mindRlj = new TH1F("otherOrig_mindRlj","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    otherOrig_mindRlj = PlotMC(Lum,v_otherOrig_dRlj_min,pdfTitle4,"origin of other OS min dR(l , jet) "," ");

    TString pdfTitle5 = string() + outFold + "dR01.pdf";
    TH1F *dR01 = new TH1F("dR01","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    dR01 = PlotMC(Lum,v_dR01,pdfTitle5,"dR01"," ");

    TString pdfTitle6 = string() + outFold + "dR23.pdf";
    TH1F *dR23 = new TH1F("dR23","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    dR23 = PlotMC(Lum,v_dR23,pdfTitle6,"dR23"," ");

    TString pdfTitle7 = string() + outFold + "phi23.pdf";
    TH1F *phi23 = new TH1F("phi23","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    phi23 = PlotMC(Lum,v_phi23,pdfTitle7,"phi23"," ");

    TString pdfTitle8 = string() + outFold + "eta23.pdf";
    TH1F *eta23 = new TH1F("eta23","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    eta23 = PlotMC(Lum,v_eta23,pdfTitle8,"eta23"," ");

    TString pdfTitle9 = string() + outFold + "rest_dR23.pdf";
    TH1F *dR23_rest = new TH1F("dR23_rest","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    dR23_rest = PlotMC(Lum,v_dR23_rest,pdfTitle9,"dR23 in rest frame"," ");

    TString pdfTitle10 = string() + outFold + "rest_BjetPt.pdf";
    TH1F *closB_pt = new TH1F("closB_pt","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    closB_pt =  PlotMC(Lum,v_closB_pt,pdfTitle10,"b-jet pT in rest frame"," ");

    TString pdfTitle11 = string() + outFold + "projMETl2l3.pdf";
    TH1F *l2l3pp = new TH1F("l2l3pp","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    l2l3pp =  PlotMC(Lum,v_pp,pdfTitle11,"MET Perp on l2+l3"," ");

    TString pdfTitle12 = string() + outFold + "Ml2l3lMET.pdf";
    TH1F *Ml2l3met = new TH1F("Ml2l3met","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    Ml2l3met =  PlotMC(Lum,v_Ml2l3met,pdfTitle12,"Ml2l3met [GeV]"," [GeV]");

    TString pdfTitle13 = string() + outFold + "Ml2l3l.pdf";
    TH1F *Ml2l3 = new TH1F("Ml2l3","",v_mindRlb.at(0)->GetNbinsX(),v_mindRlb.at(0)->GetBin(1),v_mindRlb.at(0)->GetBin(v_mindRlb.at(0)->GetNbinsX()));
    Ml2l3 =  PlotMC(Lum,v_Ml2l3,pdfTitle13,"Ml2l3 [GeV]"," [GeV]");

  }

}//int main()
