///////////////////////// -*- C++ -*- /////////////////////////////
// ttH->4l Shape Analysis
// Author: A.Sciandra <andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TChain.h"
#include "TFile.h"
#include "TRandom3.h"
#include <string.h>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <string>
#include <ctime>
#include <sstream>

#include "header.h"
#include "plot_shapes.h"

using namespace std      ;

//int main() {
void produce_templates()
{ 
  clock_t begin       = clock();
  //Booleans
  bool    plot        = true;  //Decide whether plotting or not
  bool    debug       = false;//true; //Debug option
  //Number of leps + 1
  int     n_leps(5);
  //Input folder
  const char* inFold  = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v27/01/";
  //const char* inFold  = "/afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/BDT/v27_looser_v27_Ml2l3_ntuples/";
  //Output folder for plots
  //const char* outFold = "/afs/cern.ch/user/a/asciandr/www/ttH/try_shapes/";   

  /* SR definition */
  TString looser4l = "@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(RunYear==2015||RunYear==2016)";

  //Root files for looping
  const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root"};
  //const char*      files[] = {"BDT_ttH_aMcNloP8.root","BDT_ttZ_aMcNloP8.root"};

  /* Set Luminosity (inv pb) and reserve enough space for meaningful vectors */
  float Lum(1.);
  //v26 Luminosity
  //Lum = 36470.2;
  //v27 Luminosity
  Lum = 36074.6;
  cout<<endl;
  cout<<"--------------> Running at an integrated luminosity of: "<< Lum/1000 <<" fb^(-1): "<<endl;
  int   NMAX =                (sizeof(files) / sizeof(files[0])) + 4.;
  vector<float>               Signal;
  vector<float>               Back  ;
  /* Reserve Enough Space for the Vectors */ 
  Signal  	              .reserve(NMAX);
  Back    	              .reserve(NMAX);
  /* Printing */
  cout<<endl;
  cout<<"   ///////////////////////// -*- C++ -*- /////////////////////////////"<<endl;
  cout<<"  // ttH->4l Shape Analysis                                        //"<<endl;
  cout<<" // Author: A.Sciandra <andrea.sciandra@cern.ch>                  //"<<endl;
  cout<<"///////////////////////////////////////////////////////////////////"<<endl;
  cout<<endl;
  if ( debug )      cout<<"!!    DEBUG mode                !!"<<endl;
  if ( plot  )      cout<<"!!    Plot option activated     !!"<<endl;
  cout<<endl;
  cout<<"** Reserving capacity() = "<< Signal.capacity() <<" for meaningful vectors       **"<<endl;
  cout<<endl;

  // Defining histos for event counting
  vector<TH1F*>     v_hN;
  v_hN              .reserve(NMAX);
  // Open the files
  cout<<endl;
  cout<<"** Opening root Input Files                                             **"<<endl;
  cout<<endl;
  cout<<"** Looping on the following samples:                                    **"<<endl;
  cout<<endl;
  vector<TFile*>              inputFile;
  vector<TTree*>              v_fChain;
  inputFile                   .reserve( NMAX );
  v_fChain                    .reserve( NMAX );
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TString     string_i  = string()+inFold+files[i]   ;
    //TString     string_i  = string()+files[i]   ;
    TTree       *chain;
    inputFile   .push_back(new TFile(string_i));
    inputFile   .at(i)->GetObject("nominal",chain); 
    v_fChain    .push_back( chain );
    
    cout<< inputFile.at(i)->GetName() <<endl;
  }
  TFile *fdummy = new TFile("dummy_out.root","RECREATE");
  /* selection for skimming */
  TString PreSelection = looser4l;
  cout    <<endl;
  cout    <<"********           	 Applying the following PRESELECTION for Skimming:                   ********"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"''       "<< PreSelection <<"       ''"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"**                       	                  Skimming root files        	      	      	      	      **"<<endl;
  // Skimmed TTreeS
  vector<TTree*>     v_fChainSkim;
  vector<nominal*>   nTree       ;  
  v_fChainSkim       .reserve( NMAX );
  nTree              .reserve( NMAX );

  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    v_fChainSkim .push_back( (TTree*)v_fChain.at(i)->CopyTree( PreSelection ) );
    nTree        .push_back( new nominal(v_fChainSkim.at(i))               );
  } 
  
  clock_t end1 = clock();
  double  elapsed_secs = double(end1 - begin) / CLOCKS_PER_SEC;
  cout    <<endl;
  cout    <<"**        Skimming complete... Time elapsed: "<< elapsed_secs <<" s   	  **"<<endl;
  cout    <<endl;
  cout    <<"**                                     Skimmed     TTree contains:                                  **"<<endl;
  cout    <<endl;
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) cout<< inputFile.at(i)->GetName() <<"  ->  "<< v_fChainSkim.at(i)->GetEntriesFast() <<"  *RAW* events        **"<<endl;
  cout    <<endl;

  //Summing up all MC yields
  string histName3 = string()+"htotMC";
  TH1F   *hMC  = new TH1F(histName3.c_str(),"",11, -0.5, 10.5);
  hMC->Sumw2();
  /* Plotting */
  //vector<TH1F*> v_etmiss, v_nbjets, v_njets, v_M4l, v_pt0, v_pt1, v_pt2, v_pt3, v_Ml2l3met, v_Ml2met, v_Ml3met, v_Mll01, v_Mll23, v_dR01, v_dR23, v_dRlb_min, v_dRlb_max, v_dPhi23, v_dRlj_min, v_Ml0l1met, v_mlbtop, v_mlbV;
  //vector<TH1F*> v_DPhillfromtop, v_DPhillfromtopV, v_DPhillfromV, v_DPhilbfromtop, v_DPhilbfromVtop;
  //v_etmiss.reserve(NMAX); v_Ml2met.reserve(NMAX); v_Ml3met.reserve(NMAX); v_Mll01.reserve(NMAX); v_Mll23.reserve(NMAX); v_dR01.reserve(NMAX), v_dR23.reserve(NMAX);
  //v_pt0.reserve(NMAX); v_pt1.reserve(NMAX); v_pt2.reserve(NMAX); v_pt3.reserve(NMAX);
  //v_M4l.reserve(NMAX); v_njets.reserve(NMAX); v_nbjets.reserve(NMAX);
  //v_Ml2l3met.reserve(NMAX); v_dPhi23.reserve(NMAX);
  //v_dRlj_min.reserve(NMAX); v_Ml0l1met.reserve(NMAX);
  //v_mlbtop.reserve(NMAX); v_mlbV.reserve(NMAX);
  //v_DPhillfromtop.reserve(NMAX); v_DPhillfromtopV.reserve(NMAX); v_DPhillfromV.reserve(NMAX);
  //v_DPhilbfromtop.reserve(NMAX); v_DPhilbfromVtop.reserve(NMAX);
  /* Plotting */
  /* Loop over the different samples */
  for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {
    if ( debug ) {
      cout<<endl;
      cout<<"****                    Processing "<< inputFile.at(t)->GetName() <<"                    ****"<<endl;
      cout<<endl;
    }
    
    /* OUTPUT FILES */
    string out_name="", ending="";
    if(t==0)            { out_name=string()+"shapesfile_4lep_v27_ttH.root"; ending="ttH"; }
    else if(t==1)       { out_name=string()+"shapesfile_4lep_v27_ttZ.root"; ending="bkg"; }
    else                cout<<"WHAT ARE YOU DOING?? CHECK ME!"<<endl;
    TFile *fout = new TFile(out_name.c_str(),"RECREATE");

    /* Defining histos for total yields */
    string histName  = string()+"hN_"+ending;
    TH1F   *hN    = new TH1F(histName.c_str() ,"", 11, -0.5, 10.5);
    hN     ->Sumw2();

    /*******************/
    /**** PLOTTING *****/
    /*******************/

    string hist_etmiss = string()+"fourLep_etmiss_"+ending;
    TH1F   *h_etmiss = new TH1F(hist_etmiss.c_str(),"", 20, 0., 300.);
    h_etmiss->Sumw2();

    string hist_dRlb_min = string()+"fourLep_dRlb_min_"+ending;
    TH1F   *h_dRlb_min = new TH1F(hist_dRlb_min.c_str(),"", 60, -1.5, 4.5);
    h_dRlb_min->Sumw2();

    string hist_dRlb_max = string()+"fourLep_dRlb_max_"+ending;
    TH1F   *h_dRlb_max = new TH1F(hist_dRlb_max.c_str(),"", 80, -1.5, 6.5);
    h_dRlb_max->Sumw2();

    string hist_dRlj_min = string()+"fourLep_dRlj_min_"+ending;
    TH1F   *h_dRlj_min = new TH1F(hist_dRlj_min.c_str(),"", 60, -1.5, 4.5);
    h_dRlj_min->Sumw2();

    string hist_nbjets = string()+"fourLep_nbjets_"+ending;
    TH1F   *h_nbjets = new TH1F(hist_nbjets.c_str(),"", 15, -0.5, 14.5);
    h_nbjets->Sumw2();

    string hist_njets = string()+"fourLep_njets_"+ending;
    TH1F   *h_njets = new TH1F(hist_njets.c_str(),"", 15, -0.5, 14.5);
    h_njets->Sumw2();

    string hist_M4l = string()+"fourLep_M4l_"+ending;
    TH1F   *h_M4l = new TH1F(hist_M4l.c_str(),"", 20, 0., 700.);
    h_M4l->Sumw2();

    string hist_pt0 = string()+"fourLep_pt0_"+ending;
    TH1F   *h_pt0 = new TH1F(hist_pt0.c_str(),"", 20, 0., 600.);
    h_pt0->Sumw2();

    string hist_pt1 = string()+"fourLep_pt1_"+ending;
    TH1F   *h_pt1 = new TH1F(hist_pt1.c_str(),"", 20, 0., 600.);
    h_pt1->Sumw2();

    string hist_pt2 = string()+"fourLep_pt2_"+ending;
    TH1F   *h_pt2 = new TH1F(hist_pt2.c_str(),"", 20, 0., 600.);
    h_pt2->Sumw2();

    string hist_pt3 = string()+"fourLep_pt3_"+ending;
    TH1F   *h_pt3 = new TH1F(hist_pt3.c_str(),"", 20, 0., 600.);
    h_pt3->Sumw2();

    string hist_Ml0l1met = string()+"fourLep_Ml0l1met_"+ending;
    TH1F   *h_Ml0l1met = new TH1F(hist_Ml0l1met.c_str(),"", 16, -100., 700.);
    h_Ml0l1met->Sumw2();

    string hist_Ml2l3met = string()+"fourLep_Ml2l3met_"+ending;
    TH1F   *h_Ml2l3met = new TH1F(hist_Ml2l3met.c_str(),"", 14, 0., 700.);
    h_Ml2l3met->Sumw2();

    string hist_Ml2met = string()+"fourLep_Ml2met_"+ending;
    TH1F   *h_Ml2met = new TH1F(hist_Ml2met.c_str(),"", 11, -10., 400.);
    h_Ml2met->Sumw2();

    string hist_Ml3met = string()+"fourLep_Ml3met_"+ending;
    TH1F   *h_Ml3met = new TH1F(hist_Ml3met.c_str(),"", 11, -10., 400.);
    h_Ml3met->Sumw2();
  
    string hist_dPhi23 = string()+"fourLep_dPhi23_"+ending;
    TH1F   *h_dPhi23 = new TH1F(hist_dPhi23.c_str(),"", 16, -4, 4);
    h_dPhi23->Sumw2();

    string hist_Mll01 = string()+"fourLep_Mll01_"+ending;
    TH1F   *h_Mll01 = new TH1F(hist_Mll01.c_str(),"", 20, 0., 400.); 
    h_Mll01->Sumw2();

    string hist_Mll23 = string()+"fourLep_Mll23_"+ending;
    TH1F   *h_Mll23 = new TH1F(hist_Mll23.c_str(),"", 60, 0., 800.); 
    h_Mll23->Sumw2();

    string hist_dR01 = string()+"fourLep_dR01_"+ending;
    TH1F   *h_dR01 = new TH1F(hist_dR01.c_str(),"", 10, 0., 5.);
    h_dR01->Sumw2();

    string hist_dR23 = string()+"fourLep_dR23_"+ending;
    TH1F   *h_dR23 = new TH1F(hist_dR23.c_str(),"", 10, 0., 5.);
    h_dR23->Sumw2();

    string hist_mlbtop = string()+"fourLep_mlbtop_"+ending;
    TH1F   *h_mlbtop = new TH1F(hist_mlbtop.c_str(),"",20,0.,600.);
    h_mlbtop->Sumw2();

    string hist_mlbVtop = string()+"fourLep_mlbVtop_"+ending;
    TH1F   *h_mlbVtop = new TH1F(hist_mlbVtop.c_str(),"",20,0.,600.);
    h_mlbVtop->Sumw2();

    string hist_DPhilbfromtop = string()+"fourLep_DPhilbfromtop_"+ending;
    TH1F   *h_DPhilbfromtop = new TH1F(hist_DPhilbfromtop.c_str(),"",24,-4.,4.);
    h_DPhilbfromtop->Sumw2();

    string hist_DPhilbfromVtop = string()+"fourLep_DPhilbfromVtop_"+ending;
    TH1F   *h_DPhilbfromVtop = new TH1F(hist_DPhilbfromVtop.c_str(),"",24,-4.,4.);
    h_DPhilbfromVtop->Sumw2();

    string hist_DPhillfromtop = string()+"fourLep_DPhillfromtop_"+ending;
    TH1F   *h_DPhillfromtop = new TH1F(hist_DPhillfromtop.c_str(),"",24,-4.,4.);
    h_DPhillfromtop->Sumw2();

    string hist_DPhillfromtopV = string()+"fourLep_DPhillfromtopV_"+ending;
    TH1F   *h_DPhillfromtopV = new TH1F(hist_DPhillfromtopV.c_str(),"",24,-4.,4.);
    h_DPhillfromtopV->Sumw2();

    string hist_DPhillfromV = string()+"fourLep_DPhillfromV_"+ending;
    TH1F   *h_DPhillfromV = new TH1F(hist_DPhillfromV.c_str(),"",24,-4.,4.);
    h_DPhillfromV->Sumw2();

    //////////////////////////////////////////////////////////////

    Long64_t nentries = v_fChainSkim.at(t)->GetEntriesFast();
    /* Looping over the events */
    for (Long64_t ievt=0; ievt<nentries; ievt++) {
    //for (Long64_t ievt=0; ievt<10; ievt++) 
      int trash=nTree.at(t)->GetEntry(ievt);
      /* weight each event in the proper way */
      const double weight = nTree.at(t)->mcWeightOrg*nTree.at(t)->xsec*(1./nTree.at(t)->totweight)*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->MV2c10_70_EventWeight;
      //const double weight = nTree.at(t)->mcWeightOrg*nTree.at(t)->xsec*(1./nTree.at(t)->totweight)*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight;
      /* BOOLEANS */
      bool goodOne(false);
      bool isTTH(false), isTTZ(false);
      int isH(0.), isT(0.), isZ(0.); 
      const int ind_size=4;
      int lep_index[ind_size] = {99,99,99,99}; //First 2 from t and others from H/Z
      const int njets = nTree.at(t)->m_jet_pt->size();
      int nbjets(0.);
      vector<int> v_bjetIndex; v_bjetIndex.reserve(20);
      //const int bjet_index
      /* Defining ttH and ttZ main signatures */
      if (debug) cout<<"Event n. "<< ievt <<endl;
      if ((((string)inputFile.at(t)->GetName()).find("ttH_aMcNloP8"))!=string::npos) {
      //if ((((string)inputFile.at(t)->GetName()).find("ttH"))!=string::npos) {
	//bool topo(false);
	for(unsigned int i=0; i<nTree.at(t)->lepton_pt->size(); i++) {
	  if(nTree.at(t)->lepton_truthOrigin->at(i)==10) {
	    isT ++;
            lep_index[isT-1]=i;
            if (debug) cout<<"lep_index["<< isT-1 <<"]= "<< lep_index[isT-1] <<endl;
	  }
	  if(nTree.at(t)->lepton_truthOrigin->at(i)==14) {
	    isH ++;
	    lep_index[isH+1]=i;
            if (debug) cout<<"lep_index["<< isH+1 <<"]= "<< lep_index[isH+1] <<endl;
	  }
	}
	if( isH==2 && isT==2 ) isTTH=true;
	//goodOne = isTTH;
      }
      if ((((string)inputFile.at(t)->GetName()).find("ttZ_aMcNloP8"))!=string::npos) {
      //if ((((string)inputFile.at(t)->GetName()).find("ttZ"))!=string::npos) {
	//bool topo(false);
	for(unsigned int i=0; i<nTree.at(t)->lepton_pt->size(); i++) {
	  if(debug) { cout<<"TRUTH ORIGIN in TTZ: "<< nTree.at(t)->lepton_truthOrigin->at(i) <<endl;  }
	  if(nTree.at(t)->lepton_truthOrigin->at(i)==10) {
	    isT ++;
	    lep_index[isT-1]=i;
          }
	  if(nTree.at(t)->lepton_truthOrigin->at(i)==13) {
       	    isZ ++;
	    lep_index[isZ+1]=i;
          }
	}
	if( isZ==2 && isT==2 ) isTTZ=true;
	//goodOne = isTTZ;
      }
      goodOne = isTTH || isTTZ;
      /* Defining ttH and ttZ main signatures */
      /* Define b-jets */
      if ( goodOne ) {
        for(unsigned int i=0; i<nTree.at(t)->m_jet_pt->size(); i++) {
	  if (nTree.at(t)->m_jet_flavor_truth_label->at(i)==5) {
	    nbjets++;
	    v_bjetIndex.push_back(i);
	  }
	}
        /* Leptons from t and b-jets */
 	//TLorentzVector l00, l11;
	//l00.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[0])),double(nTree.at(t)->lepton_eta->at(lep_index[0])),double(nTree.at(t)->lepton_phi->at(lep_index[0])),double(nTree.at(t)->lepton_E->at(lep_index[0])));
        //l11.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[1])),double(nTree.at(t)->lepton_eta->at(lep_index[1])),double(nTree.at(t)->lepton_phi->at(lep_index[1])),double(nTree.at(t)->lepton_E->at(lep_index[1])));
        float dRlb_min(-1.), dRlb_max(-1.);
        for(unsigned int i=0; i<nbjets; i++) {
	  //TLorentzVector lbj;
      	  //lbj.SetPtEtaPhiE();    
      	  float dR0 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[0]),nTree.at(t)->m_jet_eta->at(v_bjetIndex[i]),nTree.at(t)->lepton_phi->at(lep_index[0]),nTree.at(t)->m_jet_phi->at(v_bjetIndex[i]));
      	  float dR1 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[1]),nTree.at(t)->m_jet_eta->at(v_bjetIndex[i]),nTree.at(t)->lepton_phi->at(lep_index[1]),nTree.at(t)->m_jet_phi->at(v_bjetIndex[i]));
      	  dRlb_min = (dR0<dR1) ? dR0 : dR1;
          h_dRlb_min->Fill(dRlb_min,weight);
      	  dRlb_max = (dR0>dR1) ? dR0 : dR1;
          h_dRlb_max->Fill(dRlb_max,weight);
        }
        if (nbjets==0) {
	  h_dRlb_min->Fill(dRlb_min,weight);
  	  h_dRlb_max->Fill(dRlb_max,weight);
        }
      
        float dRlj_min(99.);
        for(unsigned int j=0; j<nTree.at(t)->m_jet_pt->size(); j++) {
          float dR0 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[0]),nTree.at(t)->m_jet_eta->at(j),nTree.at(t)->lepton_phi->at(lep_index[0]),nTree.at(t)->m_jet_phi->at(j));
          float dR1 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[1]),nTree.at(t)->m_jet_eta->at(j),nTree.at(t)->lepton_phi->at(lep_index[1]),nTree.at(t)->m_jet_phi->at(j));
          float dRmin = (dR0<dR1) ? dR0 : dR1;	
          if(dRmin<dRlj_min) dRlj_min=dRmin; 
        }
        if(debug) cout<<"dRlj_min= "<< dRlj_min <<endl;
        h_dRlj_min->Fill(dRlj_min,weight);
      }      

      /* Define b-jets */
      if (debug&&isTTH) cout<<"Main signatures defined for ttH"<<endl;
      if (debug&&isTTZ) cout<<"Main signatures defined for ttZ"<<endl;
      ////////////////////////////////////////////////////////
      /* Exploit main signatures for lepton-jet association */
      ////////////////////////////////////////////////////////
      if ( goodOne ) {
	//ttH & ttZ
	if ((((string)inputFile.at(t)->GetName()).find("ttH"))!=string::npos||(((string)inputFile.at(t)->GetName()).find("ttZ"))!=string::npos) {
          if (debug) {
	    cout<<"New event"<<endl; 
	    cout<<"lep_index size= "<< ind_size <<endl;
            for(unsigned int i=0; i<ind_size; i++) cout<<"lep_index["<< i <<"]= "<< lep_index[i] <<" origin = "<< nTree.at(t)->lepton_truthOrigin->at(lep_index[i]) <<" pT: "<< nTree.at(t)->lepton_pt->at(lep_index[i]) <<endl;
	  }
	  if (debug) { 
	    for(unsigned int i=0; i<nTree.at(t)->lepton_pt->size(); i++) cout<<"index= "<< lep_index[i] <<endl;
          } 
	  //lep_index filled above -> 0,1 from t and 2,3 from H
	  //TLorentzVectors definition
	  TLorentzVector l0, l1, l2, l3, lmet;
          lmet.SetPtEtaPhiM(double(nTree.at(t)->MET_RefFinal_et),0,double(nTree.at(t)->MET_RefFinal_phi),0);
          l0.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[0])),double(nTree.at(t)->lepton_eta->at(lep_index[0])),double(nTree.at(t)->lepton_phi->at(lep_index[0])),double(nTree.at(t)->lepton_E->at(lep_index[0])));
          l1.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[1])),double(nTree.at(t)->lepton_eta->at(lep_index[1])),double(nTree.at(t)->lepton_phi->at(lep_index[1])),double(nTree.at(t)->lepton_E->at(lep_index[1])));
          l2.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[2])),double(nTree.at(t)->lepton_eta->at(lep_index[2])),double(nTree.at(t)->lepton_phi->at(lep_index[2])),double(nTree.at(t)->lepton_E->at(lep_index[2])));
          l3.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[3])),double(nTree.at(t)->lepton_eta->at(lep_index[3])),double(nTree.at(t)->lepton_phi->at(lep_index[3])),double(nTree.at(t)->lepton_E->at(lep_index[3])));
          float Mll01 = invMass(l0,l1);
          float Mll23 = invMass(l2,l3);
          float Ml0l1met = (l0+l1+lmet).M();
	  h_Mll01->Fill(Mll01/1000.,weight);
	  h_Mll23->Fill(Mll23/1000.,weight);
          h_Ml0l1met->Fill(Ml0l1met/1000.,weight);
          float Ml2met = invMass(l2,lmet);
          float Ml3met = invMass(l3,lmet);
          float Ml2l3met = (l2+l3+lmet).M();
          h_Ml2met->Fill(Ml2met/1000.,weight);
          h_Ml3met->Fill(Ml3met/1000.,weight);
 	  h_Ml2l3met->Fill(Ml2l3met/1000.,weight);
	  //dR01, dR23;	
	  float dR01 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[0]),nTree.at(t)->lepton_eta->at(lep_index[1]),nTree.at(t)->lepton_phi->at(lep_index[0]),nTree.at(t)->lepton_phi->at(lep_index[1]));
          h_dR01->Fill(dR01,weight);
	  float dR23 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[2]),nTree.at(t)->lepton_eta->at(lep_index[3]),nTree.at(t)->lepton_phi->at(lep_index[2]),nTree.at(t)->lepton_phi->at(lep_index[3]));
	  h_dR23->Fill(dR23,weight);
          float dPhi23 = l2.DeltaPhi(l3);
	  h_dPhi23->Fill(dPhi23,weight);
	  
	}	
      }
      ////////////////////////////////////////////////////////
      /* Exploit main signatures for lepton-jet association */
      ////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////
      /* NEW: templates of M(l,b) and DPhi(l,l) with l either from t or V  */
      //////////////////////////////////////////////////////////////////////
      //// truth b-jets have indices: vector<int> v_bjetIndex 		//// 
      //// top leptons indices: lep_index[0], lep_index[1]    		//// 
      //// V leptons indices: lep_index[2], lep_index[3]      		////
      //// Try already available templates: mlbfromtop and mlbfromVtop  ////
      for(unsigned int i=0; i<nTree.at(t)->mlbfromtop->size(); i++)  	h_mlbtop->Fill(nTree.at(t)->mlbfromtop->at(i)/1000.,weight);
      for(unsigned int i=0; i<nTree.at(t)->mlbfromVtop->size(); i++) 	h_mlbVtop->Fill(nTree.at(t)->mlbfromVtop->at(i)/1000.,weight);
      for(unsigned int i=0; i<nTree.at(t)->DPhillfromtop->size(); i++)  h_DPhillfromtop->Fill(nTree.at(t)->DPhillfromtop->at(i),weight);
      for(unsigned int i=0; i<nTree.at(t)->DPhillfromV->size(); i++) 	h_DPhillfromV->Fill(nTree.at(t)->DPhillfromV->at(i),weight);
      for(unsigned int i=0; i<nTree.at(t)->DPhillfromtopV->size(); i++) h_DPhillfromtopV->Fill(nTree.at(t)->DPhillfromtopV->at(i),weight);
      for(unsigned int i=0; i<nTree.at(t)->DPhilbfromtop->size(); i++)  h_DPhilbfromtop->Fill(nTree.at(t)->DPhilbfromtop->at(i),weight);
      for(unsigned int i=0; i<nTree.at(t)->DPhilbfromVtop->size(); i++) h_DPhilbfromVtop->Fill(nTree.at(t)->DPhilbfromVtop->at(i),weight);
      //////////////////////////////////////////////////////////
      /* NEW: templates of M(l,b) with l either from t or V  */
      ////////////////////////////////////////////////////////

      if( goodOne ) {
	hN               ->Fill(nTree.at(t)->lepton_pt->size(),weight);
	h_etmiss	 ->Fill(nTree.at(t)->MET_RefFinal_et/1000.,weight);
        h_nbjets         ->Fill(nbjets,weight);
        h_njets          ->Fill(njets,weight);
        h_M4l            ->Fill(nTree.at(t)->Mllll0123/1000.,weight);
        h_pt0            ->Fill(nTree.at(t)->lepton_pt->at(lep_index[0])/1000.,weight);
        h_pt1            ->Fill(nTree.at(t)->lepton_pt->at(lep_index[1])/1000.,weight);
        h_pt2            ->Fill(nTree.at(t)->lepton_pt->at(lep_index[2])/1000.,weight);
        h_pt3            ->Fill(nTree.at(t)->lepton_pt->at(lep_index[3])/1000.,weight);
      }//if( goodOne )
      
    }//for (Long64_t ievt=0; ievt<nentries;ievt++) 
 
    v_hN    .push_back( hN    );
    //MC yields
    if( (((string)inputFile.at(t)->GetName()).find("data"))==string::npos ) {
      cout<<"Number of events in "<< inputFile.at(t)->GetName() <<" events passing cuts at L="<< Lum/1000 <<" fb^(-1): "<< Lum*( v_hN.at(t)->GetBinContent(n_leps) ) <<" +- "<< Lum*( v_hN.at(t)->GetBinError(n_leps) ) <<" in VR"<<endl;
    }
    h_etmiss->Scale(1/h_etmiss->Integral()); 	 			//v_etmiss  .push_back(h_etmiss);
    h_nbjets->Scale(1/h_nbjets->Integral()); 	 			//v_nbjets  .push_back(h_nbjets);
    h_njets->Scale(1/h_njets->Integral());   	 			//v_njets   .push_back(h_njets);
    h_M4l->Scale(1/h_M4l->Integral());       	 			//v_M4l     .push_back(h_M4l);
    h_pt0->Scale(1/h_pt0->Integral());       	 			//v_pt0     .push_back(h_pt0);
    h_pt1->Scale(1/h_pt1->Integral());       	 			//v_pt1     .push_back(h_pt1);
    h_pt2->Scale(1/h_pt2->Integral());       	 			//v_pt2     .push_back(h_pt2);
    h_pt3->Scale(1/h_pt3->Integral());       	 			//v_pt3     .push_back(h_pt3);
    h_Ml0l1met->Scale(1/h_Ml0l1met->Integral()); 			//v_Ml0l1met.push_back(h_Ml0l1met);
    h_Ml2l3met->Scale(1/h_Ml2l3met->Integral()); 			//v_Ml2l3met.push_back(h_Ml2l3met);
    h_Ml2met->Scale(1/h_Ml2met->Integral());     			//v_Ml2met  .push_back(h_Ml2met);
    h_Ml3met->Scale(1/h_Ml3met->Integral()); 	 			//v_Ml3met  .push_back(h_Ml3met);
    h_Mll01->Scale(1/h_Mll01->Integral()); 	 			//v_Mll01   .push_back(h_Mll01);
    h_Mll23->Scale(1/h_Mll23->Integral()); 	 			//v_Mll23   .push_back(h_Mll23);
    h_dR01->Scale(1/h_dR01->Integral()); 	 			//v_dR01    .push_back(h_dR01);
    h_dR23->Scale(1/h_dR23->Integral()); 	 			//v_dR23    .push_back(h_dR23);
    h_dRlb_min->Scale(1/h_dRlb_min->Integral()); 			//v_dRlb_min.push_back(h_dRlb_min);
    h_dRlj_min->Scale(1/h_dRlj_min->Integral()); 			//v_dRlj_min.push_back(h_dRlj_min);
    h_dRlb_max->Scale(1/h_dRlb_max->Integral()); 			//v_dRlb_max.push_back(h_dRlb_max);
    h_dPhi23->Scale(1/h_dPhi23->Integral()); 	 			//v_dPhi23  .push_back(h_dPhi23);
    h_mlbtop->Scale(1/h_mlbtop->Integral());	 			//v_mlbtop  .push_back(h_mlbtop);
    h_mlbVtop->Scale(1/h_mlbVtop->Integral());	 			//v_mlbV    .push_back(h_mlbVtop);
    h_DPhillfromtop->Scale(1/h_DPhillfromtop->Integral()); 		//v_DPhillfromtop.push_back(h_DPhillfromtop);
    h_DPhillfromtopV->Scale(1/h_DPhillfromtopV->Integral()); 		//v_DPhillfromtopV.push_back(h_DPhillfromtopV);
    h_DPhillfromV->Scale(1/h_DPhillfromV->Integral()); 			//v_DPhillfromV.push_back(h_DPhillfromV);
    h_DPhilbfromtop->Scale(1/h_DPhilbfromtop->Integral());		//v_DPhilbfromtop.push_back(h_DPhilbfromtop);
    h_DPhilbfromVtop->Scale(1/h_DPhilbfromVtop->Integral());		//v_DPhilbfromVtop.push_back(h_DPhilbfromVtop);
    
    /* SAVE TEMPLATES INTO OUTPUT FILES */
    //fdummy->Add(h_etmiss);
    //fout->Add(h_etmiss);
    fout->Write();
    //fout->Close();
    cout<<"Histograms written for "<< ending <<endl;
  } //for(int t=0; t<sizeof(files) / sizeof(files[0]); t++)
  cout<<"Templates are ready!"<<endl;
  //fdummy->Add(v_etmiss[0]);
  //fdummy->Write();
  //delete Signal; delete Back; delete v_hN; delete inputFile; delete v_fChain; delete v_fChainSkim; delete nTree;  
  //delete inputFile;
  //delete fdummy;

}//void 
