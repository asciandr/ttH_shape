///////////////////////// -*- C++ -*- /////////////////////////////
// ttH->4l Shape Analysis
// Author: A.Sciandra <andrea.sciandra@cern.ch>
///////////////////////////////////////////////////////////////////

#include "TChain.h"
#include "TFile.h"
#include "TRandom3.h"
#include <string.h>
#include <vector>
#include <stdio.h>
#include <cstring>
#include <string>
#include <ctime>
#include <sstream>

#include "header.h"
#include "plot_shapes.h"

using namespace std      ;

int main() {
  clock_t begin       = clock();
  //Booleans
  bool    plot        = true;  //Decide whether plotting or not
  bool    debug       = false;//true; //Debug option
  bool    evaluation  = true;
  //Number of leps + 1
  int     n_leps(5);
  //Input folder
  //const char* inFold  = "/afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/BDT/v27_looser_v27_Ml2l3_ntuples/";
  //const char* inFold  = "/afs/cern.ch/user/a/asciandr/work/ttH/ttH_shape/BDT/v27_Ml2l3_ntuples/";
  const char* inFold  = "/eos/atlas/user/a/asciandr/ttHmultilepton_samples/v27/01/";
  //Output folder for plots
  const char* outFold = "/afs/cern.ch/user/a/asciandr/www/ttH/truth_shapes/";   
  //const char* outFold = "./plots";

  vector<TFile*> v_outFiles; v_outFiles.reserve(10);

  /* SR definition */
  //TString SR4l ="@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(lepton_isolationGradient[0]==1&&lepton_isolationGradient[1]==1&&lepton_isolationGradient[2]==1&&lepton_isolationGradient[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(RunYear==2015||RunYear==2016)";
  //TString SR4l ="@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(lepton_isolationGradient[0]==1&&lepton_isolationGradient[1]==1&&lepton_isolationGradient[2]==1&&lepton_isolationGradient[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(RunYear==2015||RunYear==2016)";
  //TString fixCutLooseSR4l ="@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(lepton_isolationFixedCutLoose[0]==1&&lepton_isolationFixedCutLoose[1]==1&&lepton_isolationFixedCutLoose[2]==1&&lepton_isolationFixedCutLoose[3]==1)&&(RunYear==2015||RunYear==2016)";
  //TString looser4l = "@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(RunYear==2015||RunYear==2016)";
  //TString PLI4l = "@lepton_pt.size()==4&&((abs(lepton_ID[0])/lepton_ID[0])+(abs(lepton_ID[1])/lepton_ID[1])+(abs(lepton_ID[2])/lepton_ID[2])+(abs(lepton_ID[3])/lepton_ID[3])==0)&&(lepton_isTrigMatch[0]==1||lepton_isTrigMatch[1]==1||lepton_isTrigMatch[2]==1||lepton_isTrigMatch[3]==1)&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.)<12.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.)<12.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.)<12.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.)<12.)))&&(!((lepton_ID[0]+lepton_ID[1]==0)*(abs(Mll01/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[2]==0)*(abs(Mll02/1000.-91.2)<10.)) && !((lepton_ID[0]+lepton_ID[3]==0)*(abs(Mll03/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[2]==0)*(abs(Mll12/1000.-91.2)<10.)) && !((lepton_ID[1]+lepton_ID[3]==0)*(abs(Mll13/1000.-91.2)<10.)) && !((lepton_ID[2]+lepton_ID[3]==0)*(abs(Mll23/1000.-91.2)<10.)))&&(@m_jet_pt.size()>=2)&&(Sum$(m_jet_flavor_weight_MV2c10>0.8244)>=1)&&(RunYear==2015||RunYear==2016)&&(lep_promptLeptonIso_TagWeight_2<-0.5&&lep_promptLeptonIso_TagWeight_3<-0.5)&&((lepton_ID[0]+lepton_ID[1]==0)||(lepton_ID[0]+lepton_ID[2]==0)||(lepton_ID[0]+lepton_ID[3]==0)||(lepton_ID[1]+lepton_ID[2]==0)||(lepton_ID[1]+lepton_ID[3]==0)||(lepton_ID[2]+lepton_ID[3]==0))";
  TString Zenriched = "quadlep_type&&(lep_isTrigMatch_0||lep_isTrigMatch_1||lep_isTrigMatch_2||lep_isTrigMatch_3||lep_isTrigMatchDLT_0||lep_isTrigMatchDLT_1||lep_isTrigMatchDLT_2||lep_isTrigMatchDLT_3)&&!total_charge&&(lep_ID_0!=-lep_ID_1||Mll01>12e3&&abs(Mll01-91.2e3)>10e3)&&(lep_ID_0!=-lep_ID_2||Mll02>12e3&&abs(Mll02-91.2e3)>10e3)&&(lep_ID_0!=-lep_ID_3||Mll03>12e3&&abs(Mll03-91.2e3)>10e3)&&(lep_ID_1!=-lep_ID_2||Mll12>12e3&&abs(Mll12-91.2e3)>10e3)&&(lep_ID_1!=-lep_ID_3||Mll13>12e3&&abs(Mll13-91.2e3)>10e3)&&(lep_ID_2!=-lep_ID_3||Mll23>12e3&&abs(Mll23-91.2e3)>10e3)&&nJets_OR>1&&nJets_OR_MV2c10_70&&abs(Mllll0123-125e3)>5e3&&(lep_promptLeptonIso_TagWeight_2<-0.5&&lep_promptLeptonIso_TagWeight_3<-0.5)&&(!(lep_ID_0+lep_ID_1)||!(lep_ID_0+lep_ID_2)||!(lep_ID_0+lep_ID_3)||!(lep_ID_1+lep_ID_2)||!(lep_ID_1+lep_ID_3)||!(lep_ID_2+lep_ID_3))";

  //Root files for looping
  //const char*      files[] = {"BDT_ttH_aMcNloP8.root","BDT_ttZ_aMcNloP8.root","BDT_ttW_aMcNloP8.root","BDT_ttbar_2lep_PP8.root","BDT_rare.root","BDT_diboson_bfilter.root"};
  //const char*      files[] = {"BDT_ttH_aMcNloP8.root","BDT_ttZ_aMcNloP8.root"};
  const char*      files[] = {"ttH_aMcNloP8.root","ttZ_aMcNloP8.root"};
  //const char*      files[] = {"BDT_ttH_aMcNloP8.root","BDT_ttZ_aMcNloP8.root","BDT_ttW_aMcNloP8.root","BDT_ttbar_3lep.root","BDT_rare.root","BDT_diboson_bfilter.root"};
  //const char*      files[] = {"BDT_ttH_aMcNloP8.root","BDT_ttZ_aMcNloP8.root","BDT_ttZ.root","BDT_ttH.root","BDT_diboson.root"};
  //const char*      files[] = {"BDT_ttH_aMcNloP8.root","BDT_ttZ_aMcNloP8.root","BDT_ttbar_2lep_PP8.root"};
  //const char*      files[] = {"BDT_ttH_aMcNloP8.root","BDT_ttZ_aMcNloP8.root","BDT_ttZ_Sherpa.root"};

  /* Set Luminosity (inv pb) and reserve enough space for meaningful vectors */
  //float Lum15 = 3212.956160 ;
  //float Lum16 = 18857.4 ; //v21 Int Lum
  //Lum = Lum15 + Lum16       ;
  //v26 Luminosity
  //Lum = 36470.2;
  float Lum(1.);
  //v27 Luminosity
  Lum = 36074.6;
  cout<<endl;
  cout<<"--------------> Running at an integrated luminosity of: "<< Lum/1000 <<" fb^(-1): "<<endl;
  int   NMAX =                (sizeof(files) / sizeof(files[0])) + 4.;
  vector<float>               Signal;
  vector<float>               Back  ;
  /* Reserve Enough Space for the Vectors */ 
  Signal  	              .reserve(NMAX);
  Back    	              .reserve(NMAX);
  /* Printing */
  cout<<endl;
  cout<<"   ///////////////////////// -*- C++ -*- /////////////////////////////"<<endl;
  cout<<"  // ttH->4l Shape Analysis                                        //"<<endl;
  cout<<" // Author: A.Sciandra <andrea.sciandra@cern.ch>                  //"<<endl;
  cout<<"///////////////////////////////////////////////////////////////////"<<endl;
  cout<<endl;
  if ( debug )      cout<<"!!    DEBUG mode                !!"<<endl;
  if ( plot  )      cout<<"!!    Plot option activated     !!"<<endl;
  cout<<endl;
  cout<<"** Reserving capacity() = "<< Signal.capacity() <<" for meaningful vectors       **"<<endl;
  cout<<endl;

  // Defining histos for event counting
  vector<TH1F*>     v_hN;
  v_hN              .reserve(NMAX);
  // Open the files
  cout<<endl;
  cout<<"** Opening root Input Files                                             **"<<endl;
  cout<<endl;
  cout<<"** Looping on the following samples:                                    **"<<endl;
  cout<<endl;
  vector<TFile*>              inputFile;
  vector<TTree*>              v_fChain;
  inputFile                   .reserve( NMAX );
  v_fChain                    .reserve( NMAX );
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    TString     string_i  = string()+inFold+files[i]   ;
    //TString     string_i  = string()+files[i]   ;
    TTree       *chain;
    inputFile   .push_back(new TFile(string_i));
    inputFile   .at(i)->GetObject("nominal",chain); 
    v_fChain    .push_back( chain );
    
    cout<< inputFile.at(i)->GetName() <<endl;
  }
  /* selection for skimming */
  TString PreSelection = Zenriched;
  //TString PreSelection = PLI4l;
  //TString PreSelection = looser4l;
  //TString PreSelection = fixCutLooseSR4l;
  //TString PreSelection = SR4l;
  cout    <<endl;
  cout    <<"********           	 Applying the following PRESELECTION for Skimming:                   ********"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"''       "<< PreSelection <<"       ''"<<endl;
  cout    <<endl;
  cout    <<endl;
  cout    <<"**                       	                  Skimming root files        	      	      	      	      **"<<endl;
  // Skimmed TTreeS
  vector<TTree*>     v_fChainSkim;
  vector<nominal*>   nTree       ;  
  v_fChainSkim       .reserve( NMAX );
  nTree              .reserve( NMAX );

  /* OUTPUT FILES */
  TFile *f = new TFile("shapes.root","RECREATE");
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) {
    v_fChainSkim .push_back( (TTree*)v_fChain.at(i)->CopyTree( PreSelection ) );
    nTree        .push_back( new nominal(v_fChainSkim.at(i))               );
  } 
  
  clock_t end1 = clock();
  double  elapsed_secs = double(end1 - begin) / CLOCKS_PER_SEC;
  cout    <<endl;
  cout    <<"**        Skimming complete... Time elapsed: "<< elapsed_secs <<" s   	  **"<<endl;
  cout    <<endl;
  cout    <<"**                                     Skimmed     TTree contains:                                  **"<<endl;
  cout    <<endl;
  for(int i=0; i<sizeof(files) / sizeof(files[0]); i++) cout<< inputFile.at(i)->GetName() <<"  ->  "<< v_fChainSkim.at(i)->GetEntriesFast() <<"  *RAW* events        **"<<endl;
  cout    <<endl;

  //Summing up all MC yields
  string histName3 = string()+"htotMC";
  TH1F   *hMC  = new TH1F(histName3.c_str(),"",11, -0.5, 10.5);
  hMC->Sumw2();
  /* Plotting */
  vector<TH1F*> v_etmiss, v_nbjets, v_njets, v_M4l, v_pt0, v_pt1, v_pt2, v_pt3, v_Ml2l3met, v_Ml2met, v_Ml3met, v_Mll01, v_Mll23, v_dR01, v_dR23, v_dRlb_min, v_dRlb_max, v_dPhi23, v_dRlj_min, v_Ml0l1met, v_mlbtop, v_mlbV;
  vector<TH1F*> v_DPhillfromtop, v_DPhillfromtopV, v_DPhillfromV, v_DPhilbfromtop, v_DPhilbfromVtop;
  v_etmiss.reserve(NMAX); v_Ml2met.reserve(NMAX); v_Ml3met.reserve(NMAX); v_Mll01.reserve(NMAX); v_Mll23.reserve(NMAX); v_dR01.reserve(NMAX), v_dR23.reserve(NMAX);
  v_pt0.reserve(NMAX); v_pt1.reserve(NMAX); v_pt2.reserve(NMAX); v_pt3.reserve(NMAX);
  v_M4l.reserve(NMAX); v_njets.reserve(NMAX); v_nbjets.reserve(NMAX);
  v_Ml2l3met.reserve(NMAX); v_dPhi23.reserve(NMAX);
  v_dRlj_min.reserve(NMAX); v_Ml0l1met.reserve(NMAX);
  v_mlbtop.reserve(NMAX); v_mlbV.reserve(NMAX);
  v_DPhillfromtop.reserve(NMAX); v_DPhillfromtopV.reserve(NMAX); v_DPhillfromV.reserve(NMAX);
  v_DPhilbfromtop.reserve(NMAX); v_DPhilbfromVtop.reserve(NMAX);
  /* Plotting */
  /* Loop over the different samples */
  for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {
    if ( debug ) {
      cout<<endl;
      cout<<"****                    Processing "<< inputFile.at(t)->GetName() <<"                    ****"<<endl;
      cout<<endl;
    }
    
    /* Defining histos for total yields */
    string histName  = string()+"hN_"+inputFile.at(t)->GetName();
    TH1F   *hN    = new TH1F(histName.c_str() ,"", 11, -0.5, 10.5);
    hN     ->Sumw2();

    /*******************/
    /**** PLOTTING *****/
    /*******************/

    string hist_etmiss = string()+"hist_etmiss_"+inputFile.at(t)->GetName();
    TH1F   *h_etmiss = new TH1F(hist_etmiss.c_str(),"", 20, 0., 300.);
    h_etmiss->Sumw2();

    string hist_dRlb_min = string()+"hist_dRlb_min_"+inputFile.at(t)->GetName();
    TH1F   *h_dRlb_min = new TH1F(hist_dRlb_min.c_str(),"", 60, -1.5, 4.5);
    h_dRlb_min->Sumw2();

    string hist_dRlb_max = string()+"hist_dRlb_max_"+inputFile.at(t)->GetName();
    TH1F   *h_dRlb_max = new TH1F(hist_dRlb_max.c_str(),"", 80, -1.5, 6.5);
    h_dRlb_max->Sumw2();

    string hist_dRlj_min = string()+"hist_dRlj_min_"+inputFile.at(t)->GetName();
    TH1F   *h_dRlj_min = new TH1F(hist_dRlj_min.c_str(),"", 60, -1.5, 4.5);
    h_dRlj_min->Sumw2();

    string hist_nbjets = string()+"hist_nbjets_"+inputFile.at(t)->GetName();
    TH1F   *h_nbjets = new TH1F(hist_nbjets.c_str(),"", 15, -0.5, 14.5);
    h_nbjets->Sumw2();

    string hist_njets = string()+"hist_njets_"+inputFile.at(t)->GetName();
    TH1F   *h_njets = new TH1F(hist_njets.c_str(),"", 15, -0.5, 14.5);
    h_njets->Sumw2();

    string hist_M4l = string()+"hist_M4l"+inputFile.at(t)->GetName();
    TH1F   *h_M4l = new TH1F(hist_M4l.c_str(),"", 20, 0., 700.);
    h_M4l->Sumw2();

    string hist_pt0 = string()+"hist_pt0"+inputFile.at(t)->GetName();
    TH1F   *h_pt0 = new TH1F(hist_pt0.c_str(),"", 20, 0., 600.);
    h_pt0->Sumw2();

    string hist_pt1 = string()+"hist_pt1_"+inputFile.at(t)->GetName();
    TH1F   *h_pt1 = new TH1F(hist_pt1.c_str(),"", 20, 0., 600.);
    h_pt1->Sumw2();

    string hist_pt2 = string()+"hist_pt2_"+inputFile.at(t)->GetName();
    TH1F   *h_pt2 = new TH1F(hist_pt2.c_str(),"", 20, 0., 600.);
    h_pt2->Sumw2();

    string hist_pt3 = string()+"hist_pt3_"+inputFile.at(t)->GetName();
    TH1F   *h_pt3 = new TH1F(hist_pt3.c_str(),"", 20, 0., 600.);
    h_pt3->Sumw2();

    string hist_Ml0l1met = string()+"hist_Ml0l1met"+inputFile.at(t)->GetName();
    TH1F   *h_Ml0l1met = new TH1F(hist_Ml0l1met.c_str(),"", 16, -100., 700.);
    h_Ml0l1met->Sumw2();

    string hist_Ml2l3met = string()+"hist_Ml2l3met"+inputFile.at(t)->GetName();
    TH1F   *h_Ml2l3met = new TH1F(hist_Ml2l3met.c_str(),"", 14, 0., 700.);
    h_Ml2l3met->Sumw2();

    string hist_Ml2met = string()+"hist_Ml2met_"+inputFile.at(t)->GetName();
    TH1F   *h_Ml2met = new TH1F(hist_Ml2met.c_str(),"", 11, -10., 400.);
    h_Ml2met->Sumw2();

    string hist_Ml3met = string()+"hist_Ml3met_"+inputFile.at(t)->GetName();
    TH1F   *h_Ml3met = new TH1F(hist_Ml3met.c_str(),"", 11, -10., 400.);
    h_Ml3met->Sumw2();
  
    string hist_dPhi23 = string()+"hist_dPhi23_"+inputFile.at(t)->GetName();
    TH1F   *h_dPhi23 = new TH1F(hist_dPhi23.c_str(),"", 16, -4, 4);
    h_dPhi23->Sumw2();

    string hist_Mll01 = string()+"hist_Mll01_"+inputFile.at(t)->GetName();
    TH1F   *h_Mll01 = new TH1F(hist_Mll01.c_str(),"", 20, 0., 400.); 
    h_Mll01->Sumw2();

    string hist_Mll23 = string()+"hist_Mll23_"+inputFile.at(t)->GetName();
    TH1F   *h_Mll23 = new TH1F(hist_Mll23.c_str(),"", 60, 0., 800.); 
    h_Mll23->Sumw2();

    string hist_dR01 = string()+"hist_dR01_"+inputFile.at(t)->GetName();
    TH1F   *h_dR01 = new TH1F(hist_dR01.c_str(),"", 10, 0., 5.);
    h_dR01->Sumw2();

    string hist_dR23 = string()+"hist_dR23_"+inputFile.at(t)->GetName();
    TH1F   *h_dR23 = new TH1F(hist_dR23.c_str(),"", 10, 0., 5.);
    h_dR23->Sumw2();

    string hist_mlbtop = string()+"hist_mlbtop_"+inputFile.at(t)->GetName();
    TH1F   *h_mlbtop = new TH1F(hist_mlbtop.c_str(),"",20,0.,600.);
    h_mlbtop->Sumw2();

    string hist_mlbVtop = string()+"hist_mlbVtop_"+inputFile.at(t)->GetName();
    TH1F   *h_mlbVtop = new TH1F(hist_mlbVtop.c_str(),"",20,0.,600.);
    h_mlbVtop->Sumw2();

    string hist_DPhilbfromtop = string()+"hist_DPhilbfromtop_"+inputFile.at(t)->GetName();
    TH1F   *h_DPhilbfromtop = new TH1F(hist_DPhilbfromtop.c_str(),"",24,-4.,4.);
    h_DPhilbfromtop->Sumw2();

    string hist_DPhilbfromVtop = string()+"hist_DPhilbfromVtop_"+inputFile.at(t)->GetName();
    TH1F   *h_DPhilbfromVtop = new TH1F(hist_DPhilbfromVtop.c_str(),"",24,-4.,4.);
    h_DPhilbfromVtop->Sumw2();

    string hist_DPhillfromtop = string()+"hist_DPhillfromtop_"+inputFile.at(t)->GetName();
    TH1F   *h_DPhillfromtop = new TH1F(hist_DPhillfromtop.c_str(),"",24,-4.,4.);
    h_DPhillfromtop->Sumw2();

    string hist_DPhillfromtopV = string()+"hist_DPhillfromtopV_"+inputFile.at(t)->GetName();
    TH1F   *h_DPhillfromtopV = new TH1F(hist_DPhillfromtopV.c_str(),"",24,-4.,4.);
    h_DPhillfromtopV->Sumw2();

    string hist_DPhillfromV = string()+"hist_DPhillfromV_"+inputFile.at(t)->GetName();
    TH1F   *h_DPhillfromV = new TH1F(hist_DPhillfromV.c_str(),"",24,-4.,4.);
    h_DPhillfromV->Sumw2();

    //////////////////////////////////////////////////////////////

    Long64_t nentries = v_fChainSkim.at(t)->GetEntriesFast();
    /* Looping over the events */
    for (Long64_t ievt=0; ievt<nentries; ievt++) {
    //for (Long64_t ievt=0; ievt<10; ievt++) {
      int trash=nTree.at(t)->GetEntry(ievt);
      /* weight each event in the proper way */
      const double weight = nTree.at(t)->mcWeightOrg*nTree.at(t)->xsec*(1./nTree.at(t)->totweight)*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->MV2c10_70_EventWeight;
      //const double weight = nTree.at(t)->mcWeightOrg*nTree.at(t)->xsec*(1./nTree.at(t)->totweight)*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight;
      /* BOOLEANS */
      bool goodOne(false);
      bool isTTH(false), isTTZ(false);
      int isH(0.), isT(0.), isZ(0.); 
      const int ind_size=4;
      int lep_index[ind_size] = {99,99,99,99}; //First 2 from t and others from H/Z
      const int njets = nTree.at(t)->m_jet_pt->size();
      int nbjets(0.);
      vector<int> v_bjetIndex; v_bjetIndex.reserve(20);
      //const int bjet_index
      /* Defining ttH and ttZ main signatures */
      if (debug) cout<<"Event n. "<< ievt <<endl;
      if ((((string)inputFile.at(t)->GetName()).find("ttH_aMcNloP8"))!=string::npos) {
      //if ((((string)inputFile.at(t)->GetName()).find("ttH"))!=string::npos) {
	//bool topo(false);
	for(unsigned int i=0; i<nTree.at(t)->lepton_pt->size(); i++) {
	  if(nTree.at(t)->lepton_truthOrigin->at(i)==10) {
	    isT ++;
            lep_index[isT-1]=i;
            if (debug) cout<<"lep_index["<< isT-1 <<"]= "<< lep_index[isT-1] <<endl;
	  }
	  if(nTree.at(t)->lepton_truthOrigin->at(i)==14) {
	    isH ++;
	    lep_index[isH+1]=i;
            if (debug) cout<<"lep_index["<< isH+1 <<"]= "<< lep_index[isH+1] <<endl;
	  }
	}
	if( isH==2 && isT==2 ) isTTH=true;
	//goodOne = isTTH;
      }
      if ((((string)inputFile.at(t)->GetName()).find("ttZ_aMcNloP8"))!=string::npos) {
      //if ((((string)inputFile.at(t)->GetName()).find("ttZ"))!=string::npos) {
	//bool topo(false);
	for(unsigned int i=0; i<nTree.at(t)->lepton_pt->size(); i++) {
	  if(debug) { cout<<"TRUTH ORIGIN in TTZ: "<< nTree.at(t)->lepton_truthOrigin->at(i) <<endl;  }
	  if(nTree.at(t)->lepton_truthOrigin->at(i)==10) {
	    isT ++;
	    lep_index[isT-1]=i;
          }
	  if(nTree.at(t)->lepton_truthOrigin->at(i)==13) {
       	    isZ ++;
	    lep_index[isZ+1]=i;
          }
	}
	if( isZ==2 && isT==2 ) isTTZ=true;
	//goodOne = isTTZ;
      }
      goodOne = isTTH || isTTZ;
      /* Defining ttH and ttZ main signatures */
      /* Define b-jets */
      if ( goodOne ) {
        for(unsigned int i=0; i<nTree.at(t)->m_jet_pt->size(); i++) {
	  if (nTree.at(t)->m_jet_flavor_truth_label->at(i)==5) {
	    nbjets++;
	    v_bjetIndex.push_back(i);
	  }
	}
        /* Leptons from t and b-jets */
 	//TLorentzVector l00, l11;
	//l00.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[0])),double(nTree.at(t)->lepton_eta->at(lep_index[0])),double(nTree.at(t)->lepton_phi->at(lep_index[0])),double(nTree.at(t)->lepton_E->at(lep_index[0])));
        //l11.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[1])),double(nTree.at(t)->lepton_eta->at(lep_index[1])),double(nTree.at(t)->lepton_phi->at(lep_index[1])),double(nTree.at(t)->lepton_E->at(lep_index[1])));
        float dRlb_min(-1.), dRlb_max(-1.);
        for(unsigned int i=0; i<nbjets; i++) {
	  //TLorentzVector lbj;
      	  //lbj.SetPtEtaPhiE();    
      	  float dR0 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[0]),nTree.at(t)->m_jet_eta->at(v_bjetIndex[i]),nTree.at(t)->lepton_phi->at(lep_index[0]),nTree.at(t)->m_jet_phi->at(v_bjetIndex[i]));
      	  float dR1 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[1]),nTree.at(t)->m_jet_eta->at(v_bjetIndex[i]),nTree.at(t)->lepton_phi->at(lep_index[1]),nTree.at(t)->m_jet_phi->at(v_bjetIndex[i]));
      	  dRlb_min = (dR0<dR1) ? dR0 : dR1;
          h_dRlb_min->Fill(dRlb_min,weight);
      	  dRlb_max = (dR0>dR1) ? dR0 : dR1;
          h_dRlb_max->Fill(dRlb_max,weight);
        }
        if (nbjets==0) {
	  h_dRlb_min->Fill(dRlb_min,weight);
  	  h_dRlb_max->Fill(dRlb_max,weight);
        }
      
        float dRlj_min(99.);
        for(unsigned int j=0; j<nTree.at(t)->m_jet_pt->size(); j++) {
          float dR0 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[0]),nTree.at(t)->m_jet_eta->at(j),nTree.at(t)->lepton_phi->at(lep_index[0]),nTree.at(t)->m_jet_phi->at(j));
          float dR1 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[1]),nTree.at(t)->m_jet_eta->at(j),nTree.at(t)->lepton_phi->at(lep_index[1]),nTree.at(t)->m_jet_phi->at(j));
          float dRmin = (dR0<dR1) ? dR0 : dR1;	
          if(dRmin<dRlj_min) dRlj_min=dRmin; 
        }
        if(debug) cout<<"dRlj_min= "<< dRlj_min <<endl;
        h_dRlj_min->Fill(dRlj_min,weight);
      }      

      /* Define b-jets */
      if (debug&&isTTH) cout<<"Main signatures defined for ttH"<<endl;
      if (debug&&isTTZ) cout<<"Main signatures defined for ttZ"<<endl;
      ////////////////////////////////////////////////////////
      /* Exploit main signatures for lepton-jet association */
      ////////////////////////////////////////////////////////
      if ( goodOne ) {
	//ttH & ttZ
	if ((((string)inputFile.at(t)->GetName()).find("ttH"))!=string::npos||(((string)inputFile.at(t)->GetName()).find("ttZ"))!=string::npos) {
          if (debug) {
	    cout<<"New event"<<endl; 
	    cout<<"lep_index size= "<< ind_size <<endl;
            for(unsigned int i=0; i<ind_size; i++) cout<<"lep_index["<< i <<"]= "<< lep_index[i] <<" origin = "<< nTree.at(t)->lepton_truthOrigin->at(lep_index[i]) <<" pT: "<< nTree.at(t)->lepton_pt->at(lep_index[i]) <<endl;
	  }
	  if (debug) { 
	    for(unsigned int i=0; i<nTree.at(t)->lepton_pt->size(); i++) cout<<"index= "<< lep_index[i] <<endl;
          } 
	  //lep_index filled above -> 0,1 from t and 2,3 from H
	  //TLorentzVectors definition
	  TLorentzVector l0, l1, l2, l3, lmet;
          lmet.SetPtEtaPhiM(double(nTree.at(t)->MET_RefFinal_et),0,double(nTree.at(t)->MET_RefFinal_phi),0);
          l0.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[0])),double(nTree.at(t)->lepton_eta->at(lep_index[0])),double(nTree.at(t)->lepton_phi->at(lep_index[0])),double(nTree.at(t)->lepton_E->at(lep_index[0])));
          l1.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[1])),double(nTree.at(t)->lepton_eta->at(lep_index[1])),double(nTree.at(t)->lepton_phi->at(lep_index[1])),double(nTree.at(t)->lepton_E->at(lep_index[1])));
          l2.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[2])),double(nTree.at(t)->lepton_eta->at(lep_index[2])),double(nTree.at(t)->lepton_phi->at(lep_index[2])),double(nTree.at(t)->lepton_E->at(lep_index[2])));
          l3.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(lep_index[3])),double(nTree.at(t)->lepton_eta->at(lep_index[3])),double(nTree.at(t)->lepton_phi->at(lep_index[3])),double(nTree.at(t)->lepton_E->at(lep_index[3])));
          float Mll01 = invMass(l0,l1);
          float Mll23 = invMass(l2,l3);
          float Ml0l1met = (l0+l1+lmet).M();
	  h_Mll01->Fill(Mll01/1000.,weight);
	  h_Mll23->Fill(Mll23/1000.,weight);
          h_Ml0l1met->Fill(Ml0l1met/1000.,weight);
          float Ml2met = invMass(l2,lmet);
          float Ml3met = invMass(l3,lmet);
          float Ml2l3met = (l2+l3+lmet).M();
          h_Ml2met->Fill(Ml2met/1000.,weight);
          h_Ml3met->Fill(Ml3met/1000.,weight);
 	  h_Ml2l3met->Fill(Ml2l3met/1000.,weight);
	  //dR01, dR23;	
	  float dR01 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[0]),nTree.at(t)->lepton_eta->at(lep_index[1]),nTree.at(t)->lepton_phi->at(lep_index[0]),nTree.at(t)->lepton_phi->at(lep_index[1]));
          h_dR01->Fill(dR01,weight);
	  float dR23 = deltaR(nTree.at(t)->lepton_eta->at(lep_index[2]),nTree.at(t)->lepton_eta->at(lep_index[3]),nTree.at(t)->lepton_phi->at(lep_index[2]),nTree.at(t)->lepton_phi->at(lep_index[3]));
	  h_dR23->Fill(dR23,weight);
          float dPhi23 = l2.DeltaPhi(l3);
	  h_dPhi23->Fill(dPhi23,weight);
	  
	}	
      }
      ////////////////////////////////////////////////////////
      /* Exploit main signatures for lepton-jet association */
      ////////////////////////////////////////////////////////

      ////////////////////////////////////////////////////////////////////////
      /* NEW: templates of M(l,b) and DPhi(l,l) with l either from t or V  */
      //////////////////////////////////////////////////////////////////////
      //// truth b-jets have indices: vector<int> v_bjetIndex 		//// 
      //// top leptons indices: lep_index[0], lep_index[1]    		//// 
      //// V leptons indices: lep_index[2], lep_index[3]      		////
      //// Try already available templates: mlbfromtop and mlbfromVtop  ////
      for(unsigned int i=0; i<nTree.at(t)->mlbfromtop->size(); i++)  	h_mlbtop->Fill(nTree.at(t)->mlbfromtop->at(i)/1000.,weight);
      for(unsigned int i=0; i<nTree.at(t)->mlbfromVtop->size(); i++) 	h_mlbVtop->Fill(nTree.at(t)->mlbfromVtop->at(i)/1000.,weight);
      for(unsigned int i=0; i<nTree.at(t)->DPhillfromtop->size(); i++)  h_DPhillfromtop->Fill(nTree.at(t)->DPhillfromtop->at(i),weight);
      for(unsigned int i=0; i<nTree.at(t)->DPhillfromV->size(); i++) 	h_DPhillfromV->Fill(nTree.at(t)->DPhillfromV->at(i),weight);
      for(unsigned int i=0; i<nTree.at(t)->DPhillfromtopV->size(); i++) h_DPhillfromtopV->Fill(nTree.at(t)->DPhillfromtopV->at(i),weight);
      for(unsigned int i=0; i<nTree.at(t)->DPhilbfromtop->size(); i++)  h_DPhilbfromtop->Fill(nTree.at(t)->DPhilbfromtop->at(i),weight);
      for(unsigned int i=0; i<nTree.at(t)->DPhilbfromVtop->size(); i++) h_DPhilbfromVtop->Fill(nTree.at(t)->DPhilbfromVtop->at(i),weight);
      //////////////////////////////////////////////////////////
      /* NEW: templates of M(l,b) with l either from t or V  */
      ////////////////////////////////////////////////////////

      if( goodOne ) {
	hN               ->Fill(nTree.at(t)->lepton_pt->size(),weight);
	h_etmiss	 ->Fill(nTree.at(t)->MET_RefFinal_et/1000.,weight);
        h_nbjets         ->Fill(nbjets,weight);
        h_njets          ->Fill(njets,weight);
        h_M4l            ->Fill(nTree.at(t)->Mllll0123/1000.,weight);
        h_pt0            ->Fill(nTree.at(t)->lepton_pt->at(lep_index[0])/1000.,weight);
        h_pt1            ->Fill(nTree.at(t)->lepton_pt->at(lep_index[1])/1000.,weight);
        h_pt2            ->Fill(nTree.at(t)->lepton_pt->at(lep_index[2])/1000.,weight);
        h_pt3            ->Fill(nTree.at(t)->lepton_pt->at(lep_index[3])/1000.,weight);
      }//if( goodOne )
      
    }//for (Long64_t ievt=0; ievt<nentries;ievt++) 
    v_hN    .push_back( hN    );
    //MC yields
    if( (((string)inputFile.at(t)->GetName()).find("data"))==string::npos ) {
      cout<<"Number of events in "<< inputFile.at(t)->GetName() <<" events passing cuts at L="<< Lum/1000 <<" fb^(-1): "<< Lum*( v_hN.at(t)->GetBinContent(n_leps) ) <<" +- "<< Lum*( v_hN.at(t)->GetBinError(n_leps) ) <<" in VR"<<endl;
    }
    h_etmiss->Scale(1/h_etmiss->Integral()); 	 			v_etmiss  .push_back(h_etmiss);
    h_nbjets->Scale(1/h_nbjets->Integral()); 	 			v_nbjets  .push_back(h_nbjets);
    h_njets->Scale(1/h_njets->Integral());   	 			v_njets   .push_back(h_njets);
    h_M4l->Scale(1/h_M4l->Integral());       	 			v_M4l     .push_back(h_M4l);
    h_pt0->Scale(1/h_pt0->Integral());       	 			v_pt0     .push_back(h_pt0);
    h_pt1->Scale(1/h_pt1->Integral());       	 			v_pt1     .push_back(h_pt1);
    h_pt2->Scale(1/h_pt2->Integral());       	 			v_pt2     .push_back(h_pt2);
    h_pt3->Scale(1/h_pt3->Integral());       	 			v_pt3     .push_back(h_pt3);
    h_Ml0l1met->Scale(1/h_Ml0l1met->Integral()); 			v_Ml0l1met.push_back(h_Ml0l1met);
    h_Ml2l3met->Scale(1/h_Ml2l3met->Integral()); 			v_Ml2l3met.push_back(h_Ml2l3met);
    h_Ml2met->Scale(1/h_Ml2met->Integral());     			v_Ml2met  .push_back(h_Ml2met);
    h_Ml3met->Scale(1/h_Ml3met->Integral()); 	 			v_Ml3met  .push_back(h_Ml3met);
    h_Mll01->Scale(1/h_Mll01->Integral()); 	 			v_Mll01   .push_back(h_Mll01);
    h_Mll23->Scale(1/h_Mll23->Integral()); 	 			v_Mll23   .push_back(h_Mll23);
    h_dR01->Scale(1/h_dR01->Integral()); 	 			v_dR01    .push_back(h_dR01);
    h_dR23->Scale(1/h_dR23->Integral()); 	 			v_dR23    .push_back(h_dR23);
    h_dRlb_min->Scale(1/h_dRlb_min->Integral()); 			v_dRlb_min.push_back(h_dRlb_min);
    h_dRlj_min->Scale(1/h_dRlj_min->Integral()); 			v_dRlj_min.push_back(h_dRlj_min);
    h_dRlb_max->Scale(1/h_dRlb_max->Integral()); 			v_dRlb_max.push_back(h_dRlb_max);
    h_dPhi23->Scale(1/h_dPhi23->Integral()); 	 			v_dPhi23  .push_back(h_dPhi23);
    h_mlbtop->Scale(1/h_mlbtop->Integral());	 			v_mlbtop  .push_back(h_mlbtop);
    h_mlbVtop->Scale(1/h_mlbVtop->Integral());	 			v_mlbV    .push_back(h_mlbVtop);
    h_DPhillfromtop->Scale(1/h_DPhillfromtop->Integral()); 		v_DPhillfromtop.push_back(h_DPhillfromtop);
    h_DPhillfromtopV->Scale(1/h_DPhillfromtopV->Integral()); 		v_DPhillfromtopV.push_back(h_DPhillfromtopV);
    h_DPhillfromV->Scale(1/h_DPhillfromV->Integral()); 			v_DPhillfromV.push_back(h_DPhillfromV);
    h_DPhilbfromtop->Scale(1/h_DPhilbfromtop->Integral());		v_DPhilbfromtop.push_back(h_DPhilbfromtop);
    h_DPhilbfromVtop->Scale(1/h_DPhilbfromVtop->Integral());		v_DPhilbfromVtop.push_back(h_DPhilbfromVtop);

  } //for(int t=0; t<sizeof(files) / sizeof(files[0]); t++)
  cout<<"Templates are ready. Passing to evaluation..."<<endl;

  /* SAVE TEMPLATE plots in outFolder */
  if (plot) {
    TString pdfTitle = string() + outFold + "tot_etmiss.pdf";
    TH1F *tot_h_etmiss = new TH1F("tot_h_etmiss","",v_etmiss.at(0)->GetNbinsX(),v_etmiss.at(0)->GetBin(1),v_etmiss.at(0)->GetBin(v_etmiss.at(0)->GetNbinsX()));
    tot_h_etmiss = PlotMC(Lum,v_etmiss,pdfTitle,"E_{T}^{miss} [GeV]"," GeV");

    TString pdfTitle21 = string() + outFold + "mlbV.pdf";
    TH1F *mlbVtop = new TH1F("mlbVtop","",v_mlbtop.at(0)->GetNbinsX(),v_etmiss.at(0)->GetBin(1),v_etmiss.at(0)->GetBin(v_etmiss.at(0)->GetNbinsX()));
    mlbVtop = PlotMC(Lum,v_mlbV,pdfTitle21,"mlbV [GeV]"," GeV");

    TString pdfTitle20 = string() + outFold + "mlbtop.pdf";
    TH1F *mlbtop = new TH1F("mlbtop","",v_mlbtop.at(0)->GetNbinsX(),v_etmiss.at(0)->GetBin(1),v_etmiss.at(0)->GetBin(v_etmiss.at(0)->GetNbinsX()));
    mlbtop = PlotMC(Lum,v_mlbtop,pdfTitle20,"mlbtop [GeV]"," GeV");

    TString pdfTitle19 = string() + outFold + "Ml0l1met.pdf";
    TH1F *Ml0l1met = new TH1F("Ml0l1met","",v_Ml0l1met.at(0)->GetNbinsX(),v_dRlb_max.at(0)->GetBin(1),v_dRlb_max.at(0)->GetBin(v_dRlb_max.at(0)->GetNbinsX()));
    Ml0l1met = PlotMC(Lum,v_Ml0l1met,pdfTitle19,"M_{l0l1met}","");

    TString pdfTitle18 = string() + outFold + "dRlj_min.pdf";
    TH1F *dRlj_min = new TH1F("dRlj_min","",v_dRlb_min.at(0)->GetNbinsX(),v_dRlb_min.at(0)->GetBin(1),v_dRlb_min.at(0)->GetBin(v_dRlb_min.at(0)->GetNbinsX()));
    dRlj_min = PlotMC(Lum,v_dRlj_min,pdfTitle18,"min(#Delta R)_{top l - jet} ","");

    TString pdfTitle17 = string() + outFold + "dPhi23.pdf";
    TH1F *dPhi23 = new TH1F("dPhi23","",v_Ml2l3met.at(0)->GetNbinsX(),v_dRlb_max.at(0)->GetBin(1),v_dRlb_max.at(0)->GetBin(v_dRlb_max.at(0)->GetNbinsX()));
    dPhi23 = PlotMC(Lum,v_dPhi23,pdfTitle17,"#Delta#phi_{23}","");

    TString pdfTitle16 = string() + outFold + "Ml2l3met.pdf";
    TH1F *Ml2l3met = new TH1F("Ml2l3met","",v_Ml2l3met.at(0)->GetNbinsX(),v_dRlb_max.at(0)->GetBin(1),v_dRlb_max.at(0)->GetBin(v_dRlb_max.at(0)->GetNbinsX()));
    Ml2l3met = PlotMC(Lum,v_Ml2l3met,pdfTitle16,"M_{l2l3met}","");

    TString pdfTitle15 = string() + outFold + "dRlb_max.pdf";
    TH1F *dRlb_max = new TH1F("dRlb_max","",v_dRlb_max.at(0)->GetNbinsX(),v_dRlb_max.at(0)->GetBin(1),v_dRlb_max.at(0)->GetBin(v_dRlb_max.at(0)->GetNbinsX()));
    dRlb_max = PlotMC(Lum,v_dRlb_max,pdfTitle15,"max(#Delta R)_{top #ell - b} ","");  

    TString pdfTitle14 = string() + outFold + "dRlb_min.pdf";
    TH1F *dRlb_min = new TH1F("dRlb_min","",v_dRlb_min.at(0)->GetNbinsX(),v_dRlb_min.at(0)->GetBin(1),v_dRlb_min.at(0)->GetBin(v_dRlb_min.at(0)->GetNbinsX()));
    dRlb_min = PlotMC(Lum,v_dRlb_min,pdfTitle14,"min(#Delta R)_{top #ell - b} ","");  

    TString pdfTitle13 = string() + outFold + "nbjets.pdf";
    TH1F *nbjets = new TH1F("nbjets","",v_nbjets.at(0)->GetNbinsX(),v_nbjets.at(0)->GetBin(1),v_nbjets.at(0)->GetBin(v_nbjets.at(0)->GetNbinsX()));
    nbjets = PlotMC(Lum,v_nbjets,pdfTitle13,"n b-jets","");

    TString pdfTitle12 = string() + outFold + "njets.pdf";
    TH1F *njets = new TH1F("njets","",v_njets.at(0)->GetNbinsX(),v_njets.at(0)->GetBin(1),v_njets.at(0)->GetBin(v_njets.at(0)->GetNbinsX()));
    njets = PlotMC(Lum,v_njets,pdfTitle12,"n jets","");

    TString pdfTitle11 = string() + outFold + "M4l_.pdf";
    TH1F *M4l = new TH1F("M4l","",v_M4l.at(0)->GetNbinsX(),v_M4l.at(0)->GetBin(1),v_M4l.at(0)->GetBin(v_M4l.at(0)->GetNbinsX()));
    M4l = PlotMC(Lum,v_M4l,pdfTitle11,"M_{4#ell} [GeV]"," GeV");

    TString pdfTitle7 = string() + outFold + "pt0.pdf";
    TH1F *pt0 = new TH1F("pt0","",v_pt0.at(0)->GetNbinsX(),v_pt0.at(0)->GetBin(1),v_pt0.at(0)->GetBin(v_pt0.at(0)->GetNbinsX()));
    pt0 = PlotMC(Lum,v_pt0,pdfTitle7,"pt_{0} [GeV]"," GeV");

    TString pdfTitle8 = string() + outFold + "pt1.pdf";
    TH1F *pt1 = new TH1F("pt1","",v_pt1.at(0)->GetNbinsX(),v_pt1.at(0)->GetBin(1),v_pt1.at(0)->GetBin(v_pt1.at(0)->GetNbinsX()));
    pt1 = PlotMC(Lum,v_pt1,pdfTitle8,"pt_{1} [GeV]"," GeV");

    TString pdfTitle9 = string() + outFold + "pt2.pdf";
    TH1F *pt2 = new TH1F("pt2","",v_pt2.at(0)->GetNbinsX(),v_pt2.at(0)->GetBin(1),v_pt2.at(0)->GetBin(v_pt2.at(0)->GetNbinsX()));
    pt2 = PlotMC(Lum,v_pt2,pdfTitle9,"pt_{2} [GeV]"," GeV");

    TString pdfTitle10 = string() + outFold + "pt3.pdf";
    TH1F *pt3 = new TH1F("pt3","",v_pt3.at(0)->GetNbinsX(),v_pt3.at(0)->GetBin(1),v_pt3.at(0)->GetBin(v_pt3.at(0)->GetNbinsX()));
    pt3 = PlotMC(Lum,v_pt3,pdfTitle10,"pt_{3} [GeV]"," GeV");

    TString pdfTitle5 = string() + outFold + "Ml2met.pdf";
    TH1F *Ml2met = new TH1F("Ml2met","",v_Ml2met.at(0)->GetNbinsX(),v_Ml2met.at(0)->GetBin(1),v_Ml2met.at(0)->GetBin(v_Ml2met.at(0)->GetNbinsX()));
    Ml2met = PlotMC(Lum,v_Ml2met,pdfTitle5,"M_{2 met} [GeV]"," GeV");

    TString pdfTitle6 = string() + outFold + "Ml3met.pdf";
    TH1F *Ml3met = new TH1F("Ml3met","",v_Ml3met.at(0)->GetNbinsX(),v_Ml3met.at(0)->GetBin(1),v_Ml3met.at(0)->GetBin(v_Ml3met.at(0)->GetNbinsX()));
    Ml3met = PlotMC(Lum,v_Ml3met,pdfTitle6,"M_{3 met} [GeV]"," GeV");

    TString pdfTitle3 = string() + outFold + "Mll01.pdf";
    TH1F *Mll01 = new TH1F("Mll01","",v_Mll01.at(0)->GetNbinsX(),v_Mll01.at(0)->GetBin(1),v_Mll01.at(0)->GetBin(v_Mll01.at(0)->GetNbinsX()));
    Mll01 = PlotMC(Lum,v_Mll01,pdfTitle3,"M_{01} [GeV]"," GeV");

    TString pdfTitle4 = string() + outFold + "Mll23.pdf";
    TH1F *Mll23 = new TH1F("Mll23","",v_Mll23.at(0)->GetNbinsX(),v_Mll23.at(0)->GetBin(1),v_Mll23.at(0)->GetBin(v_Mll23.at(0)->GetNbinsX()));
    Mll23 = PlotMC(Lum,v_Mll23,pdfTitle4,"M_{23} [GeV]"," GeV");

    TString pdfTitle1 = string() + outFold + "dR01.pdf";
    TH1F *dR01 = new TH1F("dR01","",v_dR01.at(0)->GetNbinsX(),v_dR01.at(0)->GetBin(1),v_dR01.at(0)->GetBin(v_dR01.at(0)->GetNbinsX()));
    dR01 = PlotMC(Lum,v_dR01,pdfTitle1,"#Delta R","");

    TString pdfTitle2 = string() + outFold + "dR23.pdf";
    TH1F *dR23 = new TH1F("dR23","",v_dR23.at(0)->GetNbinsX(),v_dR23.at(0)->GetBin(1),v_dR23.at(0)->GetBin(v_dR23.at(0)->GetNbinsX()));
    dR23 = PlotMC(Lum,v_dR23,pdfTitle2,"#Delta R","");  

  }//if (plot)
  //f->Write();
  //f->Close();

  ////////////////////////////////////////////////////////
  /* Trying the other way around -> H/Z system and top */
  //////////////////////////////////////////////////////

  if(evaluation) {
    vector<TH1F*> v_newProb, v_newProb1, v_newProb2, v_newProb3;
    float zeroValue=pow(10,-2);
    for(int t=0; t<sizeof(files) / sizeof(files[0]); t++) {
      if ( debug ) {
        cout<<endl;
        cout<<"****                    Processing "<< inputFile.at(t)->GetName() <<"                    ****"<<endl;
        cout<<endl;
      }

      Long64_t nentries = v_fChainSkim.at(t)->GetEntriesFast(); 
      //Create a copy of nominal TTree and add new branch
      TString outName=string()+"myprob_"+files[t];
      TFile *g = new TFile(outName, "RECREATE");
      TTree *newtree = (TTree*)v_fChainSkim.at(t)->CloneTree(0);
      Float_t my_prob(-99.);
      newtree->Branch("myProb", &my_prob, "myProb/F");
      //Create a copy of nominal TTree and add new branch

      //Histos to save PROBABILITY //
      string hist_newProb = string()+"hist_newProb_"+inputFile.at(t)->GetName();
      TH1F   *h_newProb = new TH1F(hist_newProb.c_str(),"", 22, -10., 5.);
      h_newProb->Sumw2();

      string hist_newProb1 = string()+"hist_newProb1_"+inputFile.at(t)->GetName();
      TH1F   *h_newProb1 = new TH1F(hist_newProb1.c_str(),"", 50, -10., 4.);
      h_newProb1->Sumw2();

      string hist_newProb2 = string()+"hist_newProb2_"+inputFile.at(t)->GetName();
      TH1F   *h_newProb2 = new TH1F(hist_newProb2.c_str(),"", 50, -10., 4.);
      h_newProb2->Sumw2();

      string hist_newProb3 = string()+"hist_newProb3_"+inputFile.at(t)->GetName();
      TH1F   *h_newProb3 = new TH1F(hist_newProb3.c_str(),"", 50, -10., 4.);
      h_newProb3->Sumw2();

      //Looping over the events //
      for (Long64_t ievt=0; ievt<nentries;ievt++) {
      //for (Long64_t ievt=0; ievt<10;ievt++) 
        int trash=nTree.at(t)->GetEntry(ievt);
        //weight each event in the proper way //
        const double weight = nTree.at(t)->mcWeightOrg*nTree.at(t)->xsec*(1./nTree.at(t)->totweight)*nTree.at(t)->pileupEventWeight_090*nTree.at(t)->lepSFObjLoose*nTree.at(t)->lepSFTrigLoose*nTree.at(t)->JVT_EventWeight*nTree.at(t)->SherpaNJetWeight*nTree.at(t)->MV2c10_70_EventWeight;
        //Identify OS lepton pairs
        int indices[3]={0};
        int charge[4]={0};
        for (unsigned int i=0; i<nTree.at(t)->lepton_pt->size(); i++) {
          charge[i]=nTree.at(t)->lepton_ID->at(i)/fabs(nTree.at(t)->lepton_ID->at(i));
          if(debug) cout<<"q["<< i <<"] = "<< charge[i] <<endl;
        }
	if (!((charge[0]+charge[1]==0)||(charge[0]+charge[2]==0))) cout<<"**** PLEASE CHECK ME ****"<<endl;
        indices[0]                   = (charge[0]+charge[1]==0) ? 1 : 2;
        if(indices[0]==1) indices[1] = (charge[0]+charge[2]==0) ? 2 : 3;
        else              indices[1] = 3;
        if(debug) cout<<"indices[0]= "<< indices[0] <<"  indices[1]= "<< indices[1] <<endl;
	//Pairs are 0 and indices[0] and 0 and indices[1]
	for(unsigned int i=1; i<nTree.at(t)->lepton_pt->size(); i++) if(i!=indices[0]&&i!=indices[1]) indices[2] = i;
        if (charge[0]!=charge[indices[2]]) cout<<"**** PLEASE CHECK ME: aren't you running with total charge == 0 events?! ****"<<endl;
	//Identify b-tagged jets
	vector<int> btagJindex; btagJindex.reserve(10);
	for(unsigned int i=0; i<nTree.at(t)->m_jet_pt->size(); i++) { if(nTree.at(t)->m_jet_flavor_weight_MV2c10->at(i)>0.8244) btagJindex.push_back(i); }
	//NB -> When accessing leps from top info there will be up to 4 combs! //
        vector<vector<float>> S_probs; S_probs.reserve(4);
        //vector<vector<int>> -> each element is a combination with the following ordering: l0, l1, l2 and l3 (top and H/Z leptons in pT decreasing order)
        vector<vector<int>> v_OScombs = OScombs(nTree.at(t)->lepton_ID,nTree.at(t)->lepton_pt);
	if(debug) {
          for(unsigned int i=0; i<v_OScombs.size(); i++) {
	    cout<<"Component i: "<< i <<endl;
	    for(unsigned int j=0; j<v_OScombs[i].size(); j++) cout<<"v_OScombs[i].at(j) "<< v_OScombs[i].at(j) <<endl;
          }
        }//if(debug)
        //Loop over each combination
        for(unsigned int i=0; i<v_OScombs.size(); i++) {
          int i0 = v_OScombs[i].at(0), i1 = v_OScombs[i].at(1), i2 = v_OScombs[i].at(2), i3 = v_OScombs[i].at(3); 
 	  TLorentzVector l0, l1, l2, l3, lMET;
	  l0.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(i0)),double(nTree.at(t)->lepton_eta->at(i0)),double(nTree.at(t)->lepton_phi->at(i0)),double(nTree.at(t)->lepton_E->at(i0)));
          l1.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(i1)),double(nTree.at(t)->lepton_eta->at(i1)),double(nTree.at(t)->lepton_phi->at(i1)),double(nTree.at(t)->lepton_E->at(i1)));
	  l2.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(i2)),double(nTree.at(t)->lepton_eta->at(i2)),double(nTree.at(t)->lepton_phi->at(i2)),double(nTree.at(t)->lepton_E->at(i2)));
          l3.SetPtEtaPhiE(double(nTree.at(t)->lepton_pt->at(i3)),double(nTree.at(t)->lepton_eta->at(i3)),double(nTree.at(t)->lepton_phi->at(i3)),double(nTree.at(t)->lepton_E->at(i3)));
          lMET.SetPtEtaPhiM(double(nTree.at(t)->MET_RefFinal_et),0,double(nTree.at(t)->MET_RefFinal_phi),0);
	  //Top lepton pTs
          float S_p_pt0 = (v_pt0[0]->GetBinContent(v_pt0[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i0)/1000.))>0) ? v_pt0[0]->GetBinContent(v_pt0[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i0)/1000.)) : zeroValue;
          float B_p_pt0 = (v_pt0[0]->GetBinContent(v_pt0[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i3)/1000.))>0) ? v_pt0[0]->GetBinContent(v_pt0[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i3)/1000.)) : zeroValue;
          float S_p_pt1 = (v_pt1[0]->GetBinContent(v_pt1[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i1)/1000.))>0) ? v_pt1[0]->GetBinContent(v_pt1[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i1)/1000.)) : zeroValue;
          float B_p_pt1 = (v_pt1[0]->GetBinContent(v_pt1[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i2)/1000.))>0) ? 1./v_pt1[0]->GetBinContent(v_pt1[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i2)/1000.)) : zeroValue;
	  //H/Z lepton pTs
	  float S_p_pt2 = (v_pt2[0]->GetBinContent(v_pt2[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i2)/1000.))>0) ? v_pt2[0]->GetBinContent(v_pt2[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i2)/1000.)) : zeroValue;
          float B_p_pt2 = (v_pt2[1]->GetBinContent(v_pt2[1]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i2)/1000.))>0) ? v_pt2[1]->GetBinContent(v_pt2[1]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i2)/1000.)) : zeroValue;
          float S_p_pt3 = (v_pt3[0]->GetBinContent(v_pt3[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i3)/1000.))>0) ? v_pt3[0]->GetBinContent(v_pt3[0]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i3)/1000.)) : zeroValue;
          float B_p_pt3 = (v_pt3[1]->GetBinContent(v_pt3[1]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i3)/1000.))>0) ? v_pt3[1]->GetBinContent(v_pt3[1]->GetXaxis()->FindBin(nTree.at(t)->lepton_pt->at(i3)/1000.)) : zeroValue;
	  //Mll23
	  float S_p_Mll23 = (v_Mll23[0]->GetBinContent(v_Mll23[0]->GetXaxis()->FindBin(invMass(l2,l3)/1000.))>0) ? v_Mll23[0]->GetBinContent(v_Mll23[0]->GetXaxis()->FindBin(invMass(l2,l3)/1000.)) : zeroValue;
          float B_p_Mll23 = (v_Mll23[1]->GetBinContent(v_Mll23[1]->GetXaxis()->FindBin(invMass(l2,l3)/1000.))>0) ? v_Mll23[1]->GetBinContent(v_Mll23[1]->GetXaxis()->FindBin(invMass(l2,l3)/1000.)) : zeroValue;
          float S_p_Ml2l3met = v_Ml2l3met[0]->GetBinContent(v_Ml2l3met[0]->GetXaxis()->FindBin(invMass(l2+l3,lMET)/1000.));
          float B_p_Ml2l3met = v_Ml2l3met[1]->GetBinContent(v_Ml2l3met[1]->GetXaxis()->FindBin(invMass(l2+l3,lMET)/1000.));
          float S_p_dR23  = v_dR23[0]->GetBinContent(v_dR23[0]->GetXaxis()->FindBin(l2.DeltaR(l3)));
          float B_p_dR23  = v_dR23[1]->GetBinContent(v_dR23[1]->GetXaxis()->FindBin(l2.DeltaR(l3)));
	  float S_p_dR01  = v_dR01[0]->GetBinContent(v_dR01[0]->GetXaxis()->FindBin(l0.DeltaR(l1)));
	  float B_p_dR01  = v_dR01[0]->GetBinContent(v_dR01[0]->GetXaxis()->FindBin(l2.DeltaR(l3)));
          float S_p_Mlb(1.), B_p_Mlb(1.), S_p_DPhilb(1.), B_p_DPhilb(1.);
	  for(unsigned int j=0; j<btagJindex.size(); j++) {
            TLorentzVector lb;
            lb.SetPtEtaPhiE(double(nTree.at(t)->m_jet_pt->at(btagJindex[j])),double(nTree.at(t)->m_jet_eta->at(btagJindex[j])),double(nTree.at(t)->m_jet_phi->at(btagJindex[j])),double(nTree.at(t)->m_jet_E->at(btagJindex[j])));
            float dRb0=lb.DeltaR(l0); float dRb1=lb.DeltaR(l1); float dRb2=lb.DeltaR(l2); float dRb3=lb.DeltaR(l3);
	    float mindRlb_top_M = (dRb0<dRb1) ? invMass(lb,l0) : invMass(lb,l1);
            S_p_Mlb *= (v_mlbtop[0]->GetBinContent(v_mlbtop[0]->GetXaxis()->FindBin(mindRlb_top_M/1000.))>0) ? v_mlbtop[0]->GetBinContent(v_mlbtop[0]->GetXaxis()->FindBin(mindRlb_top_M/1000.)) : zeroValue;
            B_p_Mlb *= (v_mlbV[1]->GetBinContent(v_mlbV[1]->GetXaxis()->FindBin(mindRlb_top_M/1000.))>0) ? v_mlbV[1]->GetBinContent(v_mlbV[1]->GetXaxis()->FindBin(mindRlb_top_M/1000.)) : zeroValue;
	    float minDPhilb     = (lb.DeltaPhi(l0)<lb.DeltaPhi(l1)) ? lb.DeltaPhi(l0) : lb.DeltaPhi(l1);
            S_p_DPhilb *= (v_DPhilbfromtop[0]->GetBinContent(v_DPhilbfromtop[0]->GetXaxis()->FindBin(minDPhilb))>0) ? v_DPhilbfromtop[0]->GetBinContent(v_DPhilbfromtop[0]->GetXaxis()->FindBin(minDPhilb)) : zeroValue;
            B_p_DPhilb *= (v_DPhilbfromVtop[0]->GetBinContent(v_DPhilbfromVtop[0]->GetXaxis()->FindBin(minDPhilb))>0) ? v_DPhilbfromVtop[0]->GetBinContent(v_DPhilbfromVtop[0]->GetXaxis()->FindBin(minDPhilb)) : zeroValue;
	  }
	  float S_p_DPhillfromtop = (v_DPhillfromtop[0]->GetBinContent(v_DPhillfromtop[0]->GetXaxis()->FindBin(l0.DeltaPhi(l1)))>0) ? v_DPhillfromtop[0]->GetBinContent(v_DPhillfromtop[0]->GetXaxis()->FindBin(l0.DeltaPhi(l1))) : 0;
          float B_p_DPhillfromtopV   = (v_DPhillfromtopV[1]->GetBinContent(v_DPhillfromtopV[1]->GetXaxis()->FindBin(l0.DeltaPhi(l1)))>0) ? v_DPhillfromtopV[1]->GetBinContent(v_DPhillfromtopV[1]->GetXaxis()->FindBin(l0.DeltaPhi(l1))) : 0;
          //float B_p_DPhillfromtopV   = (v_DPhillfromtopV[0]->GetBinContent(v_DPhillfromtopV[0]->GetXaxis()->FindBin(l0.DeltaPhi(l1)))>0) ? v_DPhillfromtopV[0]->GetBinContent(v_DPhillfromtopV[0]->GetXaxis()->FindBin(l0.DeltaPhi(l1))) : 0;
          float S_p_DPhillfromV   = (v_DPhillfromV[0]->GetBinContent(v_DPhillfromV[0]->GetXaxis()->FindBin(l2.DeltaPhi(l3)))>0) ? v_DPhillfromV[0]->GetBinContent(v_DPhillfromV[0]->GetXaxis()->FindBin(l2.DeltaPhi(l3))) : 0;
          float B_p_DPhillfromV   = (v_DPhillfromV[1]->GetBinContent(v_DPhillfromV[1]->GetXaxis()->FindBin(l2.DeltaPhi(l3)))>0) ? v_DPhillfromV[1]->GetBinContent(v_DPhillfromV[1]->GetXaxis()->FindBin(l2.DeltaPhi(l3))) : 0;

	  // Signal/Background - probabilities for this combination
	  //cout<<"S_p_DPhilb: "<< S_p_DPhilb <<" B_p_DPhilb: "<< B_p_DPhilb <<endl;
	  //cout<<"S_p_Mlb: "<< S_p_Mlb <<" B_p_Mlb: "<< B_p_Mlb <<endl;
	  float pS=(S_p_pt2*S_p_pt3*S_p_Mll23*S_p_DPhillfromtop*S_p_DPhilb*S_p_Mlb*S_p_DPhillfromV*S_p_dR01*S_p_dR23*S_p_pt0*S_p_pt1);
          float pB=(B_p_pt2*B_p_pt3*B_p_Mll23*B_p_DPhillfromtopV*B_p_DPhilb*B_p_Mlb*B_p_DPhillfromtopV*B_p_dR01*B_p_dR23*B_p_pt0*B_p_pt1);
          //float pB=(B_p_pt2*B_p_pt3*B_p_Mll23*B_p_DPhillfromtopV*B_p_DPhillfromtopV*B_p_dR01*B_p_dR23*B_p_pt0*B_p_pt1);
	  vector<float> pSandB; pSandB.reserve(2); pSandB.push_back(pS); pSandB.push_back(pB);
	  S_probs.push_back(pSandB);
        }//for(unsigned int i=0; i<v_OScombs.size(); i++)
	//Maximum prob
	float pSmax=TMath::Max(S_probs[0].at(0),S_probs[1].at(0)); pSmax=TMath::Max(pSmax,S_probs[2].at(0)); pSmax=TMath::Max(pSmax,S_probs[3].at(0));
	float pBmax(-99);
	if     (pSmax==S_probs[0].at(0))     pBmax=S_probs[0].at(1);
        else if(pSmax==S_probs[1].at(0))     pBmax=S_probs[1].at(1);
        else if(pSmax==S_probs[2].at(0))     pBmax=S_probs[2].at(1);
        else if(pSmax==S_probs[3].at(0))     pBmax=S_probs[3].at(1);

        //EVENT-ONLY PROBS (independent of any lepton and jet choiche)
        //M4l
        //float M4l=nTree.at(t)->Mllll0123;
        //float S_p_M4l = v_M4l[0]->GetBinContent(v_M4l[0]->GetXaxis()->FindBin(M4l/1000.)); 
        //float B_p_M4l = v_M4l[1]->GetBinContent(v_M4l[1]->GetXaxis()->FindBin(M4l/1000.)); 
        //pSmax*=S_p_M4l;   pBmax*=B_p_M4l;
        
        my_prob = log10(pSmax/pBmax);
	if (my_prob!=my_prob||my_prob>pow(10,10)) { cout<<"*WARNING* I'm nan!"<< my_prob <<endl; my_prob = -20; }

        h_newProb ->Fill(my_prob    ,weight);
        //h_newProb ->Fill(log10(pSmax/pBmax)    ,weight);
        //h_newProb1->Fill(log10(pSmax_1/pBmax_1),weight);
        //h_newProb2->Fill(log10(pSmax/pBmax)    ,weight);
        //h_newProb3->Fill(log10(pSmax/pBmax)    ,weight);
 
	if(debug) { cout<<"pSmax: "<< pSmax <<" pBmax: "<< pBmax <<endl; cout<<"prob: "<< my_prob <<endl; }
        g->cd();
        newtree->Fill();

      }//for (Long64_t ievt=0; ievt<nentries;ievt++)
      v_newProb .push_back(h_newProb );
      v_newProb1.push_back(h_newProb1);
      v_newProb2.push_back(h_newProb2);
      v_newProb3.push_back(h_newProb3);

      g->Write();
    }//for(int t=0; t<sizeof(files) / sizeof(files[0]); t++)

    TString pdfProb = string() + outFold + "newProb.pdf";
    TH1F *newProb = new TH1F("newProb","",v_newProb.at(0)->GetNbinsX(),v_newProb.at(0)->GetBin(1),v_newProb.at(0)->GetBin(v_newProb.at(0)->GetNbinsX()));
    newProb = PlotMCRatio(Lum,v_newProb,pdfProb,"log(PROB)","");

    TString pdfProb1 = string() + outFold + "newProb1.pdf";
    TH1F *newProb1 = new TH1F("newProb1","",v_newProb.at(0)->GetNbinsX(),v_newProb.at(0)->GetBin(1),v_newProb.at(0)->GetBin(v_newProb.at(0)->GetNbinsX()));
    newProb1 = PlotMCRatio(Lum,v_newProb1,pdfProb1,"PROB 1","");

    TString pdfProb2 = string() + outFold + "newProb2.pdf";
    TH1F *newProb2 = new TH1F("newProb2","",v_newProb.at(0)->GetNbinsX(),v_newProb.at(0)->GetBin(1),v_newProb.at(0)->GetBin(v_newProb.at(0)->GetNbinsX()));
    newProb2 = PlotMCRatio(Lum,v_newProb2,pdfProb2,"PROB 2","");

    TString pdfProb3 = string() + outFold + "newProb3.pdf";
    TH1F *newProb3 = new TH1F("newProb3","",v_newProb.at(0)->GetNbinsX(),v_newProb.at(0)->GetBin(1),v_newProb.at(0)->GetBin(v_newProb.at(0)->GetNbinsX()));
    newProb3 = PlotMCRatio(Lum,v_newProb3,pdfProb3,"PROB 3","");

  } //if(evaluation)

  f->Close();
  return 0;
}//int main() 
